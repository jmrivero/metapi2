package org.metapi.web;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.Test;

public class ZipInputStreamTest {

  @Test
  public void test() throws IOException {
    int count = 0;
    ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(new File("src/test/resources/zipfile.zip")));
    ZipEntry zipEntry = zipInputStream.getNextEntry();
    while (zipEntry != null) {
      count++;
      if (zipEntry.getSize() == -1) {
        zipInputStream.closeEntry();
        zipEntry = zipInputStream.getNextEntry();
        continue;
      }
      zipInputStream.closeEntry();
      zipEntry = zipInputStream.getNextEntry();
    }
    zipInputStream.close();
    assertEquals(3, count);
  }

}
