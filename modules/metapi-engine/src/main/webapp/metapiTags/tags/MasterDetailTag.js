var MasterDetailTag = MetapiTag
		.extend({

			init : function(containerId, objectType,associationName,destType, elementSelector,
					frontendScript, backendScript) {
				this._super();
				this.containerId = containerId;
				this.objectType = objectType;
				this.associationName = associationName;
				this.destType = destType;
				this.elementSelector = elementSelector;
				this.setFrontendScript(frontendScript);
				this.registerBackendScript("GET", objectType + "s",
						backendScript);
			},

			getDefaultFrontendScript : function(script) {
				return ("var self = this;"
						+ "$.get(this.getUrlPrefix() + this.objectType + \"s\", function(elements) {"
						+ "	elements = JSON.parse(elements);"
						+ "	var domElement = $(self.containerId).find(self.elementSelector);"
						+ "	for (i in elements) {"
						+ "		var newElement = domElement.clone();"
						+ "		newElement.html(elements[i]);"
						+ "		domElement.append(newElement);" + "	}" + "});")
			},

			getDefaultBackendScript : function(script) {
				return '["o1", "o2", "o3","o44455"]';
			},
			detalle : function (idElement) {
				
				alert(idElement);
			},
			run : function(context) {
				var self = this;
				$.get(this.getUrlPrefix() + this.objectType + "s",function(elements) 
				{
					elements = JSON.parse(elements);	
					var domElement = $(self.containerId).find(self.elementSelector);
					//agrego Titulo al listado principal
					var titCabecera='<h3>'+self.objectType+'s</h3>';
					domElement.append(titCabecera);
					//agrego Cabeceras
					var cabecera='<tr>';
					for(var attr in elements[0]){
						cabecera=cabecera+'<th>'+attr+'</th>';
					}
					cabecera+'</tr>';
					domElement.append(cabecera);
					for (i in elements) 
					{
						var newElement='<tr>'
						for(var attr in elements[i]){
							newElement =newElement+'<td>'+elements[i][attr]+'</td>';
						}
						newElement =  newElement+'<td><button id="'+elements[i].id+'">detalles</td></tr>';
						domElement.append(newElement);
						$("#"+elements[i].id).click(function(){
							var domDetailElement=$("#detalle");
							domDetailElement.empty();
							//creo el endPoint para aobtener los detalles
							var data = {
									method: 'GET',
									urlRegex: 'vacunas',
									script: 'env.db.getAssociatedObjects("'+self.objectType+'", "'+self.associationName+'", "'+self.destType+'", '+this.id+')'
								}
							$.ajax({url: "/metapi/endpoints", type: "POST", data: JSON.stringify(data), success: function(createdEndpoint) {
			 					endpointId = JSON.parse(createdEndpoint).id;
			 					console.log("crea "+endpointId);
								$.get(self.getUrlPrefix() + self.destType + "s",function(detalles) 
										{
											detalles=JSON.parse(detalles);
											//agrego Titulo al listado principal
											var titCabecera='<h3>'+self.destType+'s</h3>';
											domDetailElement.append(titCabecera);
											//agrego Cabeceras
											var cabecera='<tr>';
											for(var attr in detalles[0]){
												cabecera=cabecera+'<th>'+attr+'</th>';
											}
											cabecera+'</tr>';
											domDetailElement.append(cabecera);
											
											for (i in detalles) {
												var newElement="<tr>";
												for(var attr in detalles[i]){
													newElement =newElement+'<td>'+detalles[i][attr]+'</td>';
												}
												newElement =  newElement+'</tr>';
												domDetailElement.append(newElement);
											};
										});
								//borro el endPointCreado
								$.ajax({url: "/metapi/endpoints/" + endpointId, type: "DELETE", success: function() {console.log("borro "+endpointId);}});
			 					
							}});				
		 					

						});

					}
				});
			}


		});