var SaveTag = MetapiTag.extend({

	init: function(url,formulario,selector,tagHijos) {
		this._super();
		this.disparador = "click";
		this.selector = selector;
		this.formulario = formulario;
		this.url = url;
		this.tagHijos = tagHijos;
		this.domElement=null;
	},
	
	run: function(object) {
		self=this;
		$(self.selector)[self.disparador](
				function (){
					var jsonObject={};
					for (j in self.tagHijos){
						jsonObject =self.tagHijos[j].run($(self.formulario),jsonObject);
					}
					if (jsonObject.id==null || jsonObject.id==""){
						var urlP=self.getUrlPrefix() + self.url;
						var typeP="POST";
					}
					else{
						var urlP=self.getUrlPrefix() + self.url+"/"+jsonObject.id;
						var typeP="PUT";
					}
					console.log(jsonObject);
					$.ajax({url: urlP, type: typeP, data:JSON.stringify(jsonObject), 
						success: 
						function() {
							console.log("ok");
						},
						error:
						function() {
							console.log("error");
						}	
					})
				}
		);		
	}
});