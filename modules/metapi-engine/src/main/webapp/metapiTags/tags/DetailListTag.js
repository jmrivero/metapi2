var DetailListTag = MetapiTag
		.extend({

			init : function(detalle,clonElement,propiedad,tagHijos, disparador, selector) {
				this._super();
				this.detalle = detalle;
				this.clonElement=clonElement;
				this.propiedad=propiedad;
				this.tagHijos = tagHijos;
				this.disparador = disparador;
				this.selector = selector;
				
			},


			run : function(element, objeto) {
				self.domElement = $(this.detalle).find(this.cloneElement);
				var elements=element[this.propiedad];
				for (i in elements) {
					var newElement= self.domElement.clone();
					for (j in self.tagHijos){
						this.tagHijos[j].run(newElement,elements[i]);
						$(this.detalle).append(newElement);
					}	
				}
			/*	element.find(self.selector)[self.disparador](function() {
					for (j in self.tagHijos) {
						self.tagHijos[j].run($(self.detalle), objeto);
					}
				})*/
			}
		});