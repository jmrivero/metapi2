var PropertyTag = MetapiTag.extend({

	init: function(propiedad,selector) {
		this._super();
		this.propiedad = propiedad;
		this.selector = selector;
	},
	
	run: function(element,objeto) {
		element.find(this.selector).val(objeto[this.propiedad]);
		element.find(this.selector).html(objeto[this.propiedad]);
		return objeto;
	}

});