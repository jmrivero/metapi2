var PropertyTagGet = MetapiTag.extend({

	init: function(propiedad,selector) {
		this._super();
		this.propiedad = propiedad;
		this.selector = selector;
	},
	
	run: function(element,objeto) {
		objeto[this.propiedad]=element.find(this.selector).val();
		return objeto;
	},
});