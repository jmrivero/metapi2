<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Metapi service editor</title>

<script src="lib/codemirror.js"></script><style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript" ></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>
	

		
<link rel="stylesheet" href="css/codemirror.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">


<style>
	#alerts {
		/*height: 63px;*/
	}
  
  #fileList {
    list-style: none;
    margin: 0px;
    padding: 0px;
    height:  437px;
    overflow-y: scroll;
  }
</style>
</head>
<body style="">
  
<jsp:include page="parts/menu.jsp"></jsp:include>

</nav>



  <script type="text/javascript">
    var templateEditor;
    var currentFilename = null;
    var newFile = false;
    var applicationId = 2;
    var popupManager = new PopupManager();
    
    var extensionModeMappings = {
    	js: "javascript",
    	xml: "xml",
    	html: "htmlmixed",
    	htm: "htmlmixed",
    	css: "css",
    	tl: "dust"
    }
    
    var metapiResourcePath = "../metapi/resources/1"
    
    function extractExtension(filename) {
    	var parts = filename.split(".");
      return parts[parts.length - 1];
    }
    
    function updateEditorMode(filename) {
    	var extension = extractExtension(filename);
    	if (extensionModeMappings[extension]) {
    		templateEditor.setOption("mode", extensionModeMappings[extension]);
    	}
    }
    
    function fileSelected(filename, content) {
        templateEditor.setValue(content);
        updateEditorMode(filename);
        $("#filename").text(filename);
        currentFilename = filename;
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
    
    
    function closed(){
    	$("#not").remove();
    }
    
    
    function fileSaved(filename, content) {
    	popupManager.showSuccess("Resource saved");
    	if (newFile) {
    		addFileToList(filename);
    		newFile = false;
    	}
    }
    
    function addFileToList(filename) {
        var html = "<li>";
        filename = filename[0] == '/' ? filename.substring(1) : filename;
        var extension = extractExtension(filename);
        html = html + '<button class="btn btn-xs deleteFile" href="[filename]">Del</button>'.replace(/\[filename\]/g, filename);
        if (extensionModeMappings[extension]) {
          html = html + '<a href="file/[filename]" class="fileLink">[filename]</a>'.replace(/\[filename\]/g, filename);
        } else {
          html = html + filename;
        }
        html = html + "</li>";
        var jHtml = $(html);
        jHtml.find('a.fileLink').click(function(e) {
            var self = this;
            e.preventDefault();   
            $.ajax({url: metapiResourcePath + "/" + filename, type: "GET", success: function(response) {
              fileSelected(filename, response)
             }});
        });
        jHtml.find('button.deleteFile').click(function(e) {
            var self = this;
            e.preventDefault();
            $.ajax({url: metapiResourcePath + "/" + $(self).attr('href'), type: "DELETE", success: function(response) {
              $(self).parent().hide(500, function() {$(self).parent().remove()});
            }});
        });
        $("#fileList").append(jHtml);
    }
    
    $(document).ready(function() {
    	
        templateEditor = CodeMirror($("#editor")[0], {
            value: "",
            mode:  "dust",
            lineNumbers: true
        });
        templateEditor.setSize(700, 450);
    	
    	  $.ajax({url: metapiResourcePath, type: "GET", dataType: 'json', success: function(response) {
            $.each(response.files, function(_, filename) {
            	 addFileToList(filename);
            });
        }});
        
        $("#saveButton").click(function(e) {
            var self = this;
            e.preventDefault();   
            $.ajax({url: metapiResourcePath + "/" + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
              fileSaved(currentFilename, templateEditor.getValue());
             }});
        });
        
        $("#uploadZip").click(function(e) {
            var self = this;
            e.preventDefault();  
            $("#zipFile").trigger("click");
        });  
        
        $("#zipFile").change(function(e) {
        	$("#zipUploadForm").submit()
        }); 
        
        $("#newFile").click(function(e) {
        	  e.preventDefault();
            var filename = prompt("Enter filename");
            if (!filename) {
            	return;
            }
            if (filename[0] == '/') {
            	filename = filename.substring(1);
            }
            fileSelected(filename, "");
            newFile = true;
         });
       
        
        

        
        
        
        
        
        
    });
    
		</script>

  <div class="container">
      <div id="areaR" >
    </div>

    <div class="row">
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <form id="zipUploadForm" action="resources/1/upload" method="POST" enctype="multipart/form-data">
          <input id="zipFile" type="file" name="file" style="display: none">
          <!-- <button id="uploadZip" class="btn">Upload zip file</button> -->
          <button id="uploadZip" class="btn btn-default">Upload zip file <span class="glyphicon glyphicon-compressed"></span></button>
          <!-- <button id="newFile" class="btn">New file</button> -->
          <button id="newFile" class="btn btn-default">New File <span class="glyphicon glyphicon-file"></span></button>
        </form>
      </div>
      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <h2>
          <span id="filename"></span>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <ul id="fileList">
		</ul>

		
      </div>
      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
          <!-- <button class="btn btn-primary" id="saveButton">Save</button> -->
          <button id="saveButton" class="btn btn-primary btn-sm">Save <span class="glyphicon glyphicon-floppy-disk"></span></button>
        </div>
        <div class="row">
          <div id="editorContainer">
            <div id="editor">
</div>
          </div>
        </div>
      </div>
    </div>
  </div>

</body></html>
