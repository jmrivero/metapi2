<nav class="navbar navbar-default navbar-static-top navbar-inverse"	role="navigation">
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul id="menu" class="nav navbar-nav">
			<li><div class="navbar-brand">MetapiTag</div><li>
			<li><a href="editor">Tags Editor</a></li>
			<li><a href="projects">Projects</a></li>
		</ul>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			var urlParts = location.pathname.split("/")
			page = urlParts[urlParts.length - 1];
			$("#menu li a[href=" + page + "]").parent().addClass("active");
		})
	</script>
</nav>