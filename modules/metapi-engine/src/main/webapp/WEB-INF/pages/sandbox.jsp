<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Metapi service editor</title>
<script src="lib/codemirror.js"></script><style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript" ></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>

<link rel="stylesheet" href="css/codemirror.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">


<style>
	#alerts {
		/*height: 63px;*/
	}
  
  #fileList {
    list-style: none;
    margin: 0px;
    padding: 0px;
    height:  535px;
    overflow-y: scroll;
  }
</style>
<script src="lib/bootbox.min.js"></script>
<script type="text/javascript">
	var popupManager = new PopupManager();
		
	function showMessage(message, type) {
		if (type == "success") {
			popupManager.showSuccess(message);
		} else {
			popupManager.showError(message);
		}
	}
		
	</script>
	
	
	
	
</head>
<body style="">
  
<jsp:include page="parts/menu.jsp"></jsp:include>

  <script type="text/javascript">
    var scriptEditor;
    
    $(document).ready(function() {
    	
        scriptEditor = CodeMirror($("#editor")[0], {
            value: "",
            mode:  "javascript",
            lineNumbers: true
        });
        scriptEditor.setSize(1122, 360);
        
        $("#runButton").click(function() {
		$("#result").attr("disabled", "disabled");
		$.ajax({url: "../metapi/run", type: "POST", data: scriptEditor.getValue(), success: function(response) {
			$("#result").val(response);
			$("#result").attr("disabled", null);
			showMessage("OK", "success");
		}, error: function(request, message, error) {
			showMessage(request.responseText, "error");
		}});
        });
    });
    
</script>

  <div class="container">
    <div id="alerts"></div>
    <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- <button class="btn btn-success" id="runButton">Run</button> -->
			<button id="runButton" class="btn btn-success btn-sm">Run <span class="glyphicon glyphicon-play"></span></button>
		</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
          <div id="editorContainer">
          <p class="text-info">Please, enter your code in this box:</p>
            <div id="editor"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <textarea id="result" class="form-control" rows="4"  ></textarea>        
          </div>
    </div>
    
  </div>
  
</body>
</html>