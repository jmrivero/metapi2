<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Metapi service editor</title>
<script src="lib/codemirror.js"></script>
<style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript"></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript"></script>
<script src="libs/noty/layouts/top.js" type="text/javascript"></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript"></script>
<script src="libs/noty/themes/default.js" type="text/javascript"></script>
<script src="scripts/util/PopupManager.js" type="text/javascript"></script>

<link rel="stylesheet" href="css/codemirror.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">


<style>
#alerts {
	/*height: 63px;*/
	
}

#fileList {
	list-style: none;
	margin: 0px;
	padding: 0px;
	height: 535px;
	overflow-y: scroll;
}
</style>
<script src="lib/bootbox.min.js"></script>
<script type="text/javascript">
	var popupManager = new PopupManager();

	function showMessage(message, type) {
		if (type == "success") {
			popupManager.showSuccess(message);
		} else {
			popupManager.showError(message);
		}
	}
</script>




</head>
<body style="">

	<jsp:include page="parts/menu.jsp"></jsp:include>

	<script type="text/javascript">
		var scriptEditor;

		$(document).ready(function() {

			$("#exportButton").click(function(e) {
				e.preventDefault();
				window.location = "model"
			});

			$("#uploadZip").click(function(e) {
				var self = this;
				e.preventDefault();
				$("#zipFile").trigger("click");
			});
			
			$("#zipFile").change(function(e) {
				$("#zipUploadForm").submit();
			});

		});
	</script>

	<div class="container">
		<div id="alerts"></div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<form id="zipUploadForm" action="model" method="POST" enctype="multipart/form-data">
					<!-- <button class="btn btn-success" id="exportButton">Export</button> -->
					<button id="exportButton" class="btn btn-default ">Export <span class="glyphicon glyphicon-eject"></span></button>
					<input id="zipFile" type="file" name="file" style="display: none">
					<!-- <button id="uploadZip" class="btn">Upload model file</button> -->
					<button id="uploadZip" class="btn btn-default">Upload model file <span class="glyphicon glyphicon-compressed"></span></button>
				</form>
				<input type="file" id="file" file="file" style="display: none" />
			</div>
		</div>

	</div>

</body>
</html>