<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Metapi endpoint editor</title>
<script src="lib/codemirror.js"></script><style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript" ></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>

<link rel="stylesheet" href="css/codemirror.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<style>
	#alerts {
		/*height: 53px;*/
	}
  
  #fileList {
    list-style: none;
    margin: 0px;
    padding: 0px;
    height:  535px;
    overflow-y: scroll;
  }
</style>

</head>
<body style="">
  
<jsp:include page="parts/menu.jsp"></jsp:include>

<script src="lib/bootbox.min.js"></script>
  <script type="text/javascript">
    var endpointEditor;
    var currentFilename = null;
    var newFile = false;
    var endpoints;
    var selectedEndpointId;
    var popupManager = new PopupManager();
       
    function endpointSaved(filename, content) {
    	popupManager.showSuccess("Endpoint saved");
    	loadEndpoints();
    }
    
    function addEndpoint(endpoint) {
        $("#endpoints").append('<option value="[value]">[label]</option>'
		.replace("[value]",  endpoint.id)
		.replace("[label]", endpoint.method + " " + endpoint.urlRegex));
	endpoints[endpoint.id] = endpoint;
    }
    
    function cleanEndpointList() {
	$("#endpoints option").remove();
	$("#endpoints").append('<option value="(new)">(new)</option>');
    }
    
    function loadEndpoints() {
		cleanEndpointList();
		endpoints = {};
		$.ajax({url: "../metapi/endpoints", type: "GET", dataType: 'json', success: function(response) {
	            $.each(response.endpoints, function(_, endpoint) {
	            	 addEndpoint(endpoint);
	            	 if (selectedEndpointId) {
						$("#endpoints").val(selectedEndpointId);
			            	 } else {
						$("#endpoints").val("(new)");
	            	 }
	            });
	        }});
    }
    
    function cleanFields() {
		$("#method").val("GET");
		
		$("#urlRegex").val("");
		endpointEditor.setValue("");
		selectedEndpointId = null;
    }
    
    $(document).ready(function() {
    	
    	
    	
        endpointEditor = CodeMirror($("#editor")[0], {
            value: "",
            mode:  "javascript",
            lineNumbers: true
        });
        endpointEditor.setSize(1120, 450);
        
        $("#saveButton").click(function(e) {
        	
        	
        	
        	
        	var self = this;
			e.preventDefault();   
			var endpoint = endpoints[$("#endpoints").val()];
			var data = {
				method: $("#method").val(),
				urlRegex: $("#urlRegex").val(),
				script: endpointEditor.getValue()
			}
			if (!endpoint) {
				$.ajax({url: "/../metapi/endpoints", type: "POST", data: JSON.stringify(data), success: function(createdEndpoint) {
					selectedEndpointId = JSON.parse(createdEndpoint).id;
					endpointSaved($("#urlRegex").val(), endpointEditor.getValue());
				}});
			} else {
				$.ajax({url: "/../metapi/endpoints/" + endpoint.id, type: "PUT", data: JSON.stringify(data), success: function(response) {
					endpointSaved($("#urlRegex").val(), endpointEditor.getValue());
				}});
			}
        });
        
        $("#deleteButton").click(function(e) {
		var endpoint = endpoints[$("#endpoints").val()];
			if (!endpoint) {
				return;
			}
			$.ajax({url: "/../metapi/endpoints/" + endpoint.id, type: "DELETE", success: function(response) {
				popupManager.showSuccess("Endpoint deleted");
				cleanFields();
				loadEndpoints();
			}});
        });
        
        $("#endpoints").change(function() {
			endpoint = endpoints[$("#endpoints").val()];
			if (endpoint) {
				$("#method").val(endpoint.method);
				$("#urlRegex").val(endpoint.urlRegex);
				endpointEditor.setValue(endpoint.script);
				selectedEndpointId = endpoint.id;
			} else {
				cleanFields();
			}
        });
        
        loadEndpoints();
        
        $("#resetAPIButton").click(function() {
        	if (confirm("Are you sure?")) {
    			$.ajax({url: "/../metapi/endpoints", type: "DELETE", success: function(response) {
    				popupManager.showSuccess("API cleared");
    				cleanFields();
    				loadEndpoints();
    			}});
        	}
        });
        
    });
    
		</script>

  <div class="container">
    <div id="alerts"></div>
    <div class="row" id="area">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<select id="endpoints" class="form-control"></select>
				<!-- <button class="btn btn-danger" id="deleteButton">Delete</button> -->
				<!-- <button class="btn btn-info" id="resetAPIButton">Reset API</button> -->
				<button id="deleteButton" class="btn btn-danger">Delete <span class="glyphicon glyphicon-trash"></span></button>
				<button id="resetAPIButton" class="btn btn-info">Reset API <span class="glyphicon glyphicon-off"></span></button>
    	</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		  <select class="form-control" id="method">
		    <option>GET</option>
		    <option>POST</option>
		    <option>PUT</option>
		    <option>DELETE</option>
		  </select>  	  
          <input type="text" class="form-control" placeholder="Text input" id="urlRegex"></input>
          <button class="btn btn-primary btn-sm" id="saveButton">Save <span class="glyphicon glyphicon-floppy-disk"></span></button>
          <!-- <button class="btn btn-primary" id="saveButton">Save</button>  -->   
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
          <div id="editorContainer">
            <div id="editor"></div>
          </div>
        </div>
      </div>
    </div>
    
    
    
    <div class="row">
    
    
    
    </div>
  

  
  
  </div>      
  
</body>
</html>