var PopupManager = Class.extend({

	init: function(){
	
	},
	
	showSuccess: function(text) {
		noty({text: text, layout:'topRight',  type:'success', timeout: 2000});
	},
	
	showNotification: function(text) {
		noty({text: text, layout:'topRight',  type:'warning', timeout: 2000});
	},
	
	showError: function(text) {
		noty({text: text, layout:'topRight',  type:'error', timeout: 2000});
	}

});