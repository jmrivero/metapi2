var MockApiModel = ModelObject.extend({

	init: function() {
		this._super();
		this.mockups = [];
	},
	
	addMockup: function(mockup) {
		this.mockups.push(mockup);
	},
	
	removeMockup: function(mockup) {
		var index = this.mockups.indexOf(mockup);
		if (index != -1) {
			this.mockups.splice(index, 1);
		}
	},
	
	getMockups: function() {
		return this.mockups.concat();
	}

});