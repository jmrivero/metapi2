var Mockup = ModelObject.extend({
			
	init: function(id) {
		this._super();
		this.setId(id);
		this.features = [];
		this.featuresById = {};
		this.associations = [];
		this.mockupSource = null;
	},

	addFeature: function(feature) {
		////debugger;
		this.features.push(feature);
		this.featuresById[feature.getId()] = feature;
	},
	
	removeFeature: function(feature) {
		var index = this.features.indexOf(feature);
		if (index != -1) {
			this.features.splice(index, 1);
		}
	},
	
	getFeatureById: function(id) {
		return this.featuresById[id];
	},
	
	addAssociation: function(association) {
		this.associations.push(association);
	},
	
	removeAssociation: function(association) {
		var index = this.associations.indexOf(association);
		if (index != -1) {
			this.associations.splice(index, 1);
		}
	},
	
	getFeatures: function() {
		return this.features.concat();
	},
	
	getAssociations: function() {
		return this.associations.concat();
	},
	
	setMockupSource: function(source) {
		//debugger;
		this.mockupSource = source;
	},
	
	getMockupSource: function() {
		//debugger;
		return this.mockupSource;
	}
	
});