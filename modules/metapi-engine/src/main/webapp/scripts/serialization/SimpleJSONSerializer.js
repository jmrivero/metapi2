var SimpleJSONSerializer = Class.extend({

	init: function(annotationParser) {
		this.annotationParser = annotationParser;
	},
	
	serialize: function(model) {
		var	result = {resources: [], associations: []};
		for (i in model.getMockups()) {
			var mockup = model.getMockups()[i];
			result = this.serializeFeatures(result, mockup.getFeatures());
			result = this.serializeAssociations(result, mockup.getAssociations());
		}
		return JSON.stringify(result);
	},
	
	serializeFeatures: function(result, features) {
		for (i in features) {
			var feature = features[i];
			try {
				var featureContent = this.annotationParser.parse(feature.getContent());		
				if (!_.contains(result.resources, featureContent["class"])) {
					result.resources.push(featureContent["class"])
				}
			} catch (e) {}
		}
		return result;
	},
	
	serializeAssociations: function(result, associations) {
		for (i in associations) {
			var assoc = associations[i];
			try {
				var sourceClass = this.annotationParser.parse(assoc.getFeature1().getContent())["class"];
				var destClass = this.annotationParser.parse(assoc.getFeature2().getContent())["class"];
				if (
					_.filter(result.associations, function(a) { 
						a.source == sourceClass && a.destination == destClass}).length == 0
				) {
					result.associations.push({
						source: sourceClass, 
						destination: destClass,
						name: destClass + "s"
					})
				}
			} catch (e) {}
		}
		return result;
	},
	
	deserialize: function(model) {
		throw new "Not supported";
	}

});