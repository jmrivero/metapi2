var ModelXMLSerializer = Class.extend({

	init: function() {
		this.nextId = 0;
		this.x2js = new X2JS();
	},
	
	serialize: function(model) {
		result = "<mockapiModel>";
		for (i in model.getMockups()) {
			var mockup = model.getMockups()[i];
			result += '<element type="mockup" id="$mockupId"><representation>$representation</representation><features>'
				.replace("$mockupId", mockup.getId())
				.replace("$representation", mockup.getMockupSource() ? mockup.getMockupSource() : "");
			result = this.serializeFeatures(result, mockup.getFeatures());
			result = this.serializeAssociations(result, mockup.getAssociations());
			result += '</features></element>'
		}
		result += "</mockapiModel>";
		return result;
	},
	
	serializeFeatures: function(result, features) {
		for (i in features) {
			var feature = features[i];
			if (feature.getId() == null) {
				feature.setId(this.getNextId());
			}
			var boundaries = feature.getRepresentation().getLastBoundaries();
			result += '<$featureType $typeSpec="$type" id="$id" accessType="$accessType" left="$left" top="$top" width="$width" height="$height"'
				.replace("$featureType", feature.type)
				.replace("$typeSpec", feature.type == "item" ? "ofType" : "of")
				.replace("$type", feature["class"])
				.replace("$accessType", feature.accessMode)
				.replace("$id", feature.getId())
				.replace("$left", Math.round(boundaries.left))
				.replace("$top", Math.round(boundaries.top))
				.replace("$width", Math.round(boundaries.width))
				.replace("$height", Math.round(boundaries.height));
			for (i in feature.listFeatures) {
				listFeature = feature.listFeatures[i];
				if (listFeature == "sorting") {
					result += ' allowSorting="true"';
				}
				if (listFeature == "ordering") {
					result += ' allowOrdering="true"';
				}
				if (listFeature == "filtering") {
					result += ' allowFiltering="true"';
				}
				if (listFeature == "paginating") {
					result += ' allowPagination="true"';
				}
				if (listFeature == "selecting") {
					result += ' allowSelection="true"';
				}
			}
			result += "/>";
			if (feature.navigationTo) {
				result += '<navigation to="$to" />'.replace("$to", feature.navigationTo);
			}
		}
		return result;
	},
	
	serializeAssociations: function(result, associations) {
		for (i in associations) {
			var association = associations[i];
			result += '<association source="$source" destination="$destination" />'
				.replace("$source", association.getFeature1().getId())
				.replace("$destination", association.getFeature2().getId());
		}
		return result;
	},
	
	deserializeModel: function(xml) {
		var self = this;
		var json = this.x2js.xml2json(this.x2js.parseXmlString(xml));
		var model = new MockApiModel();
		_.each(json.mockapiModel.element_asArray, function(element) {
			model.addMockup(self.parseMockup(element));
		});
		return model;
	},
	
	parseMockup:  function(element) {
		var self = this;
		var mockup = new Mockup();
		mockup.setId(element._id);
		var featuresById = {};
		_.each(element.features.item_asArray, function(featureRepresentation) {
			var featureObject = self.parseItemOrList("item", featureRepresentation);
			mockup.addFeature(featureObject);
			featuresById[featureObject.getId()] = featureObject;
		});
		_.each(element.features.list_asArray, function(featureRepresentation) {
			var featureObject = self.parseItemOrList("list", featureRepresentation);
			mockup.addFeature(featureObject);
			featuresById[featureObject.getId()] = featureObject;
		});
		_.each(element.features.association_asArray, function(featureRepresentation) {
			mockup.addAssociation(self.parseAssociation(featureRepresentation, featuresById, mockup));
		});
		if (typeof element.representation == "string") {
			mockup.setMockupSource(element.representation);
		}
		return mockup;
	},
	
	parseItemOrList: function(type, feature) {
		var featureObject = new Feature.newFromData({
			type: type,
			accessMode: feature._accessType,
			"class": feature._ofType ? feature._ofType : feature._of
		});
		featureObject.setId(parseInt(feature._id));
		if (type == "list") {
			featureObject.listFeatures = [];
			if (feature._allowSorting) {
				featureObject.listFeatures.push("sorting");
			}
			if (feature._allowFiltering) {
				featureObject.listFeatures.push("filtering");
			}
			if (feature._allowOrdering) {
				featureObject.listFeatures.push("ordering");
			}
			if (feature._allowPagination) {
				featureObject.listFeatures.push("paginating");
			}
			if (feature._allowSelection) {
				featureObject.listFeatures.push("selecting");
			}
		}
		//////debugger;
		featureObject.setRepresentation({left: feature._left, top: feature._top, width: feature._width, height: feature._height});
		return featureObject;
	},
	
	parseAssociation: function(feature, featuresById, mockup) {
		var association = new Association(featuresById[feature._source], featuresById[feature._destination], mockup);
		association.setId(parseInt(feature._id));
		return association;
	},
	
	getNextId: function() {
		return new Date().getTime() + this.nextId++;
	}

});