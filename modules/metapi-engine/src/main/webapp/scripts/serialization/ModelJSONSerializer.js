var ModelJSONSerializer = Class.extend({

	init: function() {
		this.nextId = 0;
	},
	
	serialize: function(model) {
		var json = {mockups: []};
		for (i in model.getMockups()) {
			var mockup = model.getMockups()[i];
			var jsonMockup = {name: mockup.getId(), representation: mockup.getMockupSource(), features: [], associations: []};
			json.mockups.push(jsonMockup);				
			this.serializeFeatures(mockup.getFeatures(), jsonMockup);
			this.serializeAssociations(mockup.getAssociations(), jsonMockup);
		}
		return JSON.stringify(json);
	},
	
	serializeFeatures: function(features, jsonMockup) {
	for (i in features) {
			var feature = features[i];
			if (feature.getId() == null) {
				feature.setId(this.getNextId());
			}
			var boundaries = feature.getRepresentation().getLastBoundaries();
			jsonMockup.features.push({	
				id: feature.getId(), 
				content: feature.getContent(),
				left: Math.round(boundaries.left), 
				top: Math.round(boundaries.top), 
				width: Math.round(boundaries.width), 
				height: Math.round(boundaries.height),
			}); 
		}
	},
	
	serializeAssociations: function(associations, jsonMockup) {
		for (i in associations) {
			var association = associations[i];
			jsonMockup.associations.push({
				id: association.id,
				source: association.getFeature1().getId(), 
				destination: association.getFeature2().getId() 
			});	
		}
	},
		
	getNextId: function() {
		return new Date().getTime() + this.nextId++;
	},
	
	deserializeModel: function(jsonText) {
		var self = this;
		var json = JSON.parse(jsonText);
		var model = new MockApiModel();
		_.each(json.mockups, function(mockup) {
			model.addMockup(self.parseMockup(mockup));
		});
		return model;
	},
	
	parseMockup:  function(mockup) {
		var self = this;
		var mockupObject = new Mockup(mockup.name);
		var featuresById = {};
		_.each(mockup.features, function(feature) {
			var featureObject = new Feature(feature.content, feature.id);
			featureObject.setRepresentation({left: feature.left, top: feature.top, width: feature.width, height: feature.height});
			mockupObject.addFeature(featureObject);
			featuresById[featureObject.getId()] = featureObject;
		});
		_.each(mockup.associations, function(association) {
			mockupObject.addAssociation(new Association(featuresById[association.source], featuresById[association.destination], mockupObject));
		});
		if (typeof mockup.representation == "string") {
			mockupObject.setMockupSource(mockup.representation);
		}
		return mockupObject;
	},
	
	getNextId: function() {
		return new Date().getTime() + this.nextId++;
	}

});