var Figure = Class.extend({

	init: function() {
		this.eventListeners = {};
		this.connections = [];
		this.handlers = [];
		this.movable = false;
		this.resizable = false;
		this.rootElement = null;
		this.model = null;
		this.fixedBoundaries = null;
		this.lastBoundaries = null;
	},

	registerEvent: function(eventName) {
		this.eventListeners[eventName] = [];
	},
	
	addEventListener: function(eventName, f) {
		if (!this.eventListeners[eventName]) {
			throw ("Event " + eventName + " does not exist");
		}
		this.eventListeners[eventName].push(f);
	},
	
	removeEventListener: function(eventName, f) {
		if (!this.eventListeners[eventName]) {
			throw ("Event " + eventName + " does not exist");
		}
		var index = this.eventListeners.indexOf(f);
		if (index != -1) {
			this.eventListeners.splice(index, 1);
		}
	},
	
	notifyEvent: function(eventName, eventData) {
		for (i in this.eventListeners[eventName]) {
			this.eventListeners[eventName][i](eventData);
		}
	},
	
	setRootElement: function(rootElement) {
		this.rootElement = rootElement;
	},
	
	remove: function() {
		
		$(this.rootElement).remove();
		for (i in this.connections) {
			this.connections[i].remove();
		}
	},

	getModel: function(model) {
		return this.model;
	},
	
	setModel: function(model) {
		this.model = model;
		model.setRepresentation(this);
	},
	
	addConnection: function(connection) {
		this.connections.push(connection);
	},
	
	removeConnection: function(connection) {
		var index = this.connections.indexOf(connection);
		if (index != -1) {
			this.connections.splice(index, 1);
		}
	},
	
	makeMovable: function() {
		if (!this.movable) {
			var self = this;
			this.registerEvent('move');
			$(this.rootElement).draggable({
				drag: function(event, ui) {
					self.notifyEvent('move', ui.position);
					self.setBoundaries({left: ui.position.left, top: ui.position.top, width: $(self.rootElement).width(), height: $(self.rootElement).height()});
				}
			});
		}
		this.movable = true;
	},
	
	makeResizable: function() {
		if (!this.resizable) {
			var self = this;
			this.registerEvent('resize');
			$(this.rootElement).resizable({
				resize: function(event, ui) {
					self.notifyEvent('resize', ui.position);
					self.setBoundaries({left: ui.position.left, top: ui.position.top, width: $(self.rootElement).width(), height: $(self.rootElement).height()});
				}
			});
		}
		this.resizable = true;
	},
	
	enableMouseEnterAndLeaveEvents: function() {
		var self = this;
		this.registerEvent('mouseEnter');
		$(this.rootElement).mouseenter(function(e) {
			self.notifyEvent('mouseEnter', e);
		});
		this.registerEvent('mouseLeave');
		$(this.rootElement).mouseleave(function(e) {
			self.notifyEvent('mouseLeave', e);
		});
	},
	
	isMovable: function() {
		return this.movable;
	},
	
	isResizable: function() {
		return this.resizable;
	},
	
	getCenter: function() {
		var position = $(this.rootElement).position();
		return {
			left: position.left + $(this.rootElement).width() / 2,
			top: position.top + $(this.rootElement).height() / 2
		}
	},
	
	getBoundaries: function() {
		var position = $(this.rootElement).position();
		var boundaries = {
			left: position.left,
			top: position.top,
			width: $(this.rootElement).width(),
			height: $(this.rootElement).height()
		}
		return boundaries;
	},
	
	setBoundaries: function(rect) {
		this.lastBoundaries = rect;
		console.log(this.lastBoundaries);
		if (rect.width) {
			$(this.rootElement).width(rect.width);
		}
		if (rect.height) {
			$(this.rootElement).height(rect.height);
		}
		if (rect.left) {
			$(this.rootElement).css({left: rect.left});
		}
		if (rect.top) {
			$(this.rootElement).css({top: rect.top});
		}

	},
	
	getPath: function() {
		var b = this.getBoundaries();
		return [
			{x: b.left, y: b.top},
			{x: b.left + b.width, y: b.top},
			{x: b.left + b.width, y: b.top + b.height},
			{x: b.left, y: b.top + b.height},
		]
	},
	
	getLastBoundaries: function() {
		return this.lastBoundaries;
	}
	
});