var ConnectionHandlerFigure = Figure.extend({

	init: function(canvas, figure, model, connectionState) {
		this.setModel(model);
		this.canvas = canvas;
		this.figure = figure;
		this.connectionState = connectionState;
		this.initialize(figure);
		this.isActive = false;
		this.focused = false;
	},
	
	initialize: function(figure) {
		var self = this;
		//this.root = $('<div class="connectionHandler" style="display: none">/</div>');
		this.root = $('<img src="images/link.png" class="connectionHandler" style="display: none"/>');
		this.setRootElement(this.root);
		$(this.canvas).append(this.root);
		this.redraw();
		this.figure.addEventListener("move", function() {
			self.redraw();
		});
		this.figure.addEventListener("remove", function() {
			self.remove();
		});
		this.figure.addEventListener("mouseEnter", function() {
			self.redraw();
			self.root.show(0);
		});
		this.figure.addEventListener("mouseLeave", function() {
			self.redraw();
			setTimeout(function() {
				if (self.isActive) {
					return;
				}
				if (self.focused) {
					self.focused = false;
					return;
				}
				self.root.hide(0);
			}, 50);
		});
		this.root.mouseenter(function() {
			self.focused = true;
		});
		this.root.mouseleave(function() {
			if (self.isActive) {
				return;
			}
			self.focused = false;
			self.root.hide(0);
		});
		this.root.click(function() {
			self.isActive = false;
			if (!self.connectionState.source) {
				self.connectionState.source = self;
				self.root.addClass("selectedConnectionHandler");
				self.isActive = true;
			} else if (self.connectionState.source == self) {
				self.connectionState.source = null;
				self.root.removeClass("selectedConnectionHandler");
			} else {
				debugger;
				var association = new Association(self.connectionState.source.getFigure().getModel(), self.getFigure().getModel(), self.getModel());
				self.getModel().addAssociation(association);
				var connection = new LineConnectionFigure(self.canvas, self.connectionState.source.getFigure(), self.getFigure(), association);
				connection.addEventListener('remove', function() {
					self.getModel().removeAssociation(association);
				});	
				self.connectionState.source.clearState();
				self.connectionState.source = null;		
				new RemoveHandlerFigure(self.canvas, connection, RemoveHandlerFigure.positions.CENTER);
			}
		});
	},
	
	clearState: function() {
		this.root.removeClass("selectedConnectionHandler");
		this.isActive = false;
	},
	
	redraw: function() {
		var boundaries = this.figure.getBoundaries();
		this.root.css({
			left: boundaries.left,
			top: boundaries.top
		});		
	},
	
	getFigure: function() {
		return this.figure;
	}

});