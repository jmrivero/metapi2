MockApiModelEditorFigure = Figure.extend({
	
	init: function(canvas, model, store, popupManager, parser) {
		this._super();
		this.parser = parser;
		this.xmlSerializer = new ModelXMLSerializer();
		this.jsonSerializer = new SimpleJSONSerializer(parser);
		this.registerEvent('xmlGenerated');
		this.registerEvent('deployAPI');
		this.registerEvent('resetAPI');
		this.mockupEditorFigures = [];
		this.tabCounter = 0;
		this.drawOn(canvas);
		this.store = store;
		this.popupManager = popupManager;
		this.setModel(model);
	},
	
	drawOn: function(canvas) {
		var self = this;
		this.body = $(
			'<div class="mockapiEditor">' +
				'<button class="newMockup">New mockup</button>' +
				'<button class="generateXML">Generate XML</button>' +
				'<button class="saveModel">Save</button>' +
				'<button class="loadModel">Load</button>' +
				'<button class="deployAPI">Deploy API</button>' +
				'<button class="resetAPI">Reset API</button>' +
				'<ul class="tabHeaders"></ul>' +
			'</div>');
		this.setRootElement(this.body);
		this.tabHeaders = this.body.find(".tabHeaders");
		this.body.find(".newMockup").click(function() {
			self.newMockup();
		});
		this.body.find(".generateXML").click(function() {
			self.notifyEvent('xmlGenerated', self.xmlSerializer.serialize(self.getModel()));
		});
		this.body.find(".deployAPI").click(function() {
			self.notifyEvent('deployAPI', self.jsonSerializer.serialize(self.getModel()));
		});
		this.body.find(".resetAPI").click(function() {
			self.notifyEvent('resetAPI');
		});
		this.body.find(".saveModel").click(function() {
			/*
			var modelId = prompt("Enter a name for your MockApi project");
			if (!modelId) {
				return;
			}
			*/
			self.store.storeModel("defaultModel", self.getModel());
			self.popupManager.showSuccess("Model saved");
			//console.log(self.xmlSerializer.deserializeModel(self.xmlSerializer.serialize(self.getModel())));
		});
		this.body.find(".loadModel").click(function() {
			/*
			var modelId = prompt("Enter a name for the MockApi project to load");
			if (!modelId) {
				return;
			}
			*/
			self.store.loadModel("defaultModel", function(model) {
				if (!model) {
					/*
					if (!self.popupManager) {
						alert("No MockAPI model has been found with the id '$name'".replace("$name", modelId));
					} else {
						self.popupManager.showError("No MockAPI model has been found with the id '$name'".replace("$name", modelId));
					}*/
					self.popupManager.showError("No MockAPI model has been saved yet");
					return;
				}
				self.popupManager.showSuccess("Model loaded");
				self.clearTabs();
				self.setModel(model);
			});
		});
		this.body.find("button").button();
		canvas.append(this.body);
		this.body.tabs();
	},
	
	setModel: function(model) {
		this._super(model);
		var self = this;
		_.each(model.getMockups(), function(mockup) {
			self.addMockup(mockup);
		});
	},
	
	newMockup: function(mockupId) {
		if (!mockupId) {
			mockupId = this.promptMockupId();
		}
		if (!mockupId) {
			return;
		}
		var self = this;
		var mockup = new Mockup();
		mockup.setId(mockupId);
		this.getModel().addMockup(mockup);
		this.addMockup(mockup);
	},
	
	addMockup: function(mockup) {
		var self = this;
		var tabId = this.tabCounter++;
		var newTabHeader = $(
			'<li class="mockapi-editor-tab"><a id="mockapi-editor-tab-$tabId" href="#tabs-$tabId">$tabName</a> <span class="ui-icon ui-icon-close deleteMockup" role="presentation">Remove Tab</span></li>'
				.replace(/\$tabId/g, tabId)
				.replace("$tabName", mockup.getId()));
		this.tabHeaders.append(newTabHeader);
		var newTabContent = $('<div id="tabs-' + tabId + '" class="mockapi-editor-tab-content"><p></p></div>');
		this.body.append(newTabContent);
		mockup.addEventListener('change', function() {
			newTabHeader.find("#mockapi-editor-tab-" + tabId).html(mockup.getId());
		});
		new MockupEditorFigure(newTabContent, mockup, this.parser);
		newTabHeader.find(".deleteMockup").click(function() {
			newTabHeader.remove();
			newTabContent.remove();
			self.body.tabs("refresh");
			self.getModel().removeMockup(mockup);
		});
		newTabHeader.find("#mockapi-editor-tab-" + tabId).dblclick(function() {
			self.promptMockupId(mockup);
		});
		this.body.tabs("refresh");	
		this.body.tabs("option", "active", this.getModel().mockups.length - 1);
	},
	
	deleteMockup: function(mockup) {
		
	},
	
	promptMockupId: function(mockup) {
		var mockupId = prompt("New mockup id", mockup ? mockup.getId() : "").replace(" ", "");
		if (!mockupId) {
			return;
		}
		if (mockup) {
			mockup.setId(mockupId);
		}
		return mockupId;
	},
	
	clearModel: function() {
		_.each(this.getModel().getMockups(), function(mockup) {
			mockup.getRepresentation().remove();
		});
	},
	
	clearTabs: function() {
		this.body.tabs("destroy");	
		this.body.find("li").remove();
		this.body.find(".mockapi-editor-tab-content").remove();
		this.body.tabs();
	},
	
	recreateRepresentation: function() {
		_.each(this.getModel().getMockups(), function(mockup) {
			mockup.remove();
		});
	}
	
	
});