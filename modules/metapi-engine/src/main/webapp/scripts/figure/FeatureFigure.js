var FeatureFigure = Figure.extend({

	init: function(canvas, initialText, model, parser) {
		this._super();
		this.registerEvent('remove');
		this.setModel(model);
		this.text = initialText;
		this.parser = parser;
		this.drawOn(canvas);
	},
	
	drawOn: function(canvas) {
		this.body = $('<div class="feature"><textarea class="featureInput" style="display: none" /><div class="featureText" /></div>');
		var self = this;
		this.setRootElement(this.body);
		this.makeMovable();
		this.makeResizable();
		$(this.body).css("position", "absolute");
		this.input = this.body.find(".featureInput");
		this.textbox = this.body.find(".featureText");
		this.textbox.dblclick(function() {
			self.textbox.hide();
			self.input.show();
			self.input.focus();
		});
		this.input.focusout(function() {
			self.setText(self.input.val());
		});
		this.setText(this.text);
		$(canvas).append(this.body);
		this.enableMouseEnterAndLeaveEvents();
	},
	
	setText: function(text) {
		this.text = text;
		this.input.val(text);
		this.input.hide();
		this.textbox.html(text.replace(/\n/g, "<br />"));
		this.textbox.show();
		this.body.removeClass("featureWithErrors");
		try {
			this.parser.parse(text);
			this.getModel().setContent(text);
		} catch (e) {
			this.body.addClass("featureWithErrors");
		}
	},
	
	storeInModel: function(data) {
		this.getModel().setContent(data);
	},
	
	remove: function() {
		this._super();
		this.notifyEvent('remove', this.getModel());
	},
	
	setModel: function(model) {
		this._super(model);
	}
	
});
