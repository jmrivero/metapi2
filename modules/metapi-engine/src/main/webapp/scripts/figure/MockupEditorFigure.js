MockupEditorFigure = Figure.extend({

	init: function(canvas, model, parser) {
		this._super();
		this.canvas = canvas;
		this.parser = parser;
		this.drawOn(canvas);
		this.connectionState = {};
		this.setModel(model);
	},
	
	drawOn: function(canvas) {
		var self = this;
		this.body = $(
			'<div class="mockupEditor">' +
				"<label>Mockup URL:</label>" +
				"<div>" +
					"<input class='imageUrl' type='text' style='width: 500px'></input>" +
				"</div>" +
				"<div>" +
					"<button class='addItem ui-button-text'>Add Item</button>" +
					"<button style=\"display: none\" class='addList ui-button-text'>Add List</button>" +
				"</div>" +
				"<div class='mockup'></div>" +
					"<div class='mockupCanvas' src='' />" +
					"<img class='mockupImage' src='' />" +
				"</div>" +
			"</div>"
		);
		this.setRootElement(this.body);
		$(canvas).append(this.body);
		this.mockupCanvas = this.body.find(".mockupCanvas");
		this.body.find(".imageUrl").change(function() {
			self.body.find(".mockupImage").attr("src", self.body.find(".imageUrl").val());
			self.getModel().setMockupSource(self.body.find(".imageUrl").val());
		});
		this.body.find(".addItem").click(function() {
			self.createFeatureAndFigure("item(Class)");
		});
		this.body.find(".addList").click(function() {
			self.createFeatureAndFigure("list(Class)");
		});
		this.body.find("button").button();
	},
	
	setModel: function(model) {
		var self = this;
		this._super(model);
		_.each(model.getFeatures(), function(feature) {
			self.createFigureForFeature(feature);	
		});
		_.each(model.getAssociations(), function(association) {
			var connection = new LineConnectionFigure(self.canvas, association.getFeature1().getRepresentation(), association.getFeature2().getRepresentation(), association);
			new RemoveHandlerFigure(self.canvas, connection, RemoveHandlerFigure.positions.CENTER);
		});
		if (model.getMockupSource()) {
			this.body.find(".imageUrl").val(model.getMockupSource()).trigger('change');
		}
	},
	
	createFeatureAndFigure: function(text) {
		var self = this;
		var feature = new Feature();
		var figure = new FeatureFigure(this.mockupCanvas, text, feature, this.parser);
		this.getModel().addFeature(feature);
		figure.addEventListener('remove', function(feature) {
			self.getModel().removeFeature(feature);
		});
		new ConnectionHandlerFigure(this.mockupCanvas, figure, this.getModel(), this.connectionState);
		new RemoveHandlerFigure(this.mockupCanvas, figure, RemoveHandlerFigure.positions.TOP_RIGHT);
		figure.setBoundaries({left: this.mockupCanvas.position().left, top: this.mockupCanvas.position().top});
		return {figure: figure, feature: feature};
	},
	
	createFigureForFeature: function(feature) {
		var self = this;
		var boundaries = feature.getRepresentation();
		var figure = new FeatureFigure(this.mockupCanvas, feature.getContent(), feature, this.parser);
		figure.addEventListener('remove', function(feature) {
			self.getModel().removeFeature(feature);
		});
		figure.setText(feature.getContent());
		new ConnectionHandlerFigure(this.mockupCanvas, figure, this.getModel(), this.connectionState);
		new RemoveHandlerFigure(this.mockupCanvas, figure, RemoveHandlerFigure.positions.TOP_RIGHT);
		figure.setBoundaries({
			left: boundaries.left,
			top: boundaries.top,
			width: boundaries.width,
			height: boundaries.height
		});
		feature.setRepresentation(figure);
		return figure;
	}
	
});
