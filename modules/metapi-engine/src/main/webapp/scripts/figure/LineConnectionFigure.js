var LineConnectionFigure = Figure.extend({

	init: function(canvas, figure1, figure2, model) {
		this._super();
		this.setModel(model);
		this.setFigure1(figure1);
		this.setFigure2(figure2);
		this.arrowSize = 30;
		this.drawOn(canvas, figure1, figure2);
		this.registerEvent('move');
		this.registerEvent('resize');
		this.registerEvent('remove');
	},
	
	drawOn: function(canvas, figure1, figure2) {
		var self = this;
		if (!figure1.isMovable()) {
			throw "figure1 is not movable";
		};
		if (!figure2.isMovable()) {
			throw "figure2 is not movable";
		}
		this.body = $("<div>");
		this.canvas = $('<canvas class="connection">');
		this.arrowCanvas = $('<canvas class="arrow">');
		canvas.append(this.canvas);
		canvas.append(this.arrowCanvas);		
		this.setRootElement(this.canvas);
		var notifyMoveAndResize = function(position) {
			self.redraw();
			self.notifyEvent('move', position);
			self.notifyEvent('resize', position);
		};
		figure1.addEventListener('move', notifyMoveAndResize);
		figure2.addEventListener('move', notifyMoveAndResize);
		figure1.addEventListener('resize', notifyMoveAndResize);
		figure2.addEventListener('resize', notifyMoveAndResize);
		self.redraw();
		this.enableMouseEnterAndLeaveEvents();
	},
	
	setFigure1: function(figure1) {
		this.figure1 = figure1;
		figure1.addConnection(this);
	},
	
	setFigure2: function(figure2) {
		this.figure2 = figure2;
		figure2.addConnection(this);
	},
	
	redraw: function() {
		var position1 = this.figure1.getCenter();
		var position2 = this.figure2.getCenter();	
		var width = position2.left - position1.left
		var height = position2.top - position1.top;
		var left = position1.left;
		var top = position1.top;
		var collision1 = this.lineCollisionWithPath({x1: left, y1: top, x2: left + width, y2: top + height}, this.figure1.getPath());
		var collision2 = this.lineCollisionWithPath({x1: left, y1: top, x2: left + width, y2: top + height}, this.figure2.getPath());
		var inverse = false;
		if (collision1) {
			left = collision1.x;
			top = collision1.y;
		}
		if (collision2) {
			if (!collision1) {
				return;
			}
			width = collision2.x - collision1.x;
			height = collision2.y - collision1.y;
		}		
		var originalBoundaries = {left: left, top: top, width: width, height: height};
		if (width < 0) { // figure1 is at the left of figure2
			left = left + width;
			width = -width;
			inverse = !inverse;
		}
		if (height < 0) {
			top = top + height;
			height = -height;
			inverse = !inverse;
		}		
		this.canvas.css({left: left,  top: top});
		this.canvas[0].width = width;
		this.canvas[0].height = height;
		this.canvas.css({position: 'absolute'});
		var line = null;
		
		if (!inverse) {
			line = {x1: 0, y1: 0, x2: width, y2: height};
		} else {
			line = {x1: 0, y1: height, x2: width, y2: 0};
		}
		this.drawLine(line);
		this.drawArrow({x1: originalBoundaries.left, x2: originalBoundaries.left + originalBoundaries.width,
		                  y1: originalBoundaries.top, y2: originalBoundaries.top + originalBoundaries.height}, originalBoundaries);
	}, 
	
	drawLine: function(line) {
		var context = this.canvas[0].getContext("2d");
		context.lineWidth = 2;
		context.clearRect(0, 0, this.canvas[0].width, this.canvas[0].height);
		context.beginPath();
		context.moveTo(line.x1, line.y1);
		context.lineTo(line.x2, line.y2);
		context.stroke();
	},
	
	drawArrow: function(line, bound) {

		var canvas = this.arrowCanvas[0];
		var context = canvas.getContext("2d");
		
		canvas.height = this.arrowSize;
		canvas.width = this.arrowSize;
		$(canvas).css({left: bound.left + bound.width - this.arrowSize / 2, top: bound.top + bound.height - this.arrowSize / 2});
		var context = this.arrowCanvas[0].getContext("2d");
		context.beginPath();
		context.lineWidth = 2;
		this.drawArrowInContext(context, line.x1, line.y1, line.x2, line.y2);
		context.stroke();
	},
	
	drawArrowInContext: function(context, fromx, fromy, tox, toy){
		var dx = tox-fromx;
		var dy = toy-fromy;
		var angle = Math.atan2(dy,dx);
		//context.moveTo(fromx, fromy);
		var arrowSize = this.arrowSize / 2;
		context.moveTo(arrowSize, arrowSize);
		context.lineTo((1 - Math.cos(angle - Math.PI / 6)) * arrowSize,(1 - Math.sin(angle - Math.PI / 6)) * arrowSize);
		context.moveTo(arrowSize, arrowSize);
		context.lineTo((1 - Math.cos(angle + Math.PI / 6)) * arrowSize,(1 - Math.sin(angle + Math.PI / 6)) * arrowSize);
	},
	
	lineCollisionWithPath: function(line, path) {
		if (path.length < 1) {
			return null;
		}
		for (var i = 0; i < path.length; i++) {
			var pathLine = {
				x1: path[i].x, 
				y1: path[i].y, 
				x2: path[i + 1 == path.length ? 0 : i + 1].x, 
				y2: path[i + 1 == path.length ? 0 : i + 1].y};
			collision = this.lineCollision(line, pathLine);
			if (collision) {
				return collision;
			}
		}
		return null;
	},
	
	distance: function(point1, point2) {
		return Math.sqrt(Math.abs(point1.x - point2.x) ^ 2 + Math.abs(point1.y - point2.y) ^ 2)
	},
	
	between: function(a, n, b) {
		return ((a <= n) && (n <= b)) || ((b <= n) && (n <= a));
	},
	
	lineCollision: function(line1, line2) {
		var slope1 = null;
		var slope2 = null;
		if (line1.x1 != line1.x2) {
			slope1 = (line1.y2 - line1.y1) / (line1.x2 - line1.x1);
		}
		if (line2.x1 != line2.x2) {
			slope2 = (line2.y2 - line2.y1) / (line2.x2 - line2.x1);
		}
		if (slope1 != null && slope2 != null) {
			if (slope1 == slope2) {
				return null;
			}
			var b1 = line1.y1 - slope1 * line1.x1;
			var b2 = line2.y1 - slope2 * line2.x1;
			collisionX = (b2 - b1) / (slope1 - slope2)
			collisionY = slope2 * collisionX + b2;		
			if (this.between(line2.x1, collisionX, line2.x2) && this.between(line1.x1, collisionX, line1.x2))
			{
				return {x: collisionX, y: slope1 * collisionX + b1};
			}
		}
		if (slope1 != null && slope2 == null) {
			var b1 = line1.y1 - slope1 * line1.x1;
			var y = slope1 * line2.x1 + b1;
			var x = line2.x1;
			if (this.between(line2.y1, y, line2.y2) && this.between(line1.x1, x, line1.x2)) {
				return {x: x, y: y};
			}
		}
		if (slope1 == null && slope2 != null) {
			var b2 = line2.y1 - slope2 * line2.x1;
			var y = slope2 * line1.x1 + b2;
			var x = line1.x1;
			if (this.between(line1.y1, y, line1.y2) && this.between(line2.x1, x, line2.x2)) {
				return {x: x, y: y};
			}
		}
		return null;
	},
	
	remove: function() {
		this._super();
		this.getModel().setFeature1(null);
		this.getModel().setFeature2(null);
		this.getModel().getMockup().removeAssociation(this.getModel());
		this.arrowCanvas.remove();
		this.notifyEvent('remove');
	}
	
});