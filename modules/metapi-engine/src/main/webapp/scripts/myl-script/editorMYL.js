var templateEditor, modalEditor;
var currentFilename = null;
var newFile = false;
var applicationId = 2;
var popupManager = new PopupManager();
var editorMYLPath = "../../metapi/MYL/1/editorMYL/"
var fileSystemPath = "../../metapi/MYL/1/"
var inferPath = "../../metapi/MYL/1/infer/"
var delPath = "../../metapi/MYL/1/del/"
var renderPath = "../../metapi/MYL/1/render/"
var models = "../../metapi/MYL/1/models/"
var recipes = "../../metapi/MYL/1/rules/"
var templateEditor;  
var href = jQuery(location).attr('href').split("/");
var name = href[5];  	
var file;
var oldRecipe;
var contenido;
var idleInterval;
var sel = [];
var fileFT = null;	
var extensionModeMappings = {
	js: "javascript",
	xml: "xml",
	html: "htmlmixed",
	htm: "htmlmixed",
	css: "css",
	tl: "dust",
	json: "json"
}
	
	function extractExtension(filename) {
		var parts = filename.split(".");
      	return parts[parts.length - 1];
    }
	    
    function updateEditorMode(filename) {
	    		templateEditor.setOption("mode", "javascript");
	}
	
	function autoFormat() {
		if(templateEditor.getValue() != "" ){
		var totalLines = templateEditor.lineCount();
		var totalChars = templateEditor.getValue().length;
		templateEditor.autoFormatRange(
			{line: 0, ch: 0}, 
			{line: totalLines - 1, ch: templateEditor.getLine(totalLines - 1).length}
		);
	}
	else{
		popupManager.showError("Your must be select file to formating.");
		}
	}
	
	function fileSelected(filename, content) {
		templateEditor.setOption("readOnly", false);
		$("#filename").text(decode(filename));
		templateEditor.setValue(content);
		updateEditorMode(filename);
		if(esJson(filename)){
			$("#recipe").attr("class", "btn btn-primary");
			$("#recipe-model-name").text(decode(filename));
			$('#left-textarea').val(content);
			$("#left-textarea").attr("readonly", "readonly");
			modalEditor.setValue("");
			modalEditor.clearHistory();	
			$.ajax({url: recipes + name + "_Rules", type: "GET", dataType: 'text', success: function(response) {
				oldRecipe = response;
				modalEditor.setValue(response);
			}}); 
			currentFilename = "_Models/" + filename.replace("#", "%23");
		}
		else{
			$("#recipe").attr("class", "btn btn-primary disabled");
			currentFilename = "/" +filename.replace("#", "%23");
		}
		$("#editorContainer").fadeIn(500);
		    newFile = false;
	} 
	
	function esJson(sub){
		var res = sub.split(".");
		if(res[res.length - 1] == "json"){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
	
	   
	
    function deleteFile(param1){
    	var x=param1.replace("#", "%23");
    	$.ajax({url: delPath + name + "/" + x, type: "DELETE", success: function(response) {
		$('#fileListModel').empty();
		$("#modelSelect").empty();
		$.ajax({url: models + name + "_Models", type: "GET", dataType: 'json', success: function(response) {
	            $.each(response.files, function(_, filename) {
	            	addFileToListModels(filename);
	           });
	        }});   	  
	     }});
		deleted();
	}
	
	function addFileToListModels(filename) {
	    var html = "<li class=\"list-group-item-e\">";
		filename = filename.replace("%23", "#");
		html = html + "<input title=\"Delete\" type=\"image\" id=\"icon-file\" class=\"icon-files\" src=\"../images/remove.png\" onclick=\"deleteFile('"+filename+"');\"/><img id=\"icon-file\" class=\"icon-files\" src=\"../images/file.png\" /><a href=\"file/[filename]\" class=\"fileLink\" title=\"[filename]\"><p class=\"ellipsis\">[filename]</p></a>".replace(/\[filename\]/g, filename);
		html = html + '</li>'; 
		var jHtml = $(html);
		jHtml.find('a.fileLink').click(function(e) {
		    var self = this;
		    var ff = filename.replace("#","%23");
		    e.preventDefault();   
		    $.ajax({url: fileSystemPath  + name + "_Models/" + ff , type: "GET", success: function(response) {
		      fileSelected(filename, response)
		     }});
		});
		$("#fileListModel").append(jHtml);
		$("#modelSelect").append('<option><img class="icon-file" src="../images/file.png" />'+ decode(filename) + "</option>");
	}
	
	function isValidJson(json) {
	    try {
	        JSON.parse(json);
	        return true;
	    } catch (e) {
	        return false;
	    }
	}
	
	function fileSaved(filename) {	
		if(esJson(filename)){
		    	if(isValidJson(templateEditor.getValue())){
		    		popupManager.showSuccess("Valid JSON, save successfully!");
		    		$('#left-textarea').val(templateEditor.getValue());
		    		
		    	if (newFile) {
		    		addFileToList(filename, name);
		    		newFile = false;
		    	}
		    	}	
		    	else{
		    		popupManager.showError("Your JSON is not valid, check it please.");
		    	}
		}
		else{
			popupManager.showSuccess("File saved!!");
		}	
	}
	
	function inferSaved(filename, content) {
		popupManager.showSuccess("Your model was successfully inferred.");
		setTimeout("location.reload(true);",500);
	}
	
	function deleted() {
		popupManager.showSuccess("Your file was successfully deleted.");
		setTimeout("location.reload(true);",500);
	}
	
	function renderSaved(filename, content) {
		$("#myModal").modal('hide');
		$("#msj").append("<p id=\"A\" class=\"text-success\">The file are stored in versions area. Click <a href=\"../historyMYL/" + name + "_MYL\" ><i>HERE</i></a> for view, or go to the main page and then enter in Browse Versions for project "+name+".</p>");
		$("#oModal").modal('show');
	}
	    
	  
	function delFileFT (){	
		if(sel.length == 0){
			popupManager.showError("Your must be select a file to delete.");
		}
		else{
			for ( var i = 0, l = sel.length; i < l; i++ ) {
			    var ff = sel[i].replace("#","%23");
				$.ajax({url:  fileSystemPath + "delete/" + name + "/" + ff, type: "DELETE", success: function(response) {
				    
			    }});
			}
			location.reload();
		}
	}
	  
	function editFileFT (f){ 
	  var fi = escape(f);
	  $.ajax({url:  fileSystemPath + "edit/" + name + "/" + fileFT + "/" + fi, type: "PUT", success: function(response) {
   			location.reload();
	  }});
	}
	  
	function decode(str){
		return str.replace("%23", "#");
	}  
	
	
	function escape(str) {
		return str.replace("#", "%23");
	}
	
	$(document).ready(function() {		
		$("#filenameTitle").text("Files in the project " + name + ":");
		$("#menu").append("<li><a class=\"icon icon-graph\" title=\"Browse old versions of "+name+ "\"  href=\"../historyMYL/"+ name +"_MYL" + "\">Versions</a></li>");
		$("#modelsTitle").text("Models for project " + name + ":");	
		  templateEditor = CodeMirror($("#editor")[0], {
		      value: "> Select file",
		      readOnly: true,
		      mode: 'htmlmixed',
		      lineNumbers: true,
		      theme: 'default', // the theme can be set here
		      lineWrapping: false,
		      smartIndent: false,
		      styleActiveLine: true,
		      matchBrackets: true,
		      enableCodeFormatting: false,
		      autoFormatOnStart: false,
		      autoFormatOnModeChange: false,
		      extraKeys: {
		          "Ctrl-S": function (instance) {
		        	  var fileN = $("#filename").text();
		              if(fileN == "No files selected."){
		            	  popupManager.showError("You must select a file to save it.");
		              }
		              else{
		        	  	$.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
		                 	fileSaved(currentFilename, templateEditor.getValue());
		             	}});
		              }
		             return false;
		          },
		          "Cmd-S": function (instance) {
		             $.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
		                  fileSaved(currentFilename, templateEditor.getValue());
		             }});
		             return false;
		          }
		       }
		  });
		  templateEditor.setSize(920, 445);
		  
 
		  modalEditor = CodeMirror($("#editorModal")[0], {
			  mode: "javascript",
              lineNumbers: true,
              lineWrapping: true
		  });
		  modalEditor.setSize(620, 215);
		  $("#saveButton").click(function(e) {
		      var self = this;
		      e.preventDefault(); 
		      var fileN = $("#filename").text();
		      if(fileN == "No files selected."){
		    	  popupManager.showError("You must select a file to save it.");
		      }
		      else{
		      	$.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
		       	 fileSaved(currentFilename, templateEditor.getValue());
		       	}});
		      }
		  });
		  
		  
		  $("#recipe").click(function(e) {
			  startSaveRecipe();
		  });
		  
			function showMessage(message, type) {
				if (type == "success") {
					popupManager.showSuccess(message);
				} else {
					popupManager.showError(message);
				}
			}
		  
		  $("#checkRecipeButton").click(function(e) {
			 
			  $.ajax({url: recipes + name + currentFilename, type: "POST", dataType: 'javascript', data: modalEditor.getValue(),   
		
				  success: function(request, status, error) {
					  $("#recipeConsole").text(request.responseText);
					  $("#recipeConsole").css('border-color',  'green' );
				  },
		          error: function (request, status, error) {
		        	  $("#recipeConsole").css('border-color',  'red' );
		        	  $("#recipeConsole").text(request.responseText);
			      }
	
			  });
			  
			  $("#recipeConsole").attr('disabled', true);
		  });
		  
		  $("#inferButton").click(function(e) {
		      var self = this;
		      newFile = true;
		      var fileN = $("#filename").text().replace("#", "%23");
		      if(fileN == "No files selected."){
		    	  popupManager.showError("You must select a file to infer a model.");
		      }
		      else{
		          $.ajax({url: inferPath + name + "/" + fileN, type: "PUT", data: templateEditor.getValue(), success: function(response) {
		        	 if(name == fileN){
		        		 addFileToListModels(name + "_model.json"); 
		        	 }
		        	 else{
		        		 addFileToListModels(fileN + "_model.json");
		        	 }
		              inferSaved();
		           },
		           error: function (request, status, error) {
		        	 popupManager.showError(request.responseText);
		           }
		           });  
		      }  
		  	});
		  
		  
			$("#clickRender").click(function(){
				//$("#A").remove();
				if(fileFT != null){
					$("#fileRender").text(decode(fileFT));
					file = decode(fileFT);
					if ($('#modelSelect option').length == 1) {
						$('#modelSelect option').attr('selected','selected');
						$("#fileModel").text($('#modelSelect option:selected').val());
					}
					else{
						$("#fileModel").html("<i><font color=\"#898C90\">Selected model</font></i>");
					}
				}
				else{
					$("#fileRender").html("<i><font color=\"#898C90\">Selected file</font></i>");
					if ($('#modelSelect option').length == 1) {
						$('#modelSelect option').attr('selected','selected');
						$("#fileModel").text($('#modelSelect option:selected').val());
					}
					else{
						$("#fileModel").html("<i><font color=\"#898C90\">Selected model</font></i>");
					}
				}
				$("#myModal").modal('show');
			});
		
			$("#controlEdit").click(function(){
				if(fileFT == null ){
					popupManager.showError("You must select a file to editing.");
				}
				else{
					$('#texto').val(fileFT);
					$("#myModal1").modal('show');
				}
			});
		
			$("#runEdit").click(function(){
				var input = $("#texto").val();
			    editFileFT(input);	
			});
		
		
			$("#reload").click(function(){
				location.reload();
			});
		
			$("#runRender").click(function(){
				var model =  $("#modelSelect").val().replace("#", "%23");		
			    $.ajax({url: renderPath + name + "/" + file + "/" + model, type: "PUT", success: function(response) {
				          renderSaved(); 
			             },
			             error: function (request, status, error) {
			            	 popupManager.showError("Alert " + request.responseText); 	 
			             }
			     });
	    	});
			 
	        $.ajax({url: models + name + "_Models", type: "GET", dataType: 'json', success: function(response) {
			    $.each(response.files, function(_, filename) {
			   	 addFileToListModels(filename);
			    });
			}});
		
	        $("#tree").fancytree({ 	
	        	
                checkbox: true,
                selectMode: 2,
	        	//checkbox: true,
				//selectMode: 1,
				classNames: {checkbox: "fancytree-radio"},
				source: $.ajax({
			         	url: fileSystemPath + name, type: "GET",
			         	dataType: "json",
			         	success: function(response) {
			              
			            },
			            error: function (request, status, error) {
			            	 popupManager.showError(request.responseText);
			            }
			    }),
			    select: function(e, data) {
			        // Get a list of all selected nodes, and convert to a key array:
			        var a = [];
			    	selNodes1 = $.map(data.tree.getSelectedNodes(), function(node){
			        	a.push(node.title);
			        	return node.title;
			        });
			        //console.log(selNodes1);
			    	sel = a;
			    	console.log(sel);
			    	// Display list of selected nodes
			        var selNodes = data.tree.getSelectedNodes();
			        // convert to title/key array
			        var selKeys = $.map(selNodes, function(node){
			        	fileFT = node.title;	 
			        	
			        	//console.log(">> " + node.title);
			         });
			       
				},
				 click: function(event, data) {
					fileFT = data.node.title.replace("#","%23");
				 },
			    activate: function(event, data){
			        var node = data.node.title.replace("#", "%23");
			        $.ajax({url: fileSystemPath + name + "/" + node , type: "GET", success: function(response) {
			                fileSelected(node, response)
			            }});
		        },
		    });
		 
		    
		    $("#fileSelect").fancytree({ 	 	
                checkbox: false,
				source: $.ajax({
			         	url: fileSystemPath + name, type: "GET",
			         	dataType: "json",
			         	success: function(response) {
			              
			            }
			    }),
			    activate: function(event, data){
			        var node = data.node.title;
			        $("#fileRender").text("");
			        $("#fileRender").text(node);
			        file = node.replace("#","%23");
			    },
		    });
	
			$("#modelSelect").change(function() {
			  var str = "";
			  $( "#modelSelect option:selected" ).each(function() {
			    str += $( this ).text() + " ";
			  });
			  $("#fileModel").text( str );
			}).trigger( "change" );
			
			function startSaveRecipe(){
				idleInterval = setInterval(timerIncrement, 3000); 
			    $(this).mousemove(function (e) {
			        idleTime = 0;
			    });
			    $(this).keypress(function (e) {
			        idleTime = 0;
			    });
			    function timerIncrement() {
			        idleTime = idleTime + 1;
			        if (idleTime > 1) { 
			           if(haveChange()){
			        	   autoSave();
			           }
			        }
			    }
			}
		    
		    function haveChange(){
		    	var res = false;
		    	if(modalEditor.getValue() != oldRecipe){
		    		res = true;
		    		oldRecipe = modalEditor.getValue();
		    	}
		    	return res;
		    }
			
			function autoSave(){
				var recipeToSave = name + "_rule.js";
				var folderToSave = name + "_Rules" ;
				$.ajax({url: fileSystemPath + folderToSave  + "/"+ recipeToSave, type: "PUT", data: modalEditor.getValue(), 
					success: function(response) {
						popupManager.showSuccess("Your validation rule has been successfully saved");
			    	},
		           	error: function (request, status, error) {
			        	 popupManager.showError(request.responseText);
			        }
				});				
			};

			
			$("#closeRecipeModal").click(function(e) {
				autoSave();
				clearInterval(idleInterval);
				location.reload();
			});
			
					
});
    