package org.metapi.core;

public enum HttpMethod {
	GET, POST, PUT, DELETE
}
