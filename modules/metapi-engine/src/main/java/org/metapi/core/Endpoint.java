package org.metapi.core;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Endpoint implements Script {

	private String id;
	private HttpMethod httpMethod;
	private Pattern regex;
	private String script;
	private ScriptEngineManager scriptEngineManager;

	public Endpoint(String id, HttpMethod httpMethod, Pattern regex, String script) {
		this.id = id;
		this.httpMethod = httpMethod;
		this.regex = regex;
		this.script = script;
		this.scriptEngineManager = new ScriptEngineManager();
	}
	
	public Endpoint(HttpMethod httpMethod, Pattern regex, String script) {
		this(null, httpMethod, regex, script);
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public Pattern getRegex() {
		return regex;
	}

	public void setRegex(Pattern regex) {
		this.regex = regex;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getId() {
		return id;
	}
	
	public Object run() throws ScriptException {
        return this.run(new HashMap<String, Object>());
	}
	
	// TODO pre-compile script
	@Override
	public Object run(Map<String, Object> context) throws ScriptException {
        ScriptEngine engine = this.scriptEngineManager.getEngineByName("JavaScript");
        for (String key : context.keySet()) {
        	engine.put(key, context.get(key));
        }
        return engine.eval(this.script);
	}
	
	

}
