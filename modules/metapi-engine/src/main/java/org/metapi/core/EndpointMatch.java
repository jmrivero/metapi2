package org.metapi.core;

import java.util.Collections;
import java.util.List;

public class EndpointMatch {

	private Script endpoint;
	private List<String> parameters;
	
	public EndpointMatch(Script endpoint, List<String> parameters) {
		super();
		this.setEndpoint(endpoint);
		this.setParameters(parameters);
	}

	public Script getEndpoint() {
		return endpoint;
	}

	private void setEndpoint(Script endpoint) {
		this.endpoint = endpoint;
	}

	public List<String> getParameters() {
		return Collections.unmodifiableList(this.parameters);
	}

	private void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
	
	
	
	
}
