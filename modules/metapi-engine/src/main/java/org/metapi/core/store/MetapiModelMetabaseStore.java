package org.metapi.core.store;

import java.util.Collection;
import java.util.List;

import org.metapi.core.MetapiModel;
import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseObject;
import org.metapi.serializer.MetapiSerializer;
import org.metapi.utils.GenericObserver;

public class MetapiModelMetabaseStore implements MetapiStore {

	private static final String METAPI_MODEL = "__MetapiModel";
	private Metabase metabase;
	private MetapiSerializer serializer;
  private Collection<GenericObserver<MetapiStore, MetapiModel>> observers;

	public MetapiModelMetabaseStore(Metabase metabase, MetapiSerializer serializer) {
		this.metabase = metabase;
		this.serializer = serializer;
	}

	@Override
	public MetapiModel getModel() throws Exception {
		MetabaseObject modelObject = this.fetchModelObject();
		if (modelObject == null) {
			return null;
		}
		return this.serializer.deserializeMetapiModel(modelObject.getRepresentation());
	}

	private MetabaseObject fetchModelObject() throws Exception {
		List<MetabaseObject> objects = this.metabase.getAll(METAPI_MODEL);
		if (objects.size() == 0) {
			return null;
		}
		return objects.get(0);
	}
	
	@Override
	public void saveModel(MetapiModel model) throws Exception {
		MetabaseObject modelObject = this.fetchModelObject();
		if (modelObject == null) {
			this.metabase.save(METAPI_MODEL, this.serializer.serializeMetapiModel(model));
		} else {
			this.metabase.update(METAPI_MODEL, modelObject.getId(), this.serializer.serializeMetapiModel(model));
		}
		this.updateObservers(model);
	}

  private void updateObservers(MetapiModel model) {
    for (GenericObserver<MetapiStore, MetapiModel> observer : this.observers) {
      observer.update(this, model);
    }
  }

  @Override
  public void setObservers(Collection<GenericObserver<MetapiStore, MetapiModel>> observers) {
    this.observers = observers;
  }

}
