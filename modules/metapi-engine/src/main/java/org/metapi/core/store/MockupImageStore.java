package org.metapi.core.store;

import java.io.InputStream;
import java.util.List;

import org.metapi.resources.ResourceService;


public class MockupImageStore {
  
  private ResourceService mockupImageResourceService;

  public MockupImageStore(ResourceService mockupImageResourceService) {
    super();
    this.mockupImageResourceService = mockupImageResourceService;
  }
  
  public List<String> getImageNames() {
    return this.mockupImageResourceService.listResources(1);
  }
  
  public InputStream getImageContent(String imageName) {
    return this.mockupImageResourceService.getContentsOfResource(1, imageName);
  }
  
  public void addImage(String imageName, InputStream content) {
    this.mockupImageResourceService.storeResource(1, imageName, content);
  }
  
}
