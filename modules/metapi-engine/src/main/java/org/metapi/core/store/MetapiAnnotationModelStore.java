package org.metapi.core.store;

public interface MetapiAnnotationModelStore {

	String getModel(String id) throws Exception;
	
	void storeModel(String id, String representation) throws Exception;
	
}
