package org.metapi.core.store;

import java.util.Collection;

import org.metapi.core.MetapiModel;
import org.metapi.utils.GenericObserver;

public interface MetapiStore {

	MetapiModel getModel() throws Exception;

	void saveModel(MetapiModel model) throws Exception;
	
	void setObservers(Collection<GenericObserver<MetapiStore, MetapiModel>> observers);
 
}