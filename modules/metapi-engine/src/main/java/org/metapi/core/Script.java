package org.metapi.core;

import java.util.Map;

import javax.script.ScriptException;

import org.metapi.service.StopScriptExecutionException;

public interface Script {

	public abstract Object run(Map<String, Object> context) throws ScriptException;

}