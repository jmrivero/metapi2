package org.metapi.core.store;

import java.io.InputStream;
import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.metapi.core.MetapiModel;
import org.metapi.resources.ResourceService;
import org.metapi.serializer.MetapiSerializer;
import org.metapi.utils.GenericObserver;


public class MetapiModelResourceBasedStore implements MetapiStore {

  private static final String RESOURCE_NAME = "metapiModel.json";
  private MetapiSerializer serializer;
  private Collection<GenericObserver<MetapiStore, MetapiModel>> observers;
  private ResourceService resourceService;

  public MetapiModelResourceBasedStore(ResourceService resourceService, MetapiSerializer serializer) {
    this.resourceService = resourceService;
    this.serializer = serializer;
  }

  @Override
  public MetapiModel getModel() throws Exception {
    InputStream modelStream = this.resourceService.getContentsOfResource(1, RESOURCE_NAME);
    if (modelStream == null) {
      return null;
    }
    return this.serializer.deserializeMetapiModel(IOUtils.toString(modelStream));
  }
  
  @Override
  public void saveModel(MetapiModel model) throws Exception {
    String serializedModel = this.serializer.serializeMetapiModel(model);
    this.resourceService.storeResource(1, RESOURCE_NAME, IOUtils.toInputStream(serializedModel));
    this.updateObservers(model);
  }

  private void updateObservers(MetapiModel model) {
    for (GenericObserver<MetapiStore, MetapiModel> observer : this.observers) {
      observer.update(this, model);
    }
  }

  @Override
  public void setObservers(Collection<GenericObserver<MetapiStore, MetapiModel>> observers) {
    this.observers = observers;
  }

}
