package org.metapi.core.packer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.metapi.core.MetapiModel;
import org.metapi.core.store.MetapiStore;
import org.metapi.resources.ResourceService;
import org.metapi.serializer.MetapiSerializer;

public class MetapiModelPacker {

  private static final String RESOURCES_PATH = "resources";

  private static final String METAPI_MODEL_FILE = "metapiModel.json";

  private static Log log = LogFactory.getLog(MetapiModelPacker.class);

  private ResourceService resourceService;
  private MetapiStore metapiStore;
  private MetapiSerializer metapiSerializer;

  public MetapiModelPacker(ResourceService resourceService, MetapiStore metapiStore, MetapiSerializer metapiSerializer) {
    super();
    this.resourceService = resourceService;
    this.metapiStore = metapiStore;
    this.metapiSerializer = metapiSerializer;
  }

  public void loadModelFromPackage(long applicationId, InputStream modelPackage) {
    try {
      ZipInputStream zipInputStream = new ZipInputStream(modelPackage);
      ZipEntry zipEntry = zipInputStream.getNextEntry();
      this.resourceService.deleteAll(1);
      while (zipEntry != null) {
        if (zipEntry.getName().equals(METAPI_MODEL_FILE)) {
          this.metapiStore.saveModel(this.metapiSerializer.deserializeMetapiModel(IOUtils.toString(zipInputStream)));
        } else if (zipEntry.getName().startsWith(RESOURCES_PATH)){
          String resourceName = zipEntry.getName().replace(RESOURCES_PATH, "");
          if (resourceName.startsWith("/")) {
            resourceName = resourceName.substring(1);
          }
          this.resourceService.storeResource(applicationId, resourceName, zipInputStream);
        }
        zipInputStream.closeEntry();
        zipEntry = zipInputStream.getNextEntry();
      }
      zipInputStream.close();
    } catch (Exception e) {
      log.error("Error uncompressing content", e);
    }
  }

  public InputStream createPackageFromModel(long applicationId) {
    try {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
      zipOutputStream.putNextEntry(new ZipEntry(METAPI_MODEL_FILE));
      IOUtils.copy(IOUtils.toInputStream(this.metapiSerializer.serializeMetapiModel(this.metapiStore.getModel())), zipOutputStream);
      for (String resourcePath : this.resourceService.listResources(applicationId)) {
        zipOutputStream.putNextEntry(new ZipEntry("resources/" + resourcePath));
        IOUtils.write(IOUtils.toByteArray(resourceService.getContentsOfResource(applicationId, resourcePath)), zipOutputStream);
      }
      zipOutputStream.close();
      return new ByteArrayInputStream(outputStream.toByteArray());
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

}
