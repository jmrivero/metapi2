package org.metapi.core;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.MetabaseObject;

public class MetabaseWrapper {

	private Metabase metabase;
	private ScriptExecutionEnvironment environment;

	public MetabaseWrapper(Metabase metabase, ScriptExecutionEnvironment environment) {
		this.metabase = metabase;
		this.environment = environment;
	}

	public List<String> getAll(String type) throws Exception {
		return this.getRepresentations(this.metabase.getAll(type));
	}

	private List<String> getRepresentations(List<MetabaseObject> objects) {
		List<String> reps = new ArrayList<String>();
		for (MetabaseObject o : objects) {
			reps.add(this.getRepresentation(o));
		}
		return reps;
	}

	private String getRepresentation(MetabaseObject o) {
		if (o == null) {
			return null;
		}
		return o.getRepresentation();
	}

	public String getById(String type, long id) throws Exception {
		return this.getRepresentation(this.metabase.getById(type, id));
	}

	public String createObject(String type, String representation) throws Exception {
		JSONObject rep = null;
		try {
			rep = new JSONObject(representation);
		} catch (Exception e) {
			this.environment.error(400, "OBJECT_NOT_JSON", "Object is not in strict JSON format");
			return null;
		}
		MetabaseObject resultingObject = this.metabase.save(type, representation);
		rep.put("id", resultingObject.getId());
		String finalRep = rep.toString();
		this.metabase.update(type, resultingObject.getId(), finalRep);
		return finalRep;
	}
	
	public String updateObject(String type, long id, String representation) throws Exception {
		JSONObject rep = null;
		try {
			rep = new JSONObject(representation);
		} catch (Exception e) {
			this.environment.error(400, "OBJECT_NOT_JSON", "Object is not in strict JSON format");
			return null;
		}
		rep.put("id", id);
		return this.getRepresentation(this.metabase.update(type, id, rep.toString()));
	}
	
	public void deleteObject(String type, long id) throws Exception {
		this.metabase.delete(type, id);
	}

	public void associate(String sourceType, String associationName, String destType, long sourceId, long destId) throws Exception {
		this.metabase.associate(sourceType, associationName, destType, sourceId, destId);
	}

	public void dissociate(String sourceType, String associationName, String destType, long sourceId, long destId) throws Exception {
		this.metabase.dissociate(sourceType, associationName, destType, sourceId, destId);
	}

	public List<String> getAssociatedObjects(String sourceType, String associationName, String destType, long sourceId) throws Exception {
		return this.getRepresentations(this.metabase.getAssociatedObjects(sourceType, associationName, destType, sourceId));
	}
	
	public void deleteAll(String type) throws MetabaseException {
		this.metabase.deleteAll(type);
	}
	
	public void dissociateAll(String sourceType, String associationName, String destType, long sourceId) throws Exception {
		this.metabase.dissociateAll(sourceType, associationName, destType, sourceId);
	}
	
	public void clearDatabase() throws MetabaseException {
		this.metabase.clearDatabase();
	}

}