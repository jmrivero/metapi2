package org.metapi.core;

import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class SimpleScript implements Script {

	private String script;
	private ScriptEngineManager scriptEngineManager;

	public SimpleScript(String script) {
		this.script = script;
		this.scriptEngineManager = new ScriptEngineManager();
	}

	// TODO pre-compile script
	@Override
	public Object run(Map<String, Object> context) throws ScriptException {
        ScriptEngine engine = this.scriptEngineManager.getEngineByName("JavaScript");
        for (String key : context.keySet()) {
        	engine.put(key, context.get(key));
        }
        return engine.eval(this.script);
	}
	
}
