package org.metapi.core.store;

import java.util.List;

import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseObject;

public class MetabaseMetapiAnnotationModelStore implements MetapiAnnotationModelStore {

	private Metabase metabase;

	public MetabaseMetapiAnnotationModelStore(Metabase metabase) {
		this.metabase = metabase;
	}

	@Override
	public String getModel(String id) throws Exception {
		List<MetabaseObject> o = this.metabase.getAll(id);
		if (o.isEmpty()) {
			return null;
		}
		return o.get(0).getRepresentation();
	}

	@Override
	public void storeModel(String id, String representation) throws Exception {
		List<MetabaseObject> o = this.metabase.getAll(id);
		if (o.isEmpty()) {
			this.metabase.save(id, representation);
		} else {
			this.metabase.update(id, o.get(0).getId(), representation);		
		}

	}

}
