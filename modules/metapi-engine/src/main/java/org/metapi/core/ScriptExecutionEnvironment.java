package org.metapi.core;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.metapi.metabase.Metabase;
import org.metapi.resources.ResourceService;
import org.metapi.service.StopScriptExecutionException;
import org.metapi.utils.JsonBuilder;

public class ScriptExecutionEnvironment {

	private Logger logger = Logger.getLogger(ScriptExecutionEnvironment.class);

	public String data;
	public String[] params;
	public MetabaseWrapper db;
	private ResourceService resourceService;
	private ScriptEngineManager scriptEngineManager;
	private int responseCode;
	private String result;
	private MetapiModel metapiModel;
	private Map<String, String> headers;
	private HttpServletRequest request;

	private final String noopFunctionCode = "function() {}";

	public ScriptExecutionEnvironment(String data, String[] params, ResourceService resourceService, ScriptEngineManager scriptEngineManager, Metabase metabase, MetapiModel metapiModel) throws ScriptException {
		super();
		this.data = data;
		this.params = params;
		this.resourceService = resourceService;
		this.scriptEngineManager = scriptEngineManager;
		this.db = new MetabaseWrapper(metabase, this);
		this.responseCode = 200;
		this.headers = new HashMap<String, String>();
		this.metapiModel = metapiModel;
	}

	public String getData() {
		return data;
	}

	public String getResource(String path) throws IOException {
		return IOUtils.toString(this.resourceService.getContentsOfResource(1, path));
	}
	
	public InputStream getResourceAsStream(String path) throws IOException {
		return this.resourceService.getContentsOfResource(1, path);
	}
	
	public String getResourceContentType(String path) throws IOException {
		return this.resourceService.getResourceContentType(1, path);
	}
  
	public void deleteResource(String path) throws IOException {
		this.resourceService.deleteResource(1, path);
	}
	
	public boolean resourceExists(String path) {
		return this.resourceService.resourceExists(1, path);
	}
  
	public String getMergedResources(String path) throws IOException {
		String resources="";
		List<String> paths=this.resourceService.listResources(1,path);
		for(String pathResource:paths){
			resources = resources + IOUtils.toString(this.resourceService.getContentsOfResource(1, pathResource))+";";
		}
		return resources;
	}
	
	public List<String> getListResources(String path) throws IOException {
		List<String> paths=this.resourceService.listResources(1,path);
		return paths;
	}	
	public void addResource(String path, String content) {
		this.resourceService.storeResource(1, path, new ByteArrayInputStream(content.getBytes()));
	}

	public void addEndpoint(String httpMethod, String pattern, String script) {
		Endpoint endpoint = new Endpoint(HttpMethod.valueOf(httpMethod), Pattern.compile(pattern), script);
		this.metapiModel.addEndpointIfNotExists(endpoint);
	}

	public Object getScript(String scriptPath) throws ScriptException, IOException {
		ScriptEngine engine = this.scriptEngineManager.getEngineByName("javascript");
		engine.put("env", this);
		InputStream script = this.resourceService.getContentsOfResource(1, scriptPath);
		if (script == null) {
			script = IOUtils.toInputStream(this.noopFunctionCode);
		}
		return engine.eval("function(){" + IOUtils.toString(script) + "}");
	}

	public void println(String s) {
		System.out.println(s);
	}

	public void response(int code, String message) {
		this.responseCode = code;
		this.result = message;
	}

	public void returnWith(int code, String message) throws StopScriptExecutionException {
		this.responseCode = code;
		this.result = message;
		throw new StopScriptExecutionException();
	}

	public void jsonContentType() {
		this.addHeader("Content-Type", "application/json");
	}

	public void error(int responseCode, String errorCode, String errorDescription) throws StopScriptExecutionException {
		try {
			this.responseCode = responseCode;
			this.result = JsonBuilder.newWithObject("error").field("code", errorCode).field("description", errorDescription).build();
		} catch (Exception e) {
			logger.error("Error when creating error response", e);
		}
		throw new StopScriptExecutionException();
	}

	public void addHeader(String header, String value) {
		this.headers.put(header, value);
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public Object getResult() {
		return this.result;
	}

	public Object runScript(String path, Object[] params) throws ScriptException, IOException, NoSuchMethodException {
		ScriptEngine engine = this.scriptEngineManager.getEngineByName("javascript");
		InputStream is = this.resourceService.getContentsOfResource(1, path);
		if (is == null) {
			return null;
		} else {
			Object o = engine.eval("(" + IOUtils.toString(is) + ")");
			return ((Invocable) engine).invokeMethod(o, "apply", o, params);
		}
	}

	public Map<String, String> getHeaders() {
		return Collections.unmodifiableMap(this.headers);
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public String getParam(String paramName) {
		if (this.request != null) {
			return this.request.getParameter(paramName);
		}
		return null;
	}
	public List<String> getParams() {
		return new ArrayList<String>(this.request.getParameterMap().keySet());
	}
}
