package org.metapi.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;

public class MetapiModel {

	private Map<String, Endpoint> endpoints;
	
	public MetapiModel() {
		super();
		this.endpoints = new HashMap<String, Endpoint>();
	}

	public void process(HttpMethod httpMethod, String url) {
		
	}
	
	public Endpoint addEndpoint(Endpoint endpoint) {
		if (endpoint.getId() == null){
			endpoint = new Endpoint(UUID.randomUUID().toString(), endpoint.getHttpMethod(), endpoint.getRegex(), endpoint.getScript());
		}
		this.endpoints.put(endpoint.getId(), endpoint);
		return endpoint;
	}
	
	public Endpoint addEndpointIfNotExists(Endpoint endpoint) {
		Endpoint existingEndpoint = this.findEndpoint(endpoint.getHttpMethod(), endpoint.getRegex().toString());
		if (existingEndpoint != null) {
			return existingEndpoint;
		}
		return this.addEndpoint(endpoint);
	}
	
	public Collection<Endpoint> getEndpoints() {
		return Collections.unmodifiableCollection(this.endpoints.values());
	}

	public EndpointMatch findMatchingEndpoint(HttpMethod method, String url) {
		for (Endpoint endpoint : this.endpoints.values()) {
			Matcher m = endpoint.getRegex().matcher(url);
			if (endpoint.getHttpMethod().equals(method) && m.matches()) {
				List<String> parameters = new ArrayList<String>();
				for (int i = 1; i <= m.groupCount(); i++) {
					parameters.add(m.group(i));
				}
				return new EndpointMatch(endpoint, parameters);
			}
		}
		return null;
	}

	public Endpoint getEndpointById(String endpointId) {
		return this.endpoints.get(endpointId);
	}

	public void updateEndpoint(Endpoint endPoint) {
		this.endpoints.put(endPoint.getId(), endPoint);
		
	}

	public void deleteEndpoint(Endpoint endpoint) {
		this.endpoints.remove(endpoint.getId());
		
	}

	public void deleteAllEndpoints() {
		this.endpoints.clear();
	}

	public Endpoint findEndpoint(HttpMethod method, String urlRegex) {
		for (Endpoint endpoint : this.endpoints.values()) {
			if (endpoint.getHttpMethod().equals(method) && endpoint.getRegex().pattern().equals(urlRegex)) {
				return endpoint;
			}
		}
		return null;
	}
	
	
}
