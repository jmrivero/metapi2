package org.metapi.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.core.Endpoint;
import org.metapi.core.EndpointMatch;
import org.metapi.core.HttpMethod;
import org.metapi.core.MetapiModel;
import org.metapi.core.Script;
import org.metapi.core.ScriptExecutionEnvironment;
import org.metapi.core.SimpleScript;
import org.metapi.core.store.MetapiAnnotationModelStore;
import org.metapi.core.store.MetapiStore;
import org.metapi.gen.MetapiCodeGenerator;
import org.metapi.metabase.Metabase;
import org.metapi.resources.ResourceService;
import org.metapi.serializer.MetapiSerializer;
import org.metapi.utils.GenericObserver;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@Path("/")
public class MetapiRESTFacade implements GenericObserver<MetapiStore, MetapiModel>{

	private MetapiModel metapiModel;	
	private ResourceService resourceService;
	private ScriptEngineManager scriptEngineManager;
	private Metabase endpointsMetabase;
	private MetapiSerializer serializer;
	private MetapiStore metapiStore;
	private MetapiCodeGenerator codegen;
	private MetapiAnnotationModelStore annotationStore;
	
	public MetapiRESTFacade(MetapiStore metapiStore, MetapiAnnotationModelStore annotationStore, ResourceService resourceService,  Metabase metabase, MetapiSerializer serializer) throws Exception {
		this.metapiStore = metapiStore;
		this.metapiModel = this.metapiStore.getModel();
		if (this.metapiModel == null) {
			this.metapiModel = new MetapiModel();
		}
		this.resourceService = resourceService;
		this.scriptEngineManager = new ScriptEngineManager();
		this.endpointsMetabase = metabase;
		this.serializer = serializer;
		this.codegen = new MetapiCodeGenerator(this.metapiModel);
		this.annotationStore = annotationStore;
	}
	
	
	@GET
	@Path("api/{path:.*}")
	public Response runEndpointGet(@PathParam("path") String url, @Context HttpServletRequest httpRequest) {
		return this.processMethod(url, httpRequest, null);
	}
	
	
	@POST
	@Path("api/{path:.*}")
	public Response runEndpointPost(@RequestBody InputStream stream, @PathParam("path") String url, @Context HttpServletRequest httpRequest) {
		return this.processMethod(url, httpRequest, stream);
	}
	
	@PUT
	@Path("api/{path:.*}")
	public Response runEndpointPut(@RequestBody InputStream stream, @PathParam("path") String url, @Context HttpServletRequest httpRequest) {
		return this.processMethod(url, httpRequest, stream);
	}
	
	@DELETE
	@Path("api/{path:.*}")
	public Response runEndpointDelete(@RequestBody InputStream stream, @PathParam("path") String url, @Context HttpServletRequest httpRequest) {
		return this.processMethod(url, httpRequest, stream);
	}

	private Response processMethod(String url, HttpServletRequest httpRequest, InputStream stream) {
		try {
			EndpointMatch e = this.metapiModel.findMatchingEndpoint(HttpMethod.valueOf(httpRequest.getMethod()), url);
			if (e == null) {
				return Response.status(404).entity("Endpoint not found").build();
			}
			return runScript(stream, e.getParameters(), e.getEndpoint(), httpRequest);
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	private Response runScript(InputStream stream, List<String> parameters, Script script, HttpServletRequest request) throws IOException {
		try {
			Map<String, Object> context = new HashMap<String, Object>();
			ScriptExecutionEnvironment env = new ScriptExecutionEnvironment(
					stream != null ? IOUtils.toString(stream) : null, 
					parameters.toArray(new String[parameters.size()]),
					this.resourceService, 
					this.scriptEngineManager, 
					this.endpointsMetabase,
					this.metapiModel);
			env.setRequest(request);
			context.put("env", env);
			Object result = null;
			try {
			  result = script.run(context);
			} catch (ScriptException e) {
			  if (!(e.getCause().getCause() instanceof StopScriptExecutionException)) {
			    throw e;
			  }
			}
			ResponseBuilder response = Response.status(env.getResponseCode());
			Map<String, String> headers = env.getHeaders();
      for (String headerName : headers.keySet()) {
			  response.header(headerName, headers.get(headerName));
			}
			return response
					.header("Cache-Control", "no-cache")
					.entity(env.getResult() != null ? env.getResult() : this.jsonify(result)).build();
		} catch (ScriptException e) {
		  
			return Response.status(400).entity(e.getMessage()).build();
		}
	}
	
	@SuppressWarnings("rawtypes")
	private Object jsonify(Object o) throws IOException {
		if (o == null) {
			return "";
		}
		if (o instanceof Collection) {
			List<Object> objects = new ArrayList<Object>();
			for (Object co : (Collection) o) {
				try {
					objects.add(new JSONObject(co.toString()));
				} catch (Exception e){
					objects.add(co.toString());
				}
			}
			return new JSONArray(objects).toString();
		}
		if (o instanceof InputStream) {
			return o;
		}
		return o.toString();
	}

	@POST
	@Path("endpoints")
	public Response addEndpoint(@RequestBody InputStream stream) {
		try {
			Endpoint e = this.serializer.deserializeEndpoint(IOUtils.toString(stream));
			e = this.metapiModel.addEndpointIfNotExists(e);
			this.metapiStore.saveModel(this.metapiModel);
			return Response.ok(this.serializer.serializeEndpoint(e)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	
	@PUT
	@Path("endpoints/{id}")
	public Response addEndpoint(@RequestBody InputStream stream, @PathParam("id") String endpointId) {
		try {
			Endpoint updatedEndpoint = this.serializer.deserializeEndpoint(IOUtils.toString(stream));
			Endpoint oldEndpoint = this.metapiModel.getEndpointById(endpointId);
			if (oldEndpoint == null) {
				return Response.status(404).entity("Endpoint not found").build();
			}
			oldEndpoint.setHttpMethod(updatedEndpoint.getHttpMethod());
			oldEndpoint.setRegex(updatedEndpoint.getRegex());
			oldEndpoint.setScript(updatedEndpoint.getScript());
			this.metapiModel.updateEndpoint(oldEndpoint);
			this.metapiStore.saveModel(this.metapiModel);
			return Response.ok("Endpoint updated").build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	
	@DELETE
	@Path("endpoints/{id}")
	public Response deleteEndpoint(@PathParam("id") String endpointId) throws Exception {
		Endpoint endpoint = this.metapiModel.getEndpointById(endpointId);
		if (endpoint == null) {
			return Response.status(404).entity("Endpoint not found").build();
		}
		this.metapiModel.deleteEndpoint(endpoint);
		this.metapiStore.saveModel(this.metapiModel);
		return Response.status(200).entity("Endpoint deleted").build();
	}
	
	@DELETE
	@Path("endpoints")
	public Response deleteAllEndpoints() throws Exception {
		this.metapiModel.deleteAllEndpoints();
		this.metapiStore.saveModel(this.metapiModel);
		return Response.status(200).entity("All endpoints deleted").build();
	}
	
	@GET
	@Path("endpoints")
	public Response getEndpoints() {
		try {
			return Response.ok(this.serializer.serializeEndpointList(this.metapiModel)).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	
	@POST
	@Path("run")
	public Response run(@RequestBody InputStream stream, @Context HttpServletRequest request) throws IOException, ScriptException {
		return this.runScript(null, new ArrayList<String>(), new SimpleScript(IOUtils.toString(stream)), request);
	}
	
	@POST
	@Path("generatedEndpoints")
	public Response generateEndpoints(@RequestBody InputStream stream) {
		try {
			int endpointCount = 0;
			this.endpointsMetabase.clearDatabase();
			this.codegen.generateGeneralEndpoints();
			JSONObject object = new JSONObject(IOUtils.toString(stream));
			JSONArray resources = object.getJSONArray("resources");
			for (int i = 0; i < resources.length(); i++) {
				endpointCount += this.codegen.generateCrudEndpointsFor(resources.getString(i)).size();
			}
			JSONArray associations = object.getJSONArray("associations");
			for (int i = 0; i < associations.length(); i++) {
				JSONObject association = associations.getJSONObject(i);
				endpointCount += this.codegen.generateAssociation(
						association.getString("source"), 
						association.getString("name"),
						association.getString("destination")).size();
			}
			this.metapiStore.saveModel(this.metapiModel);
			return Response.ok(endpointCount + " endpoints generated").build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}	
	
	@PUT
	@Path("models/{id}")
	public Response storeModel(@RequestBody InputStream stream, @PathParam("id") String id) throws IOException, Exception {
		this.annotationStore.storeModel(id, IOUtils.toString(stream));
		return Response.ok("Model stored").build();
	}
	
	@GET
	@Path("models/{id}")
	public Response getModel(@PathParam("id") String id) throws IOException, Exception {
		String model = this.annotationStore.getModel(id);
		if (model == null) {
			return Response.status(404).entity("Model not found").build();
		} else {
			return Response.ok(model).build();
		}
	}
	
	@POST
	@Path("clean")
	public Response clean(@RequestBody InputStream stream) {
		try {
			this.endpointsMetabase.clearDatabase();
			return Response.ok("Database cleared").build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

  @Override
  public void update(MetapiStore observable, MetapiModel model) {
    this.metapiModel = model;
  }	
	
}
