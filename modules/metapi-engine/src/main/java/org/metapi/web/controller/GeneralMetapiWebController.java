package org.metapi.web.controller;

import java.io.IOException;
import java.io.InputStream;


import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.metapi.core.packer.MetapiModelPacker;
import org.metapi.resources.ResourceService;
import org.metapi.utils.JsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GeneralMetapiWebController {

  @Autowired
  private ResourceService resourceService;
  
  @Autowired
  private MetapiModelPacker modelPacker;
  
  @RequestMapping(value = "/resources", method = RequestMethod.GET)
  public ModelAndView resourceEditor() {
    return new ModelAndView("resources");
  }

  @RequestMapping(value = "/sandbox", method = RequestMethod.GET)
  public ModelAndView sandbox() {
    return new ModelAndView("sandbox");
  }

  @RequestMapping(value = "/endpoints", method = RequestMethod.GET)
  public ModelAndView endpoints() {
    return new ModelAndView("endpoints");
  }

  @RequestMapping(value = "/mockups", method = RequestMethod.GET)
  public ModelAndView mockups() {
    return new ModelAndView("mockups");
  }

  @RequestMapping(value = "/import-export", method = RequestMethod.GET)
  public ModelAndView importExport() {
    return new ModelAndView("import-export");
  }
  
  @RequestMapping(value = "/model", method = RequestMethod.POST)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadZippedModel(FileUploadForm zipfile) throws IOException {
    this.modelPacker.loadModelFromPackage(1, zipfile.getFile().getInputStream());
    return "redirect:/editor/endpoints";
  }
  
  @RequestMapping(value = "/model", method = RequestMethod.GET)
  public void downloadZipperModel(FileUploadForm zipfile, HttpServletResponse response) throws IOException {
    InputStream serializedModel = this.modelPacker.createPackageFromModel(1);
    IOUtils.copy(serializedModel, response.getOutputStream());
    response.setContentType("application/zip");      
    response.setHeader("Content-Disposition", "attachment; filename=metapiModel.zip");
    response.flushBuffer();
  }
  
  @RequestMapping(value = "/resources/{applicationId}/upload", method = RequestMethod.POST)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadResourcesFile(@PathVariable("applicationId") long applicationId, FileUploadForm zipfile) throws IOException {
    this.resourceService.uncompressContent(applicationId, zipfile.getFile().getInputStream());
    return "redirect:../../resources";
  }
  
  @RequestMapping(value = "/mockupImages", method = RequestMethod.GET)
  @ResponseBody
  public String getMockupImages(@PathVariable("applicationId") long applicationId, FileUploadForm zipfile) throws IOException {
    JsonBuilder newWithArray = JsonBuilder.newWithArray("mockups");
    return "redirect:../../resources";
  }

}
