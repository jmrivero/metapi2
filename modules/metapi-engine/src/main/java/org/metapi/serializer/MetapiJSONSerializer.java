package org.metapi.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.core.Endpoint;
import org.metapi.core.HttpMethod;
import org.metapi.core.MetapiModel;
import org.metapi.utils.JsonBuilder;

public class MetapiJSONSerializer implements MetapiSerializer {

	private Logger logger = Logger.getLogger(MetapiJSONSerializer.class);
	
	private void serializeEndpoint(JsonBuilder json, Endpoint e) throws JsonGenerationException, IOException {
		json
			.field("method", e.getHttpMethod().toString())
			.field("urlRegex", e.getRegex().toString())
			.field("script", e.getScript())
			.field("id", e.getId());
	}
	
	@Override
	public String serializeEndpointList(MetapiModel model) {
		try {
			JsonBuilder json = JsonBuilder.newWithObject();
			this.serializeEndpointList(model, json);	
			return json.build();
		} catch (Exception e) {
			logger.error("Error when serializing endpoint list", e);
			return null;
		}
	}
	
	private JsonBuilder serializeEndpointList(MetapiModel engine, JsonBuilder json) throws JsonGenerationException, IOException {
		json.objectField("endpoints");
		for (Endpoint e : getSortedEndpoints(engine)) {
			json.objectField(e.getId());
			this.serializeEndpoint(json, e);
			json.endObject();
		}	
		json.endObject();
		return json;
	}

	private Collection<Endpoint> getSortedEndpoints(MetapiModel engine) {
		List<Endpoint> endpoints = new ArrayList<Endpoint>(engine.getEndpoints());
		Collections.sort(endpoints, new Comparator<Endpoint>() {

			@Override
			public int compare(Endpoint e1, Endpoint e2) {
				return e1.getRegex().toString().compareTo(e2.getRegex().toString());
			}
			
		});
		return endpoints;
	}
	
	@Override
	public Endpoint deserializeEndpoint(String json) throws Exception {
		JSONObject object = new JSONObject(json);
		return deserializeEndpoint(object);
	}

	private Endpoint deserializeEndpoint(JSONObject object) throws JSONException {
		String id = object.has("id") ? object.getString("id") : null;
		return new Endpoint(
				id, 
				HttpMethod.valueOf(object.getString("method")), 
				Pattern.compile(object.getString("urlRegex")), 
				object.getString("script"));
	}
	
	@Override
	public String serializeMetapiModel(MetapiModel model) {
		try {
			JsonBuilder builder = JsonBuilder.newWithObject("metapiModel");
			this.serializeEndpointList(model, builder);
			return builder.build();
		} catch (Exception e) {
			logger.error("Error when serializing model", e);
			return null;
		}
	}

	@Override
	public String serializeEndpoint(Endpoint e) {
		try {
			JsonBuilder json = JsonBuilder.newWithObject();
			this.serializeEndpoint(json, e);
			return json.build();
		} catch (Exception ex) {
			logger.error("Error when serializing endpoint", ex);
			return null;
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public MetapiModel deserializeMetapiModel(String json) throws Exception {
		MetapiModel metapiModel = new MetapiModel();
		JSONObject object = new JSONObject(json);
		JSONObject endpoints = object.getJSONObject("metapiModel").getJSONObject("endpoints");
		Iterator endpointKeys = endpoints.keys();
		String key;
		while (endpointKeys.hasNext()) {
			key = endpointKeys.next().toString();
			metapiModel.addEndpoint(this.deserializeEndpoint(endpoints.getJSONObject(key)));
		}
		return metapiModel;
	}
	
}
