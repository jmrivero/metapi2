package org.metapi.serializer;

import org.metapi.core.Endpoint;
import org.metapi.core.MetapiModel;

public interface MetapiSerializer {

	public abstract String serializeEndpointList(MetapiModel model);

	public abstract Endpoint deserializeEndpoint(String json) throws Exception;

	public abstract String serializeMetapiModel(MetapiModel model);

	public abstract String serializeEndpoint(Endpoint e);

	public abstract MetapiModel deserializeMetapiModel(String representation) throws Exception;

}