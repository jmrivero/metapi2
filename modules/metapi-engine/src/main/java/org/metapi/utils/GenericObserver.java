package org.metapi.utils;


public interface GenericObserver<Observable, ObjectOfInterest> {

  void update(Observable observable, ObjectOfInterest object);
  
}
