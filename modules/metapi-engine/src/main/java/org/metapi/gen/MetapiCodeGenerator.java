package org.metapi.gen;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.metapi.core.Endpoint;
import org.metapi.core.HttpMethod;
import org.metapi.core.MetapiModel;

public class MetapiCodeGenerator {

	private static final String CHECK_OBJECT_EXISTANCE_BY_ID =
			"if (env.db.getById(\"${resource}\", env.params[0]) == null) {\n" +
			" env.response(404, \"${resource} not found\");\n" +		
			"}\n";
	
	private static final String CHECK_DEST_OBJECT_EXISTANCE_BY_ID =
			"if (env.db.getById(\"${destResource}\", env.params[1]) == null) {\n" +
			" env.response(404, \"${destResource} not found\");\n" +		
			"}\n";
	
	private static final String RETURN_JSON = "env.jsonContentType()\n";
	
	private MetapiModel model;

	public MetapiCodeGenerator(MetapiModel model) {
		this.model = model;
	}
	
	public List<Endpoint> generateGeneralEndpoints() {
		return Arrays.asList(new Endpoint[] {
			addEndpoint(HttpMethod.GET, "", "web/(.*)", "", "", "", "env.db.getResource(\"web/\" + env.params[0]);")
		});
	}
	
	public List<Endpoint> generateCrudEndpointsFor(String resource) {
		return Arrays.asList(new Endpoint[] {
			addEndpoint(HttpMethod.GET, "all", "${resource}s", resource, "", "", 
			    RETURN_JSON +    
			    "env.db.getAll(\"${resource}\")\n"),
			addEndpoint(HttpMethod.GET, "", "${resource}s/([0-9]+)", resource, "", "", 
			    RETURN_JSON + 
					"var object = env.db.getById(\"${resource}\", env.params[0]);\n" +
					"if (object == null) {\n" + 
					" env.response(404, \"${resource} not found\");\n" +
					"} else {\n" +
					" object;\n" +
					"}\n"),
			addEndpoint(HttpMethod.POST, "", "${resource}s", resource, "", "",
			    RETURN_JSON + 
					"env.getScript(\"POST_${resource}_Validate.js\")();\n" + 
					"env.db.createObject(\"${resource}\", env.data);\n"),
			addEndpoint(HttpMethod.PUT, "", "${resource}s/([0-9]+)", resource, "", "", 
			    RETURN_JSON + 
					CHECK_OBJECT_EXISTANCE_BY_ID + 
					"env.getScript(\"PUT_${resource}_Validate.js\")();\n" +
					"env.db.updateObject(\"${resource}\", env.params[0], env.data);\n"),
			addEndpoint(HttpMethod.DELETE, "", "${resource}s/([0-9]+)", resource, "", "",
					CHECK_OBJECT_EXISTANCE_BY_ID + 
					"env.db.deleteObject(\"${resource}\", env.params[0]);\n\"Object deleted\"\n"),
		});
	}
	
	public List<Endpoint> generateAssociation(String sourceType, String associationName, String destType) {
		sourceType = sourceType.substring(0, 1).toUpperCase() + sourceType.substring(1);
		destType = destType.substring(0, 1).toUpperCase() + destType.substring(1);
		associationName = associationName.substring(0, 1).toLowerCase() + associationName.substring(1);
		return Arrays.asList(new Endpoint[] {
			addEndpoint(HttpMethod.GET, "", "${resource}s/([0-9]+)/${association}", sourceType, associationName, destType, 
			  RETURN_JSON + 
				CHECK_OBJECT_EXISTANCE_BY_ID + 
				"env.db.getAssociatedObjects(\"${resource}\", \"${association}\", \"${destResource}\", env.params[0]);\n"),	
			addEndpoint(HttpMethod.PUT, "", "${resource}s/([0-9]+)/${association}", sourceType, associationName, destType, 
			  RETURN_JSON + 
				CHECK_OBJECT_EXISTANCE_BY_ID + 
				"env.db.associate(\"${resource}\", \"${association}\", \"${destResource}\", env.params[0], JSON.parse(env.data).id);\n\"Objects associated\"\n"),
			addEndpoint(HttpMethod.DELETE, "", "${resource}s/([0-9]+)/${association}/([0-9]+)", sourceType, associationName, destType,
				CHECK_OBJECT_EXISTANCE_BY_ID +
				CHECK_DEST_OBJECT_EXISTANCE_BY_ID +	
				"env.db.dissociate(\"${resource}\", \"${association}\", \"${destResource}\", env.params[0], env.params[1]);\n\"Objects dissociated\"\n"),
			addEndpoint(HttpMethod.POST, "", "${resource}s/([0-9]+)/${association}", sourceType, associationName, destType,
			  RETURN_JSON + 
				"var object = JSON.parse(env.db.createObject(\"${destResource}\", env.data));\n" +
				this.generateScriptInvocation(HttpMethod.POST, "Validate", sourceType, associationName) +
				"env.db.associate(\"${resource}\", \"${association}\", \"${destResource}\", env.params[0], object.id);\n" +
				"JSON.stringify(object);\n")
		});
	}

	private Endpoint addEndpoint(HttpMethod method, String methodModifier, String urlRegex, String resource, String association, String destResource, String script) {
		String replacedRegex = this.replacePlaceholders(urlRegex, this.lower(resource), association, this.lower(destResource));
		Endpoint e = this.model.findEndpoint(method, replacedRegex);
		script = 
				this.generateScriptInvocation(method, methodModifier + (methodModifier.isEmpty() ? "" : "_") + "Pre", resource, association) + 
				script;
		String replacedScript = this.replacePlaceholders(script, this.upper(resource), association, this.upper(destResource));
		if (e != null) {
			return e;
		}
		return this.model.addEndpoint(new Endpoint(
				method, 
				 Pattern.compile(replacedRegex), 
				replacedScript
		));
	}

	private String generateScriptInvocation(HttpMethod method,
			String modifier, String resource, String association) {
		return "env.getScript(\"" + method.toString() + "_" + resource + 
				(association.isEmpty() ? "" : "_" + association) + 
				(modifier.isEmpty() ? "" : "_" + modifier) + ".js\")();\n";
	}
	
	private String upper(String string) {
		if (string.length() == 0) {
			return "";
		}
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}
	
	private String lower(String string) {
		if (string.length() == 0) {
			return "";
		}
		return string.substring(0, 1).toLowerCase() + string.substring(1);
	}

	private String replacePlaceholders(String script, String resource, String association, String destResource) {
		return script
			.replaceAll("\\$\\{resource\\}", resource)
			.replaceAll("\\$\\{association\\}", association)
			.replaceAll("\\$\\{destResource\\}", destResource);
	}
	
}
