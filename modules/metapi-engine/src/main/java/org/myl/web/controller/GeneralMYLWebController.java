package org.myl.web.controller;

import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.myl.web.controller.FileUploadForm;
import org.myl.resources.MYLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GeneralMYLWebController {
 
  @Autowired
  private MYLService mylService;
  

  @RequestMapping(value = "/projectsMYL", method = RequestMethod.GET)
  public ModelAndView projectsMYL() {
    return new ModelAndView("projectsMYL");
  }
  
  @RequestMapping(value = "/editorMYL/{name}", method = RequestMethod.GET)
  public ModelAndView editorMYL() {
    return new ModelAndView("editorMYL");
  }
  
  @RequestMapping(value = "/historyMYL/{name}", method = RequestMethod.GET)
  public ModelAndView historyMYL() {
    return new ModelAndView("historyMYL");

  }
  
  @RequestMapping(value = "/versionsMYL/{name}", method = RequestMethod.GET)
  public ModelAndView versionsMYL() {
    return new ModelAndView("versionsMYL");

  }
  
  @RequestMapping(value = "/projectsMYL/{applicationId}/upload", method = RequestMethod.POST)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadProjectFile(@PathVariable("applicationId") long applicationId, FileUploadForm zipfile) throws IOException {
    String dir = "./files/1/files/MYL/projects";
	this.mylService.uncompressContent(applicationId, zipfile.getFile().getInputStream(), dir);
    return "redirect:../../projectsMYL";
  }
 
  
  @RequestMapping(value = "/helpMYL", method = RequestMethod.GET)
  public ModelAndView helpMYL() {
    return new ModelAndView("helpMYL");
  }
  
 
}
