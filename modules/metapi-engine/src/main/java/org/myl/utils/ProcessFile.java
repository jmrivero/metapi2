package org.myl.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;



public class ProcessFile extends SimpleFileVisitor<Path> {
	
		private ArrayList<File> fileTreeList = new ArrayList<File>();

		
		
		public ProcessFile() {
			super();
		}

		public  ArrayList<File> getList(){
			return fileTreeList;
		}
		
		@Override 
	    public FileVisitResult visitFile(
	      Path aFile, BasicFileAttributes aAttrs
	    ) throws IOException {
	      setFileInTreeList(new File (aFile.toString()));
	      return FileVisitResult.CONTINUE;
	    }
	    
	    @Override  
		public FileVisitResult preVisitDirectory(
	      Path aDir, BasicFileAttributes aAttrs
	    ) throws IOException {
	      setFileInTreeList(new File(aDir.toString()));
	      return FileVisitResult.CONTINUE;
	    }
	    
	    private void setFileInTreeList(File fileName) {
	    	fileTreeList.add(fileName);
		}
	    
	    public boolean isFile(String aFile){
	    	//return true if the file have extenxion
	    	String ext = this.getFileExtension(aFile);
	    	if(!ext.equals("empty")){
		    	int lastPoint = aFile.lastIndexOf(".") + 1 ;
				int filelong = aFile.length();
		    	return (((filelong - lastPoint) <= 4) && (valid(ext)));
	    	}
	    	else{
	    		return false;
	    	}
	    }
	    
	    private String getFileExtension(String name) {
	        int lastIndexOf = name.lastIndexOf(".");
	        if (lastIndexOf == -1) {
	            return "empty"; // empty extension
	        }
	        return name.substring(lastIndexOf);
	    }
	 
	    
		private boolean valid(String extension) {
			Boolean res = true;
			switch (extension) {
			  case ".js":
			        break;
			  case ".java":
			        break;
			  case ".json":
			        break;
			  case ".txt":
			        break;
			  case ".html":
			        break;
			  case ".jsp":
			        break;
			  case ".xml":
			        break;
			  case ".cs":
			        break;
			  case ".css":
			        break;
			  default:
			        res = false;
			        System.out.println("Invalid File for MYL");
			        break;
			}
			return res;
		}
		
		public boolean isJSONValid(String test) {
		    try {
		        new JSONObject(test);
		    } catch (JSONException ex) {
		        // edited, to include @Arthur's comment
		        // e.g. in case JSONArray is valid as well...
		        try {
		            new JSONArray(test);
		        } catch (JSONException ex1) {
		            return false;
		        }
		    }
		    return true;
		}

			
}
