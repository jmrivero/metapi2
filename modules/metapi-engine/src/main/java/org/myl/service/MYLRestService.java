package org.myl.service;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONException;
import org.myl.utils.JsonBuilder;
import org.myl.core.MakeYourLanguage;
import org.myl.model.JSONModel;
import org.myl.model.Project;
import org.myl.resources.MYLService;
import org.myl.utils.ProcessFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;



@Component
@Path("/MYL/")
public class MYLRestService {

	private static Log log = LogFactory.getLog(MYLRestService.class);
	
	@Autowired
	@Qualifier("mylService")
	private MYLService mylService;
	
	@Autowired
	@Qualifier("mylCore")
	private MakeYourLanguage mylCore;


	public MYLRestService() {
	}

	@GET
	@Path("{applicationId}/projects")
	public Response getAllFilesWithoutMYL(@PathParam("applicationId") long applicationId) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			List<Project> l = this.mylService.listProjects(applicationId);
			if (!l.isEmpty()){
				for (Project filename : this.mylService.listProjects(applicationId)) {
					if (!filename.getName().contains("_MYL") && !filename.getName().contains("_Models") && !filename.getName().contains("_Rules")){
						builder.string(filename.getName());
					}
				}
				builder.endArray();
				return Response.ok(builder.build()).build();
			}
			else{
				builder.string("empty");
				builder.endArray();
				return Response.ok(builder.build()).build();
			}
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	
	@GET
	@Path("{applicationId}/projectsMYL")
	public Response getAllFilesWithMYL(@PathParam("applicationId") long applicationId) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			for (Project filename : this.mylService.listProjects(applicationId)) {
				if (filename.getName().contains("_MYL")){
					builder.string(filename.getName());
				}
			}
			builder.endArray();
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}

	@GET
	@Path("{applicationId}/editorMYL/{name}")
	public Response getAllFilesFolder(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject) {
		/*try {
			
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			ArrayList<String> array = this.mylService.listFilesOfProject(applicationId,nameOfProject);
			for (int i = 0; i < array.size(); i++) {
				builder.string(array.get(i));
				builder.string(array.get(i+1));
				i++;
			}	
			builder.endArray();
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		*/
		return Response.status(404).entity("Files not found").build();
	}
	
	
	@GET
	@Path("{applicationId}/models/{name}")
	public Response getAllModelsForProject(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			
			for (JSONModel filename : this.mylService.listModelsOfProject(applicationId,nameOfProject)) {
				builder.string(filename.getName());
			}
			builder.endArray();
		
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	
	@GET
	@Path("{applicationId}/projectsMYL/{name}")
	public Response getAllFilesFolderMYL(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			for (JSONModel filename : this.mylService.listModelsOfProject(applicationId,nameOfProject)) {
				builder.string(filename.getName());
			}
			builder.endArray();
		
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	
	@GET
	@Path("{applicationId}/versions/{name}")
	public Response getAllVersionsOfProject(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			Boolean hay = false;
			List<Project> l = this.mylService.listProjects(applicationId);
			if (!l.isEmpty()){
				for (Project filename : this.mylService.listProjects(applicationId)) {
					if (filename.getName().equals(nameOfProject)){
						builder.string(filename.getName());
						hay = true;
					}
				}
				if(!hay){
					builder.string("empty");
					builder.endArray();
					return Response.ok(builder.build()).build();
				}
				else{
					builder.endArray();
					return Response.ok(builder.build()).build();
				}
			}
			
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	

	@GET
	@Path("{applicationId}/{folderPath:.+}/{filePath:.+}")
	public Response getFile(@PathParam("applicationId") long applicationId, @PathParam("folderPath") String folderPath, @PathParam("filePath") String nameOfFile) throws FileNotFoundException {
		String realPathFolder;
		if(folderPath.equals(nameOfFile)){
			realPathFolder = nameOfFile;
		}
		else{
			realPathFolder = this.mylService.getPathOfFile(folderPath, nameOfFile);
		}
		InputStream result = null;
		ProcessFile pf = new ProcessFile();
		if(!pf.isFile(nameOfFile)){
			//System.out.println("es una carpeta y el path es: "+ realPathFolder);
			//result = this.mylService.getContentOfFolder(realPathFolder);
			//result = this.mylService.getContentsOfResource(applicationId, realPathFolder);
		}
		else{
			result = this.mylService.getContentsOfResourceInFolder(applicationId,realPathFolder, nameOfFile);
		}
		if (result == null) {
			return Response.ok("You have selected folder:  " + realPathFolder).build();
		}
		else{
			return Response.ok(result).build();
		}
	}
	
	@PUT
	@Path("{applicationId}/{folderPath:.+}/{filePath:.+}")
	public Response putFile(@PathParam("applicationId") long applicationId, @PathParam("folderPath") String folderPath, @PathParam("filePath") String path, @RequestBody InputStream stream) {
		String route;
		if(folderPath.contains("_Rules")){
			route = folderPath;
		}
		else{
			route = this.mylService.getPathOfFile(folderPath, path);
		}
		this.mylService.storeFileInFolder(applicationId, route, path, stream);
		return Response.ok("File stored").build();
	}
		
	
	
	@PUT
	@Path("{applicationId}/infer/{name}/{file}")
	public Response inferSampleModel(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject, @PathParam("file") String nameOfFile) throws JSONException, IOException {
		String route = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
		ProcessFile pf = new ProcessFile();
		if(!pf.isFile(nameOfFile)){
			mylCore.inferSampleModelFromRoot(nameOfProject);
		}
		else{
			InputStream result = this.mylService.getContentsOfResourceInFolder(applicationId,route, nameOfFile);
			mylCore.inferSampleModelRest(nameOfProject , result, nameOfFile);
		}
		return Response.ok("s").build();
	}
	
	@PUT
	@Path("{applicationId}/render/{name}/{file}/{model}")
	public Response renderModel(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject, @PathParam("file") String nameOfFile, @PathParam("model") String nameOfModel) throws Exception {
		ProcessFile pf = new ProcessFile();
		InputStream resultValidate = null;		
		String routeModel = this.mylService.getPathOfFile(nameOfProject+"_Models", nameOfModel);
		InputStream modelIS = this.mylService.getContentsOfResourceInFolder(applicationId,routeModel, nameOfModel);
		String model = IOUtils.toString(modelIS, "UTF-8");		
		String routeRule = this.mylService.getPathOfFile(nameOfProject+"_Rules", nameOfProject+"_rule.js");
		InputStream ruleIS = this.mylService.getContentsOfResourceInFolder(applicationId,routeRule, nameOfProject+"_rule.js");
		String rule = IOUtils.toString(ruleIS, "UTF-8");	
		resultValidate = this.mylCore.validateLanguaje(rule, model);	
		StringWriter writer = new StringWriter();
		IOUtils.copy(resultValidate, writer, "UTF-8");
		String resultValidateString = writer.toString();
		if(resultValidateString.contains("Error")){
			return Response.status(404).entity(resultValidateString).type("text/plain").build();
		}
		else{
			if(mylService.validateModel(nameOfModel, nameOfProject)){
				if(!pf.isFile(nameOfFile)){
					mylCore.renderFromRoot(nameOfProject, nameOfModel);
				}
				else{
					String route = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
					InputStream result = this.mylService.getContentsOfResourceInFolder(applicationId,route, nameOfFile);
					mylCore.renderTemplatesRest(nameOfProject,nameOfFile, nameOfModel, result);
				}
				return Response.ok("s").build();
			}
			else{
				log.error("Error validate JSON Model in render method");
				return Response.status(404).entity("Invalid JSON Model").build();
			}
		}	
	}
	
	@GET
	@Path("{applicationId}/{folderPath:.+}")
	public String getTreeFile(@PathParam("applicationId") long applicationId, @PathParam("folderPath") String folderPath) throws IOException, JSONException {
		String j = mylService.getFullTreeAsString(folderPath);
		return j;
	}	
	
	@DELETE
	@Path("{applicationId}/del/{name}/{file}")
	public Response deleteModel(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject, @PathParam("file") String nameOfFile) throws JSONException, IOException {
		if(nameOfFile.contains("_rule.js")){
			mylService.deleteModel(nameOfProject, nameOfFile);
		}
		else{
			mylService.deleteModel(nameOfProject , nameOfFile);
		}
		return Response.ok("s").build();
	}
	
	@PUT
	@Path("{applicationId}/edit/{folderPath:.+}/{filePath:.+}/{newfilePath:.+}")
	public Response editNameOfFile (@PathParam("applicationId") long applicationId, @PathParam("folderPath") String nameOfProject, @PathParam("filePath") String nameOfFile, @PathParam("newfilePath") String newNameOfFile) throws FileNotFoundException {
		String route = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
		mylService.editName(route , nameOfFile, newNameOfFile);
		return Response.ok("good").build();
		
	}
	
	@DELETE
	@Path("{applicationId}/delete/{folderPath:.+}/{filePath:.+}")
	public Response delFile (@PathParam("applicationId") long applicationId, @PathParam("folderPath") String nameOfProject, @PathParam("filePath") String nameOfFile) throws FileNotFoundException {
		String route = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
		mylService.delFile(route , nameOfFile);
		return Response.ok("good").build();
		
	}
	

	@GET
	@Path("{applicationId}/projectsMYL/{projectname}/{filename}/dwl")
	@Produces("application/zip")
	public Response dwldFile (@PathParam("applicationId") long applicationId, @PathParam("filename") String nameOfFile, @PathParam("projectname") String nameOfProject) throws IOException, InterruptedException {	
		String route;
		if(nameOfFile.equals(nameOfProject)){			
		try {
    	 	route = nameOfProject + "/"; 
    	 	File dir = this.mylService.getFile(route);
    	 	String routeRules,routeModels;
    	 	routeRules = route.replace("_MYL", "_Rules");
    	 	routeModels = route.replace("_MYL", "_Models");
    	 	File dirRules = this.mylService.getFile(routeRules);
    	 	File dirModels = this.mylService.getFile(routeModels);
    	 	File[] files = dir.listFiles();
    	 	File[] rules = dirRules.listFiles();
    	 	File[] models = dirModels.listFiles();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
            for(File file : files){
              zipOutputStream.putNextEntry(new ZipEntry(nameOfFile + File.separator + file.getName()));
              IOUtils.write(IOUtils.toByteArray(new FileInputStream(this.mylService.getFile(route + file.getName()))), zipOutputStream);
            }
            for(File rule : rules){
                zipOutputStream.putNextEntry(new ZipEntry("Rules" + File.separator + rule.getName()));
                IOUtils.write(IOUtils.toByteArray(new FileInputStream(this.mylService.getFile(routeRules + rule.getName()))), zipOutputStream);
            }
            for(File model : models){
                zipOutputStream.putNextEntry(new ZipEntry("Models" + File.separator + model.getName()));
                IOUtils.write(IOUtils.toByteArray(new FileInputStream(this.mylService.getFile(routeModels + model.getName()))), zipOutputStream);
            }
            zipOutputStream.close();
            ByteArrayInputStream salida = new ByteArrayInputStream(outputStream.toByteArray());
            return Response.ok(salida).header("content-disposition","attachment; filename =\"" + nameOfProject + "\"").build();  
             
		} catch (IOException e) {
            e.printStackTrace();
          }	
		}
		else{
			route = nameOfProject +"/"+ nameOfFile;
			File file = mylService.getFile(route);    
	        ResponseBuilder response = Response.ok((Object) file);
	        response.header("Content-Disposition", "attachment; filename=\""+nameOfFile+"\"");
	        //return Response.ok(file, "application/zip").build();
	        return response.build();
		}
		return null;	
	}
	



	@GET
	@Path("{applicationId}/rules/{name}")
	public Response getValidateRuleOfFile(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject) {
		String ruleName = nameOfProject.replace("_Rules", "_rule.js");
		String realPathFolder = this.mylService.getPathOfFile(nameOfProject, ruleName);
		InputStream result = null;
		result = this.mylService.getContentsOfResourceInFolder(applicationId,realPathFolder, ruleName);
		return Response.ok(result).build();
	}
	
	@POST
	@Path("{applicationId}/rules/{name}/{filename}")
	public Response excecuteCheckRule(@PathParam("applicationId") long applicationId,@PathParam("name") String nameOfProject, @PathParam("filename") String nameOfFile, String jsRequest) throws IOException, JSONException {
		//String realPathFolder = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
		
		String route = this.mylService.getPathOfFile(nameOfProject, nameOfFile);
		InputStream modelIS = this.mylService.getContentsOfResourceInFolder(applicationId,route, nameOfFile);
		String model = IOUtils.toString(modelIS, "UTF-8");
		InputStream result = null;
		result = this.mylCore.validateLanguaje(jsRequest, model);
		return Response.ok(result).build();
	}
	
}
