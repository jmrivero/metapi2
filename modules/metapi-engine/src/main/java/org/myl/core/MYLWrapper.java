package org.myl.core;




import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.core.ScriptExecutionEnvironment;

/**
* The MYL Project Wrapper
*
* @author  Alan Garcia Camiña
* @version 1.0
* @since   2014-06-16 
*/
public class MYLWrapper {

	private MakeYourLanguage myl;
	@SuppressWarnings("unused")
	private ScriptExecutionEnvironment environment;
	
	public MYLWrapper(MakeYourLanguage mylParameter, ScriptExecutionEnvironment environment) {
		this.environment = environment;
		this.myl = mylParameter;
	}
	
	public Object renderTemplate(String pathFile, String jsonStr) throws Exception{
		return (this.myl.renderTemplate(pathFile, jsonStr));
	}
	
	
	public JSONObject inferSampleModelFromString(String aFile) throws JSONException {
		return (this.myl.inferSampleModelFromString(aFile));
	}

	
	public String renderTemplatesInFolder(String path, String jsonFile, String render) throws Exception{
		return (this.renderTemplatesInFolder(path, jsonFile, render));
	}
	
}
