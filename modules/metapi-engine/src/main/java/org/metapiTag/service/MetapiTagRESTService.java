package org.metapiTag.service;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.metapi.resources.ResourceService;
import org.metapi.utils.JsonBuilder;
import org.metapiTag.resources.MetapiTagService;
import org.myl.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.sun.jersey.multipart.FormDataParam;

@Component
@Path("/metapiTag/")
public class MetapiTagRESTService {

	private static Log log = LogFactory.getLog(MetapiTagRESTService.class);
	
	@Autowired
	@Qualifier("resourceService")
	private ResourceService resourceService;

	@Autowired
	@Qualifier("metapiTagService")
	private MetapiTagService metapiTagService;

	
	public MetapiTagRESTService() {
	}

	@GET
	@Path("{applicationId}/projects")
	public Response getAllProject(@PathParam("applicationId") long applicationId) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			List<String> l = this.metapiTagService.listProjects(applicationId);
			if (!l.isEmpty()){
				for (String filename : this.metapiTagService.listProjects(applicationId)) {
						builder.string(filename);
				}
				builder.endArray();
				return Response.ok(builder.build()).build();
			}
			else{
				builder.string("empty");
				builder.endArray();
				return Response.ok(builder.build()).build();
			}
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	
	@GET
	@Path("{applicationId}")
	public Response getAllFiles(@PathParam("applicationId") long applicationId) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			for (String filename : this.resourceService.listResources(applicationId)) {
				builder.string(filename);
			}
			builder.endArray();
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}

	@GET
	@Path("{applicationId}/{filePath}")
	public Response getAllFilesFolder(@PathParam("applicationId") long applicationId,@PathParam("filePath") String path) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			for (String filename : this.resourceService.listResources(applicationId)) {
				builder.string(filename);
			}
			builder.endArray();
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}

	@GET
	@Path("{applicationId}/{project:.+}")
	public Response getAllFilesProject(@PathParam("applicationId") long applicationId,@PathParam("project") String project) {
		try {
			JsonBuilder builder = JsonBuilder.newWithArray("files");
			for (String filename : this.metapiTagService.listFilesOfProject(applicationId,project)) {
				builder.string(filename);
			}
			builder.endArray();
			return Response.ok(builder.build()).build();
		} catch (Exception e) {
			log.error("Error building file list", e);
		}
		return Response.status(404).entity("Files not found").build();
	}
	
	@GET
	@Path("{applicationId}/{folderPath:.+}/{filePath:.+}")
	public Response getFile(@PathParam("applicationId") long applicationId, @PathParam("folderPath") String folderPath, @PathParam("filePath") String path) throws FileNotFoundException {
		String realPathFolder = this.metapiTagService.getPathOfFile(folderPath, path);
		InputStream result = null;
		result = this.metapiTagService.getContentsOfResourceInFolder(applicationId,realPathFolder, path);
		if (result == null) {
			return Response.ok("You have selected folder:  " + realPathFolder).build();
		}
		else{
			return Response.ok(result).build();
		}
	}
	
	@DELETE
	@Path("{applicationId}/{filePath:.+}")
	public Response deleteFile(@PathParam("applicationId") long applicationId, @PathParam("filePath") String path) {
		this.resourceService.deleteResource(applicationId, path);
		return Response.ok("File deleted").build();
	}

	@PUT
	@Path("{applicationId}/{filePath:.+}")
	public Response putFile(@PathParam("applicationId") long applicationId, @PathParam("filePath") String path, @RequestBody InputStream stream) {
		this.resourceService.storeResource(applicationId, path, stream);
		return Response.ok("File stored").build();
	}

	@POST
	@Path("{applicationId}/compressed")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response postCompressedFiles(@PathParam("applicationId") long applicationId, @FormDataParam("zipfile") InputStream stream) {
		this.resourceService.uncompressContent(applicationId, stream);
		return Response.ok().build();
	}
	
	@GET
	@Path("{applicationId}/snapshots/{snapshotId}")
	public Response getSnapshot(@PathParam("applicationId") String applicationId, @PathParam("snapshotId") String snapshotId) {
		InputStream result = this.resourceService.getContentsOfSnapshot(applicationId, snapshotId);
		if (result == null) {
			return Response.status(404).entity("File not found").build();
		}
		return Response.ok(result).build();
	}
}
