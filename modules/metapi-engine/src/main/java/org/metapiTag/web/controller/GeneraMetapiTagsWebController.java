package org.metapiTag.web.controller;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.metapiTag.resources.MetapiTagService;
import org.metapi.web.controller.FileUploadForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GeneraMetapiTagsWebController {

  @Autowired
  private MetapiTagService metapiTagService;

  @RequestMapping(value = "metapiTag", method = RequestMethod.GET)
  public ModelAndView index() {
    return new ModelAndView("metapiTag/index");
  }
  @RequestMapping(value = "metapiTag/editor", method = RequestMethod.GET)
  public ModelAndView editor() {
    return new ModelAndView("metapiTag/editor");
  }
  
  @RequestMapping(value = "metapiTag/projects", method = RequestMethod.GET)
  public ModelAndView projects() {
    return new ModelAndView("metapiTag/projects");
  }
  
  @RequestMapping(value = "metapiTag/{applicationId}/upload", method = RequestMethod.POST)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public String uploadResourcesFile(@PathVariable("applicationId") long applicationId, FileUploadForm zipfile) throws IOException {
    this.metapiTagService.uncompressContent(applicationId, zipfile.getFile().getInputStream());
    return "redirect:../projects";
  }
  
}