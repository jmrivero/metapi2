package org.metapi.templating.mustache;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class MustacheTemplateUnitTest {

  @Test
  public void testNestedMapsInTemplate() throws IOException {
     HashMap<String, Object> scopes = new HashMap<String, Object>();
     scopes.put("name", "Mustache");
     Map<String, String> innerMap = new HashMap<String, String>();
     scopes.put("feature", innerMap);
     
     innerMap.put("description", "nice template engine!");

     StringWriter writer = new StringWriter();
     MustacheFactory mf = new DefaultMustacheFactory();
     Mustache mustache = mf.compile(new StringReader("{{name}}, {{feature.description}}"), "example");
     mustache.execute(writer, scopes);
     
     assertEquals(writer.getBuffer().toString(), "Mustache, nice template engine!");
     writer.flush();
  }
  
  @Test
  public void testNestedListsInTemplate() throws IOException {
     HashMap<String, Object> scopes = new HashMap<String, Object>();
     scopes.put("name", "Mustache");
     List<Object> innerList = new ArrayList<Object>();
     scopes.put("features", innerList);
     
     innerList.add(this.buildHashMap("name", "simple, "));
     innerList.add(this.buildHashMap("name", "efficient"));

     StringWriter writer = new StringWriter();
     MustacheFactory mf = new DefaultMustacheFactory();
     Mustache mustache = mf.compile(new StringReader("{{name}} is {{#features}}{{name}}{{/features}}"), "example");
     mustache.execute(writer, scopes);
     
     assertEquals(writer.getBuffer().toString(), "Mustache is simple, efficient");
     writer.flush();
  }

  private Map<String, Object> buildHashMap(String key, String value) {
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put(key, value);
    return map;
  }
  
  
}
