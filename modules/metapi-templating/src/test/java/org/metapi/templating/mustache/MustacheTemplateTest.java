package org.metapi.templating.mustache;

import static org.junit.Assert.*;

import org.junit.Test;
import org.metapi.templating.TemplateModelException;


public class MustacheTemplateTest {

  @Test
  public void simpleTemplateTest() throws TemplateModelException {
    String result = new MustacheTemplate("Hi, {{name}}")
      .model()
        .prop("name", "Matias!")
      .modelEnd()
      .render();
    assertEquals("Hi, Matias!", result);
  }
  
  @Test
  public void nestedObjectsTest() throws TemplateModelException {
    String result = new MustacheTemplate("Hi, {{#user}}{{name}}{{/user}}")
      .model()
        .object("user")
          .prop("name", "Matias!")
        .end()
      .modelEnd()
      .render();
    assertEquals("Hi, Matias!", result);
  }
  
  @Test
  public void listTest() throws TemplateModelException {
    String result = new MustacheTemplate("Hi, {{#users}}{{name}}{{/users}}!")
      .model()
        .list("users")
          .object().prop("name", "Matias, ").end()
          .object().prop("name", "Juan").end()
        .end()
      .modelEnd()
      .render();
    assertEquals("Hi, Matias, Juan!", result);
  }
  
  @Test
  public void jsonRenderTest() throws Exception {
    String result = new MustacheTemplate("Hi, {{#users}}{{name}}{{/users}}!")
      .jsonRender("{users:[{'name':'Matias, '},{'name':'Juan'}]}");
    assertEquals("Hi, Matias, Juan!", result);
  }
  
}