package org.metapi.templating.mustache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.metapi.templating.ContextualTemplateModelBuilder;
import org.metapi.templating.TemplateModelException;


public class MustacheTemplateModelBuilder implements ContextualTemplateModelBuilder<MustacheTemplate> {

  private Stack<Object> stack;
  private MustacheTemplate template;
  
  public MustacheTemplateModelBuilder(MustacheTemplate template) {
    this.stack = new Stack<Object>();
    stack.push(new HashMap<String, String>());
    this.template = template;
  }

  @Override
  public MustacheTemplateModelBuilder list(String name) throws TemplateModelException {
    List<Object> list = new ArrayList<Object>();
    this.getMapOnTop().put(name, list);
    this.stack.push(list);
    
    return this;
  }

  @Override
  public MustacheTemplateModelBuilder prop(String name, Object value) throws TemplateModelException {
    this.getMapOnTop().put(name, value);
    return this;
  }

  @Override
  public MustacheTemplateModelBuilder end() throws TemplateModelException {
    Object object = this.stack.pop();
    return this;
  }
  
  @Override
  public ContextualTemplateModelBuilder<MustacheTemplate> object(String name) throws TemplateModelException {
    Map<String, Object> object = new HashMap<String, Object>();
    this.getMapOnTop().put(name, object);
    this.stack.push(object);
    return this;
  }
  

  @Override
  public ContextualTemplateModelBuilder<MustacheTemplate> object() throws TemplateModelException {
    HashMap<String, Object> object = new HashMap<String, Object>();
    this.getListOnTop().add(object);
    this.stack.push(object);
    return this;
  }

  @SuppressWarnings("unchecked")
  @Override
  public MustacheTemplate modelEnd() {
    this.template.setModel((Map<String, Object>) this.stack.get(0));
    return this.template;
  }
  
  @SuppressWarnings("unchecked")
  private Map<String, Object> getMapOnTop() throws TemplateModelException {
    if (stack.lastElement() instanceof Map) {
      return (Map<String, Object>) stack.lastElement();
    }
    throw new TemplateModelException("Not an object");
  }
  
  @SuppressWarnings("unchecked")
  private List<Object> getListOnTop() throws TemplateModelException {
    if (stack.lastElement() instanceof List) {
      return (List<Object>) stack.lastElement();
    }
    throw new TemplateModelException("Not a list");
  }



}
