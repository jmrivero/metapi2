package org.metapi.templating;


public interface ContextualTemplateModelBuilder<ContextType> {

  ContextualTemplateModelBuilder<ContextType> list(String name) throws TemplateModelException;
  
  ContextualTemplateModelBuilder<ContextType> prop(String name, Object value) throws TemplateModelException;
  
  ContextualTemplateModelBuilder<ContextType> object() throws TemplateModelException;
  
  ContextualTemplateModelBuilder<ContextType> end() throws TemplateModelException;
  
  ContextualTemplateModelBuilder<ContextType> object(String name) throws TemplateModelException;
  
  ContextType modelEnd();  
  
}
