package org.metapi.templating;

import java.util.Iterator;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class JsonModelBuilderLoader<ContextType> {

  private ContextualTemplateModelBuilder<ContextType> modelBuilder;

  public JsonModelBuilderLoader(ContextualTemplateModelBuilder<ContextType> modelBuilder) {
    this.modelBuilder = modelBuilder;
  }
  
  public void load(String json) throws Exception {
    JSONObject jsonObject = new JSONObject(json);
    processProperties(jsonObject);
    this.modelBuilder.modelEnd();
  }

  private void processProperties(JSONObject jsonObject) throws Exception, JSONException {
    Iterator keys = jsonObject.keys();
    while (keys.hasNext()) {
      String key = keys.next().toString();
      this.processObject(key, jsonObject.get(key));
    }
  }

  private void processObject(String name, Object object) throws Exception {
    if (object instanceof JSONObject) {
      this.processJsonObject(name, (JSONObject) object);
    } else if (object instanceof JSONArray) {
        this.processJsonArray(name, (JSONArray) object);
    } else {
      this.modelBuilder.prop(name, object);
    }
  }

  private void processJsonArray(String name, JSONArray array) throws Exception {
    this.modelBuilder.list(name);
    for (int i = 0; i < array.length(); i++){
      this.processObject(null, array.get(i));
    }
    this.modelBuilder.end();
  }

  private void processJsonObject(String name, JSONObject object) throws Exception {
    if (name != null) {
      this.modelBuilder.object(name);
    } else {
      this.modelBuilder.object();
    }
    this.processProperties(object);
    this.modelBuilder.end();
  }
  
  
}
