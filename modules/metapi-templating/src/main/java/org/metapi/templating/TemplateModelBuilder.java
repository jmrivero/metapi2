package org.metapi.templating;


public interface TemplateModelBuilder<T> {

  TemplateModelBuilder<T> list(String name) throws TemplateModelException;
  
  TemplateModelBuilder<T> prop(String name, Object value) throws TemplateModelException;
  
  TemplateModelBuilder<T> nextElem() throws TemplateModelException;
  
  TemplateModelBuilder<T> end() throws TemplateModelException;
  
  T build();
  
}
