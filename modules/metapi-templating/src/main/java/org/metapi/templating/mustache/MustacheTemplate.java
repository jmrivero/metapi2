package org.metapi.templating.mustache;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.metapi.templating.JsonModelBuilderLoader;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;


public class MustacheTemplate {

  private String template;
  private Map<String, Object> model;
  
  public MustacheTemplate(String template) {
    super();
    this.template = template;
  }

  public void setModel(Map<String, Object> model) {
    this.model = model;
  }
  
  public MustacheTemplateModelBuilder model() {
    return new MustacheTemplateModelBuilder(this);    
  }
  
  public String jsonRender(String json) throws Exception {
    new JsonModelBuilderLoader<MustacheTemplate>(new MustacheTemplateModelBuilder(this)).load(json);
    return this.render();
  }
  
  public String render() {
    StringWriter writer = new StringWriter();
    MustacheFactory mf = new DefaultMustacheFactory();
    Mustache mustache = mf.compile(new StringReader(this.template), "singleTemplate");
    mustache.execute(writer, this.model);
    writer.flush();
    return writer.getBuffer().toString();
  }
 
}
