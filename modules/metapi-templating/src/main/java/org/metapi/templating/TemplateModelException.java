package org.metapi.templating;


public class TemplateModelException extends Exception {

  private static final long serialVersionUID = 1L;

  public TemplateModelException(String message, Throwable cause) {
    super(message, cause);
  }

  public TemplateModelException(String message) {
    super(message);
  }
  
  

}
