TextPopup = Class.extend({

	init: function(canvas, text, title) {
		var popup = 
			$(('<div class="textPopup" title="$title">' +
				'<p><textarea></textarea></p>' +
			'</div>')
			.replace("$title", title ? title : ""));
		popup.find('textarea').val(text);
		canvas.append(popup);
		popup.dialog({modal: true, height: 300, width: 500, resizable: false});
	}

});