<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Make Your Language</title>

<script src="lib/codemirror.js"></script><style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript" ></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<link rel="stylesheet" href="css/MYLStyle.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

</head>
<body style="">
  
<nav class="navbar navbar-default" role="navigation">
 <a class="navbar-brand" href="#">Make Your Language Project</a><img id="icon-menu" src="../images/factory.png"/>
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul id="menu" class="nav navbar-nav">
			 <li> <a href="projectsMYL">Home</a></li>
			 <li> <a href="helpMYL">Help</a></li>
		</ul>
		
	</div>
</nav>





  <script type="text/javascript">
    var templateEditor;
    var currentFilename = null;
    var newFile = false;
    var applicationId = 2;
    var popupManager = new PopupManager();
    
    var extensionModeMappings = {
    	js: "javascript",
    	xml: "xml",
    	html: "htmlmixed",
    	htm: "htmlmixed",
    	css: "css",
    	tl: "dust"
    }
    
    
    
    var projects = "../metapi/MYL/1/projects"
   
  
    	    
    function extractExtension(filename) {
    	var parts = filename.split(".");
      return parts[parts.length - 1];
    }
    
    function updateEditorMode(filename) {
    	var extension = extractExtension(filename);
    	if (extensionModeMappings[extension]) {
    		templateEditor.setOption("mode", extensionModeMappings[extension]);
    	}
    }

    

    
    
    function addFileToList(filename) {
        var html = "<li  class=\"list-group-item\"><span class=\"pink\"><a href=\"editorMYL/"+filename+"\" class=\"btn btn-link btn-xs\"><span class=\"glyphicon glyphicon-hand-right\"></span>  "+filename+"</a><a class=\"icon icon-building\" title=\"View & Edit "+filename+"  in the factory\" href=\"editorMYL/"+filename+"\"></a><a class=\"icon icon-graph\" title=\"Browse old versions of "+filename+ "\"  href=\"./historyMYL/"+ filename +"_MYL" + "\"></a>";
        html = html + "</li>";
        $("#fileListP").append(html);    
    }
    
    
    
    
    $(document).ready(function() {
    	  $.ajax({url: projects, type: "GET", dataType: 'json', success: function(response) {
    	   
    		if(response.files[0] != "empty"){
    			$.each(response.files, function(_, filename) {
            	 addFileToList(filename);
            	});
            }
    		else{
    			
      			$( ".title" ).remove();
    			$("#fileListP").append("<h4 class=\"center\">Wlcome to <strong>Make Your language!</strong></h4><br><p class=\"text-center\">Let's start! upload your project and start using <strong>Make Your Language. </strong><small>Please click in <em>UploadZip.</em></small></p>"); 
    		}
          }});
    	  

        
        $("#uploadZip").click(function(e) {
            var self = this;
            e.preventDefault();  
            $("#zipFile").trigger("click");
        });  
        
        $("#zipFile").change(function(e) {
        	$("#zipUploadForm").submit()
        }); 
        

        
    
        
    });
    
    

    
		</script>

  <div class="container">

    <h4 class="titleMYL">Project Manager.<span id="filename"> <i>Manage your projects.</i></span></h4>


	<div class="row-rigth">
		 <p class="text-info" >Start here! </p>
    <div class="row">
      
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
     
        <form id="zipUploadForm" action="projectsMYL/1/upload" method="POST" enctype="multipart/form-data">
          <input id="zipFile" type="file" name="file" style="display: none">
         
         <button id="uploadZip" class="btn btn-info btn-sm">Upload Zip <span class="glyphicon glyphicon-compressed"></span></button>
    
			
        </form>
      </div>
    </div>

   


  

    </div>

    	
    	
    <br>
    <h4>List of projects</h4>
    <hr>
  
		<ul id="fileListP" class="list-group">
		
		</ul>
		

		
  </div>
 
  <blockquote class="center">
	 
	  <footer id="footer">
	  
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MYL Project</cite>
	  </footer>
  </blockquote>

</body></html>
