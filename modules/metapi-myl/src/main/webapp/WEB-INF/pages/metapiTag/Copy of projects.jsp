<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>MetapiTags Projects</title>

<script src="../lib/codemirror.js"></script>
<style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/bootstrap.min.js"></script>
<script src="../lib/alert.js"></script>

<script src="../libs/class.js" type="text/javascript"></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript"></script>
<script src="../libs/noty/themes/default.js" type="text/javascript"></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript"></script>

<link rel="stylesheet" href="../css/metapiTag/style.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<style>
#alerts {
	/*height: 63px;*/
	
}

#fileList {
	list-style: none;
	margin: 0px;
	padding: 0px;
	height: 437px;
	overflow-y: scroll;
}
</style>
</head>
<body style="">


	<script type="text/javascript">
		var projects;
		var projectId;
		var templateEditor;
		var currentFilename = null;
		var newFile = false;
		var applicationId = 2;
		var popupManager = new PopupManager();

		var extensionModeMappings = {
			js : "javascript",
			xml : "xml",
			html : "htmlmixed",
			htm : "htmlmixed",
			css : "css",
			tl : "dust"
		}

		var metapiTagResourcePath = "/../../metapi/metapiTag/1/";
		//"/../metapi/resources/1/metapiTags/"

		function loadProjects() {
			cleanProjectList();
			projects = {};
			$.ajax({
				url : "/../../metapi/metapiTag/1/projects",
				type : "GET",
				dataType : "json",
				success : function(projects) {
					for (i in projects) {
						addProject(projects[i]);
						if (projectId) {
							$("#projects").val(projectId);
						} else {
							$("#projects").val("(new)");
						}
					}
					;
				}
			});
		}

		function loadFilesProject(project) {
			$.ajax({
				url : metapiTagResourcePath + project,
				type : "GET",
				dataType : 'json',
				success : function(response) {
					$.each(response.files, function(_, filename) {
						addFileToList(filename);
					});
				}
			});
		}

		function addProject(project) {
			$("#projects").append(
					'<option value="[value]">[label]</option>'.replace(
							"[value]", project).replace("[label]", project));
			//tags[tag.tag] = tag;
		}
		function cleanProjectList() {
			$("#projects option").remove();
			$("#projects").append('<option value="(new)">(new)</option>');
		}

		function extractExtension(filename) {
			var parts = filename.split(".");
			return parts[parts.length - 1];
		}

		function updateEditorMode(filename) {
			var extension = extractExtension(filename);
			if (extensionModeMappings[extension]) {
				templateEditor.setOption("mode",
						extensionModeMappings[extension]);
			}
		}

		function fileSelected(filename, content) {
			templateEditor.setValue(content);
			updateEditorMode(filename);
			$("#filename").text(filename);
			currentFilename = filename;
			$("#editorContainer").fadeIn(500);
			newFile = false;
		}

		function closed() {
			$("#not").remove();
		}

		function fileSaved(filename, content) {
			popupManager.showSuccess("Resource saved");
			if (newFile) {
				addFileToList(filename);
				newFile = false;
			}
		}
		function cleanFields() {
			$("#fileList li").remove();
		}
		function addFileToList(filename) {
			var html = "<li>";
			filename = filename[0] == '/' ? filename.substring(1) : filename;
			var extension = extractExtension(filename);
			html = html
					+ '<a  class="btn btn-xs deleteFile" href="/../../metapi/api/tagginTool/'+projectId+'/[filename]">tagging</a>'
							.replace(/\[filename\]/g, filename);
			html = html
					+ '<a class="btn btn-xs deleteFile" href="/../../metapi/api/tagginRun/'+projectId+'/[filename]">run  </a'
							.replace(/\[filename\]/g, filename);
					
			if (extensionModeMappings[extension]) {
				html = html
						+ '<a href="" class="fileLink">[filename]</a>'
								.replace(/\[filename\]/g, filename);
			} else {
				html = html + filename;
			}
			html = html + "</li>";
			var jHtml = $(html);
			jHtml.find('a.fileLink').click(function(e) {
				//alert("entra click");
			});
			jHtml.find('button.deleteFile').click(function(e) {
				var self = this;
				e.preventDefault();
/*				$.ajax({
					url : metapiTagResourcePath + "/" + $(self).attr('href'),
					type : "DELETE",
					success : function(response) {
						$(self).parent().hide(500, function() {
							$(self).parent().remove()
						});
					}
				});*/
			});
			$("#fileList").append(jHtml);
		}

		$(document).ready(function() {

			loadProjects();
			$("#projects").change(function() {
				cleanFields();
				projectId = $("#projects").val();
				if (projectId == "(new)") {
					$("#zipUploadForm").show();
				} else {
					loadFilesProject(projectId);
					$("#zipUploadForm").hide();
				}
			});

			templateEditor = CodeMirror($("#editor")[0], {
				value : "",
				mode : "dust",
				lineNumbers : true
			});
			templateEditor.setSize(700, 450);

			$("#saveButton").click(function(e) {
				var self = this;
				e.preventDefault();
				$.ajax({
					url : metapiTagResourcePath + "/" + currentFilename,
					type : "PUT",
					data : templateEditor.getValue(),
					success : function(response) {
						fileSaved(currentFilename, templateEditor.getValue());
					}
				});
			});

			$("#uploadZip").click(function(e) {
				var self = this;
				e.preventDefault();
				$("#zipFile").trigger("click");
			});

			$("#zipFile").change(function(e) {
				$("#zipUploadForm").submit()
			});

			$("#newFile").click(function(e) {
				e.preventDefault();
				var filename = prompt("Enter filename");
				if (!filename) {
					return;
				}
				if (filename[0] == '/') {
					filename = filename.substring(1);
				}
				fileSelected(filename, "");
				newFile = true;
			});

		});
	</script>

	<div class="container">
		<div id="areaR"></div>
		<div id="alerts"></div>

		<div class="row" id="area">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<select id="projects" class="form-control"></select>
				<button class="btn btn-danger" id="deleteButton">Delete</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<form id="zipUploadForm" action="../metapiTag/1/upload"
					method="POST" enctype="multipart/form-data">
					<input id="zipFile" type="file" name="file" style="display: none">
					<button id="uploadZip" class="btn">Upload zip file</button>
					<button id="newFile" class="btn">New file</button>
				</form>
			</div>
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<h2>
					<span id="filename"></span>
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<ul id="fileList">
				</ul>
			</div>
		</div>
	</div>

	<blockquote class="center">
		<p></p>
		<footer>
			LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La
			Plata, La Plata - Argentina. <cite title="Source Title">�
				MetApi Service</cite>
		</footer>
	</blockquote>

</body>
</html>
