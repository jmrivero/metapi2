<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Tag's Editor</title>
<style>
body {
	font: 62.5% "Trebuchet MS", sans-serif;
	margin: 50px;
}

.demoHeaders {
	margin-top: 2em;
}

#dialog-link {
	padding: .4em 1em .4em 20px;
	text-decoration: none;
	position: relative;
}

#dialog-link span.ui-icon {
	margin: 0 5px 0 0;
	position: absolute;
	left: .2em;
	top: 50%;
	margin-top: -8px;
}

#icons {
	margin: 0;
	padding: 0;
}

#icons li {
	margin: 2px;
	position: relative;
	padding: 4px 0;
	cursor: pointer;
	float: left;
	list-style: none;
}

#icons span.ui-icon {
	float: left;
	margin: 0 4px;
}

.fakewindowcontain .ui-widget-overlay {
	position: absolute;
}

select {
	width: 200px;
}
</style>

<link href="../css/metapiTag/jquery-ui.css" rel="stylesheet">
<script src="../lib/codemirror.js"></script>
<script src="../lib/javascript.js"></script>
<script src="../lib/jquery.min.js"></script>

<script src="../libs/class.js" type="text/javascript"></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript"></script>
<script src="../libs/noty/themes/default.js" type="text/javascript"></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript"></script>

<link rel="stylesheet" href="../css/metapiTag/style.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<style>
#alerts {
	/*height: 53px;*/
	
}
</style>

</head>
<body style="">
<jsp:include page="../parts/menuMetapiTag.jsp"></jsp:include>

</nav>
	<script src="../lib/bootbox.min.js"></script>
	<script type="text/javascript">
		var tagClientSide;
		var tagServerSide_script;
		var tags;
		var selectedTagId;
		var popupManager = new PopupManager();

		function tagSaved() {
			popupManager.showSuccess("Tag saved");
			loadTags();
		}

		function addTag(tag) {
			$("#tags").append(
					'<option value="[value]">[label]</option>'.replace(
							"[value]", tag.tag).replace("[label]", tag.tag));
			tags[tag.tag] = tag;
		}

		function cleanTagsList() {
			$("#tags option").remove();
			$("#tags").append('<option value="(new)">(new)</option>');
		}
		function validateTag(tag) {
			if(tag.tag!="" && tag.clientSide!="" && tag.clientCreation!="" && tag.pattern!="" && tag.tagComposite!="" && tag.propierties!=""){
				return true;
			}else
				return true;
		}
		function loadTags() {
			cleanTagsList();
			tags = {};
			$.ajax({
				url : "/../metapi/api/tags",
				type : "GET",
				dataType : "json",
				success : function(tags) {
					for (i in tags.tags) {
						if (tags.tags[i].serverSide != "") {
							tags.tags[i].serverSide = JSON
									.parse(tags.tags[i].serverSide);
						}
						tags.tags[i].tagginTool = JSON
								.parse(tags.tags[i].tagginTool);
						addTag(tags.tags[i]);
						if (selectedTagId) {
							$("#tags").val(selectedTagId);
						} else {
							$("#tags").val("(new)");
						}
					}
					;
				}
			});
		}

		function cleanFields() {
			//CLIENT SIDE
			$("#tag").val("");
			tagClientSide.setValue("");
			//SERVER SIDE
			$("#httpMethod").val("");
			$("#pattern").val("");
			tagServerSide_script.setValue("");
			//TAGGIN TOOL
			$("#label").val("");
			$("#composite").val("");
			$("#properties").val("propiedad1,propiedad2,propiedad3");
			selectedTagId = null;
		}

		$(document).ready(function() {
			$("div").removeAttr("style");
			tagClientSide = CodeMirror($("#clientSide")[0], {
				value : "",
				mode : "javascript",
				lineNumbers : true
			});
			tagClientSide.setSize(1000, 250);

			tagServerSide_script = CodeMirror($("#script")[0], {
				value : "",
				mode : "javascript",
				lineNumbers : true
			});
			tagServerSide_script.setSize(1000, 100);

			$("#saveButton").click(function(e) {
				var self = this;
				e.preventDefault();
				var tag = tags[$("#tags").val()];
				var propInput=$("#properties").val().replace(/\s/g,"");
				var prop=new Array;
				if (propInput!="")
					prop=propInput.split(",");
				var data = {
					tag : $("#tag").val(),
					clientSide : tagClientSide.getValue(),
					httpMethod : $("#httpMethod").val(),
					pattern : $("#pattern").val(),
					script : tagServerSide_script.getValue(),
					pattern : $("#pattern").val(),
					label : $("#label").val(),
					tagComposite : $("#composite").val(),
					properties : prop
				};
				if ($("#tag").val() != "" && validateTag(data)) {
					$.ajax({
						url : "/../metapi/api/tags",
						type : "POST",
						data : JSON.stringify(data),
						success : function(response) {
							tagSaved();
						}
					});
				}else{
					popupManager.showError("Incomplete Data");
				}
			});

			$("#deleteButton").click(function(e) {
				var tag = tags[$("#tags").val()];
				if (!tag) {
					return;
				}
				$.ajax({
					url : "/../metapi/api/tags",
					type : "DELETE",
					data : tag.tag,
					success : function(response) {
						popupManager.showSuccess("Tag deleted");
						cleanFields();
						loadTags();
					}
				});
			});

			$("#tags").change(function() {
				cleanFields();
				tag = tags[$("#tags").val()];
				if (tag) {
					//CLIENT SIDE
					$("#tag").val(tag.tag);
					tagClientSide.setValue(tag.clientSide);
					//SERVER SIDE
					if (tag.serverSide != "") {
						tagServerSide_script.setValue(tag.serverSide.script);
						$("#httpMethod").val(tag.serverSide.httpMethod);
						$("#pattern").val(tag.serverSide.pattern);
					}
					//TAGGINTOOL
					$("#label").val(tag.tagginTool.tagName);
					$("#composite").val(tag.tagginTool.tagComposite);
					$("#properties").val(tag.tagginTool.properties);
					selectedTagId = tag.tag;
				} else {
					cleanFields();
				}
			});

			loadTags();

			$("#resetAPIButton").click(function() {
				if (confirm("Are you sure?")) {
					$.ajax({
						url : "/../metapi/tags",
						type : "DELETE",
						success : function(response) {
							popupManager.showSuccess("API cleared");
							cleanFields();
							loadTags();
						}
					});
				}
			});

		});
	</script>


	<div class="container">
		<div id="alerts"></div>
		<div class="row" id="area">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<select id="tags" class="form-control"></select>
				<button class="btn btn-danger" id="deleteButton">Delete</button>
				<!--  <button class="btn btn-info" id="resetAPIButton">Copy Tag</button>-->
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<input type="text" class="form-control" placeholder="Name tag"
					id="tag"></input>
				<button class="btn btn-primary" id="saveButton">Save</button>
			</div>
		</div>
		<div id="accordion">
			<h3>Client Side</h3>
			<div id="divClientSide">
				<div id="clientSide"></div>
			</div>
			<h3>Server Side</h3>
			<div>
				<table>
					<tbody>
						<tr>
							<td>Http Method:</td>
							<td><select class="form-control" id="httpMethod"
								style="width: 15%;">
									<option>GET</option>
									<option>POST</option>
									<option>PUT</option>
									<option>DELETE</option>
							</select></td>
						</tr>
						<tr>
							<td>Pattern:</td>
							<td style="width: 90%;">
								<div>
									<input type="text" class="form-control" placeholder="Pattern"
										id="pattern"></input>
								</div>


							</td>
						</tr>
					</tbody>

				</table>

				Script:
				<div id="script"></div>
			</div>
			<h3>Taggin Tool</h3>
			<div>
				<table>
					<tbody>
						<tr>
							<td>Tag Label:</td>
							<td>
								<div>
									<input type="text" class="form-control" placeholder="Tag Label"
										id="label"></input>
								</div>
							</td>
						</tr>
						<tr>
							<td>Composite:</td>
							<td >
								<div>
									<select class="form-control" id="composite" style="width: 15%;">
										<option>true</option>
										<option>false</option>
									</select>
								</div>


							</td>
						</tr>
						<tr>
							<td>Propierties:</td>
							<td style="width: 90%;">
								<div>
									<input type="text" class="form-control"
										placeholder="Propiedad,propiedad2,propiedad3" id="properties"></input>

								</div>


							</td>
						</tr>
					</tbody>

				</table>


			</div>
		</div>

		<script src="../libs/jquery-ui.js"></script>
		<script>
			$("#accordion").accordion();
		</script>

	</div>
	<blockquote class="center">
		<p></p>
		<footer>
			LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La
			Plata, La Plata - Argentina. <cite title="Source Title">�
				MetApi Service</cite>
		</footer>
	</blockquote>
</body>
</html>