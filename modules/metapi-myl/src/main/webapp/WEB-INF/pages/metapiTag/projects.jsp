<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Projects</title>

<script src="../lib/codemirror.js"></script>
<style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/bootstrap.min.js"></script>
<script src="../lib/alert.js"></script>

<script src="../libs/class.js" type="text/javascript"></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript"></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript"></script>
<script src="../libs/noty/themes/default.js" type="text/javascript"></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript"></script>

<link rel="stylesheet" href="../css/metapiTag/style.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">


<style>
#alerts {
	/*height: 63px;*/
	
}

#fileListP {
	list-style: none;
	margin: 0px;
	padding: 0px;
	height: 437px;
	overflow-y: scroll;
}
</style>
</head>
<body style="">
<jsp:include page="../parts/menuMetapiTag.jsp"></jsp:include>

</nav>


	<script type="text/javascript">
		var projects;
		var projectId;
		var templateEditor;
		var currentFilename = null;
		var newFile = false;
		var applicationId = 2;
		var popupManager = new PopupManager();

		var extensionModeMappings = {
			js : "javascript",
			xml : "xml",
			html : "htmlmixed",
			htm : "htmlmixed",
			css : "css",
			tl : "dust"
		}

		var metapiTagResourcePath = "/../../metapi/metapiTag/1/";
		//"/../metapi/resources/1/metapiTags/"

		function loadProjects() {
			cleanProjectList();
			projects = {};
			$.ajax({
				url : "/../../metapi/metapiTag/1/projects",
				type : "GET",
				dataType : "json",
				success : function(projects) {
					for (i in projects.files) {
						addProject(projects.files[i]);
						if (projectId) {
							$("#projects").val(projectId);
						} else {
							$("#projects").val("(new)");
						}
					}
					;
				}
			});
		}

		function loadFilesProject(project) {
			$.ajax({
				url : metapiTagResourcePath + project,
				type : "GET",
				dataType : 'json',
				success : function(response) {
					$.each(response.files, function(_, filename) {
						addFileToList(filename);
					});
				}
			});
		}

		function addProject(project) {
			$("#projects").append(
					'<option value="[value]">[label]</option>'.replace(
							"[value]", project).replace("[label]", project));
			//tags[tag.tag] = tag;
		}
		function cleanProjectList() {
			$("#projects option").remove();
			$("#projects").append('<option value="(new)">(new)</option>');
		}

		function extractExtension(filename) {
			var parts = filename.split(".");
			return parts[parts.length - 1];
		}

		function updateEditorMode(filename) {
			var extension = extractExtension(filename);
			if (extensionModeMappings[extension]) {
				templateEditor.setOption("mode",
						extensionModeMappings[extension]);
			}
		}

		function fileSelected(filename, content) {
			templateEditor.setValue(content);
			updateEditorMode(filename);
			$("#filename").text(filename);
			currentFilename = filename;
			$("#editorContainer").fadeIn(500);
			newFile = false;
		}

		function closed() {
			$("#not").remove();
		}

		function fileSaved(filename, content) {
			popupManager.showSuccess("Resource saved");
			if (newFile) {
				addFileToList(filename);
				newFile = false;
			}
		}
		function cleanFields() {
			$("#fileListP li").remove();
		}
		function addFileToList(filename) {
			filename = filename[0] == '/' ? filename.substring(1) : filename;
			filename=filename.replace("metapiTag/projects/","");
			filename=filename.substring(projectId.length+1,filename.length);

			var extension = extractExtension(filename);
			if (extensionModeMappings[extension]) {
				
		        var html = "<li  class=\"list-group-item\"><span class=\"pink\"><a href=\"\">[filename]</a></span><a class=\"icon icon-play\" title=\"Run\"  href=\"/../../metapi/api/tagginRun/"+ projectId +"/[filename]?project="+projectId+ "\"></a><a class=\"icon icon-tag\" title=\"Taggin\" href=\"/../../metapi/api/tagginTool/"+ projectId +"/[filename]?project="+projectId+ "\"></a>";
		        html=html.replace(/\[filename\]/g, filename);
			} else {
			}			
			var jHtml = $(html);
			jHtml.find('a.fileLink').click(function(e) {
			});
			jHtml.find('button.deleteFile').click(function(e) {
				var self = this;
				e.preventDefault();
			});
			$("#fileListP").append(jHtml);
		}

		$(document).ready(function() {
			var param= new RegExp('[\\?&]project=([^&#]*)').exec(window.location.href);
		    if (param != null){
		        projectId =param[1] || 0;
		    	$("#projects").val(projectId);
				loadFilesProject(projectId);
				$("#zipUploadForm").hide();

		    }
		    else
		    	projectoId ="";
			
			loadProjects();
			$("#projects").change(function() {
				cleanFields();
				projectId = $("#projects").val();
				if (projectId == "(new)") {
					$("#zipUploadForm").show();
				} else {
					loadFilesProject(projectId);
					$("#zipUploadForm").hide();
				}
			});
			

			$("#saveButton").click(function(e) {
				var self = this;
				e.preventDefault();
				$.ajax({
					url : metapiTagResourcePath + "/" + currentFilename,
					type : "PUT",
					data : templateEditor.getValue(),
					success : function(response) {
						fileSaved(currentFilename, templateEditor.getValue());
					}
				});
			});

			$("#uploadZip").click(function(e) {
				var self = this;
				e.preventDefault();
				$("#zipFile").trigger("click");
			});

			$("#zipFile").change(function(e) {
				$("#zipUploadForm").submit()
			});

			$("#newFile").click(function(e) {
				e.preventDefault();
				var filename = prompt("Enter filename");
				if (!filename) {
					return;
				}
				if (filename[0] == '/') {
					filename = filename.substring(1);
				}
				fileSelected(filename, "");
				newFile = true;
			});

		});
	</script>

	<div class="container">
		<div id="areaR"></div>
		<div id="alerts"></div>

		<div class="row" id="area">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<select id="projects" class="form-control"></select>
				<button class="btn btn-danger" id="deleteButton">Delete</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<form id="zipUploadForm" action="../metapiTag/1/upload"
					method="POST" enctype="multipart/form-data">
					<input id="zipFile" type="file" name="file" style="display: none">
					<button id="uploadZip" class="btn">Upload zip file</button>
					<button id="newFile" class="btn">New file</button>
				</form>
			</div>
		</div>
    <h4>List of files</h4>
    <hr>
  
		<ul id="fileListP" class="list-group">
		
		</ul>
		

		
  </div>


	<blockquote class="center">
		<p></p>
		<footer>
			LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La
			Plata, La Plata - Argentina. <cite title="Source Title">�
				MetApi Service</cite>
		</footer>
	</blockquote>

</body>
</html>
