<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Factory {{ MYL }}</title>
<script src="../lib/codemirror.js"></script><style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../libs/class.js" type="text/javascript" ></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js" type="text/javascript"></script>
<link href="../lib/dist/skin-themeroller/ui.fancytree.min.css" rel="stylesheet" type="text/css">
<script src="../lib/dist/jquery.fancytree.min.js" type="text/javascript"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="../libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript" ></script>	
<script src="http://codemirror.net/2/lib/util/formatting.js"></script>
<script type="text/javascript" src="../scripts/myl-script/editorMYL.js"></script>
<link rel="stylesheet" href="../css/MYLStyle.css">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
</head>
<body>
  <jsp:include page="parts/menuMYL.jsp"></jsp:include>
  <div class="container">
  	<h4 class="titleMYL"><strong>Factory</strong> 
  	<button title="Save changes in file" id="saveButton" class="btn btn-success btn-small"><i class="icon-save"></i></button>
  	<button class="btn btn-primary btn-mini" title="Format code" onclick="autoFormat();"><i class="icon-code"></i> Format</button>
  	<button id="recipe" title="Write rules for your model" class="btn btn-primary disabled" data-toggle="modal" data-target=".bs-recipe-modal-lg"><i class="icon-file-alt"></i> Rules</button>
	<button id="process" title="Write processor code in your model before generate code" class="btn btn-primary disabled" data-toggle="modal" data-target=".bs-process-modal-lg"><i class="icon-cog"></i> Processor</button>
	<button id="inferButton" title="infer sample model" class="btn btn-inverse btn-small"><i class="icon-magic"></i> Infer model</button><span id="filename">No files selected.</span>
  	<button id="clickRender" title="Open render factory" class="btn btn-inverse btn-small"><i class="icon-cogs"></i> Generate Render</button>
	</h4>
		  <div class="bs-example">  
		    <!-- Modal HTML -->
		    <div id="myModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h3 class="modal-title"><b>Render Factory</b><img id="icon-render-m" src="../images/merge.png" /></h3>
		                </div>
		                <div class="modal-body">
		                    
		      
		                    
		                    
		                    Welcome to Render Factory of <i>MYL Project</i>, let's go to generate new render...
		                    <p class="text-info"><small>Please make the following steps. You must be select one file (<b class="modal-title">1�</b> step) and model (<b class="modal-title">2�</b> step) and then click <font color="#0BD318">Generate</font> to generate the new render.</small></p>           
		                    <label><b class="modal-title">1�</b> Select your file:</label><span id ="fileSelect"></span>
		                    	
		                    <br>	
						
							<label><b class="modal-title">2�</b> Select your model:</label>
							<p><select id="modelSelect" class="form-control" data-style="btn-primary"></select> </p>
		                	
		                	<div id="suma">
			                	<label id="fileRender"></label>
								<img id="icon-render-img" src="../images/add.jpg" />
								<label id="fileModel"></label>
		                	</div>
		                </div>
		                <div class="modal-footer">
		                    <div id="use">
		                    <label title="Check this if you want to use the processing code.">
     							 <input  id="useProcess" type="checkbox"> Use processor code.
   							 </label>
   							 </div>
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button  id="runRender"  class="btn btn-success">Generate <i class="icon-play"></i></button>
		                    
		                </div>
		            </div>
		        </div>
		    </div>
		    <div id="oModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Render Factory</h4>
		                </div>
		                <div class="modal-body">
		                    <p>Congratulations! rendering the file has been successfully. <img id="icon-render-ok" src="../images/ok.png" /></p>
		                    <div id="msj">
		                    </div>
		               
		                </div>
		                <div class="modal-footer">
		                    <button id="reload" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>		
		 <div class="bs-example">  
		    <!-- Modal HTML -->
		    <div id="myModalAdd" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4  class="modal-title"> Add new file into the project</h4>
		                </div>
		                <div class="modal-body">
		                	<p  class="text-primary"><i>Please, write file name with extension, and then click add.</i></p>
		                	<p id="titleAdd" class="text-danger"><small></small></p>	
		                	<input id="textoAdd" class="form-control" placeholder="Name of file" type="text"/>
		                	<hr>
		                	<p style="display:inline" class="text-primary">Select destination for the new file:
							<select style="display:inline" id="selectAdd" class="form-control">				   
							    <option value="Files">Files</option>
								<option value="Models">Models</option>
							  </select></p>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button type="button" id="runAdd" class="btn btn-success">Add</button>
		                </div>
		            </div>
		        </div>
		    </div>
		    
		    
		    <div id="myModal1" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Editing File Name</h4>
		                </div>
		                <div class="modal-body">
		                	<p class="text-primary"><i>Editing the file name, then click OK for save changes.</i></p>
		                	<input id="texto" class="form-control" placeholder="Text input" type="text"/>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button type="button" id="runEdit" class="btn btn-success">Ok</button>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div id="oModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Render Factory</h4>
		                </div>
		                <div class="modal-body">
		                    <p>Congratulations! rendering the file has been successfully. <img class="icon-render-ok" src="../images/ok.png" /></p>
		                    <div id="msj">
		                    </div>
		               
		                </div>
		                <div class="modal-footer">
		                    <button id="reload" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		
		<div id="recipe-modal" class="modal fade bs-recipe-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div class="modal-header">
		            <!-- button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button-->
		            <h4 class="modal-title">Write your validation rule<img id="icon-recipe-m" src="../images/recipe.png" /></h4>
	            </div>
	            <div class="modal-body">
	                <p class="text-info">Note: write your rules, and then click on check rule.</p>
		            <div class="container-fluid">
                    	<div class="row">
                        	<div class="col-lg-5">
		            	 
			            	 <b id="recipe-model-name">Model.json</b>
			            	 <textarea   class="form-control" id="left-textarea" rows="10" ></textarea>
			           		</div>
			           		
			           		<div class="col-lg-7">
					             
					             <code>Write JavaScript code:</code>
					             <div id="editorModal"></div>
					             <!-- <textarea  class="form-control" id="rigth-textarea" rows="10"></textarea>	 -->	 
			         		</div>
			         	</div>
			         </div>		
			            
			            <div class="col-sm-12">
			            	
					         <label>Console:</label>
					         <textarea id="recipeConsole" class="textareawidth form-control" id="message-textarea" rows="3" cols="200" readonly="readonly"></textarea>
					    </div>
			            
			          			
	            </div>
	            <div class="modal-footer">
	            	
	                <!-- button id="buttonSaveRecipe" title="Save changes in recipe" class="btn btn-warning btn-mini"><i class="icon-save"></i> Save</button-->
	               <button id="saveRecipe" title="Save Rule" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span></button>
	               <button id="closeRecipeModal" type="button" class="btn btn-small" data-dismiss="modal">Close</button>
	               <button id="checkRecipeButton" class="btn btn-success btn-small">Check Rule <i class="icon-refresh"></i></button>
	        	</div>
		    </div>
		  </div>
		</div>
		
		<div id="process-modal" class="modal fade bs-process-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">
			    <div id="headModalP" class="modal-header">
		            <!-- button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button-->
		            <h4><strong>Processor</strong> <small>Write your processor code.</small></h4>
	            </div>
	            <div id="bodyModalP" class="modal-body">
	                <p class="text-info">Note: write your processor code, this code will be processed before rendering</p>
		            <div class="container-fluid">
                    	<div class="row">
                        	<div class="col-lg-5">
		            	 
			            	 <b id="process-model-name">Model.json</b>
			            	 <textarea   class="form-control" id="left-textareaP" rows="10" ></textarea>
			           		</div>
			           		
			           		<div class="col-lg-7">
					             
					             <code>Write JavaScript code:</code>
					             <div id="editorModalProcess"></div>
					             <!-- <textarea  class="form-control" id="rigth-textarea" rows="10"></textarea>	 -->	 
			         		</div>
			         	</div>
			         </div>		
			            <!-- 
			            <div class="col-sm-12">
			            	
					         <label>Console:</label>
					         <textarea id="processConsole" class="textareawidth form-control" id="message-textarea" rows="3" cols="200" readonly="readonly"></textarea>
					    </div>
			             -->
			          			
	            </div>
	            <div id="footModalP" class="modal-footer">
	            	
	                <!-- button id="buttonSaveRecipe" title="Save changes in recipe" class="btn btn-warning btn-mini"><i class="icon-save"></i> Save</button-->
	               <button id="saveProcess" title="Save processor code" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-floppy-disk"></span></button>
	               <button id="closeProcessModal" type="button" class="btn btn-danger btn-small" data-dismiss="modal">Close</button>
	               <!-- <button id="checkProcessButton" class="btn btn-success btn-small">Check<i class="icon-refresh"></i></button> -->
	        	</div>
		    </div>
		  </div>
		</div>
		
		
	<hr>	
    <div class="row"> 
      <div id="div-list" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
         <button id="controlEdit" title="Edit selected file" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>
         <button onclick="delFileFT();" id="control" title="Delete selected file." class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
         <button id="controlAdd" title="Create new file" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-file"></span></button>
      	<h5><span id="filenameTitle"></span></h5>
		<!-- Define where the tree should appear -->
  		<div id="fileList">
  		<div id="tree">
  		</div>
		</div>
		<hr>
		<h5><span id="modelsTitle"></span></h5>
		<ul id="fileListModel" class="list-group">
		</ul>
      </div>  
      <div  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
          <div id="editorContainer">
            <div id="editor">
			</div>
          </div>
        </div>
      </div>     
    </div> 	
  </div> 
<blockquote class="center">
	  <footer id="footer">
	  
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MYL Project</cite>
	  </footer>
  </blockquote>

</body></html>
