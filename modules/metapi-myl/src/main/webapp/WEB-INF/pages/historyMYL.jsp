<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>History MYL</title>

<script src="../lib/codemirror.js"></script><style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/bootstrap.min.js"></script>
<script src="../lib/alert.js"></script>
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<script src="../libs/class.js" type="text/javascript" ></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="../libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript" ></script>		
<link rel="stylesheet" href="../css/MYLStyle.css">
<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="../css/bootstrap.min.css"> -->
</head>
<body style="">
  
<jsp:include page="parts/menuMYL.jsp"></jsp:include>

<script type="text/javascript">
	var projectsMYL = "../../metapi/MYL/1/projectsMYL/"
	var delPath = "../../metapi/MYL/1/del/"
	var fileSystemPath = "../../metapi/MYL/1/"
	var href = jQuery(location).attr('href').split("/");
	var name = href[5];
	var templateEditor;
	var currentFilename = null;
	var newFile = false;
	var applicationId = 2;
	var popupManager = new PopupManager();
 
	
    var extensionModeMappings = {
        	js: "javascript",
        	xml: "xml",
        	html: "htmlmixed",
        	htm: "htmlmixed",
        	css: "css",
        	java: "java",
        	jsp: "htmlmixed",
        	tl: "dust"
        }
	
    function extractExtension(filename) {
    	var parts = filename.split(".");
      return parts[parts.length - 1];
    }
    
    function updateEditorMode(filename) {
    	var extension = extractExtension(filename);
    	if (extensionModeMappings[extension]) {
    		templateEditor.setOption("mode", extensionModeMappings[extension]);
    	}
    	
    }
    
    function download(){
    	if($('#filenameHis').text() == "No files selected."){
    		popupManager.showError("You must select a file to download it.");
    	}
    	else{	    	
    		var url = projectsMYL + name + "/" + $('#filenameHis').text()+ "/dwl" ;
    		window.open(url, '_blank');
    	}
    }
    
    function htmlEntities(str) {
    	return String(str).replace('&amp;',/&/g).replace( '&lt;', /</g).replace('&gt;', />/g).replace('&quot;', "/\"/g");
    }
    
    function fileSelected(filename, content) {
    	$("#download").attr('href',"../../files/1/files/MYL/projects/"+ name  +"/"+ filename + "\"");
        //var n = htmlEntries(content);
    	templateEditor.setValue(content);
        updateEditorMode(filename);
        $("#filenameHis").text(filename);
        currentFilename = filename;
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
	
    function fileSelectedProject(filename) {
    	$("#download").attr('href',"../../files/1/files/MYL/projects/"+ name + "\"");
    	$("#filenameHis").text(filename);
    	templateEditor.setValue("/* You have selected the folder " + filename + " with all its files.*/");
    	templateEditor.setOption("readOnly", true);
    	updateEditorMode(filename);
		currentFilename = "/" +filename;
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
    
    function deleted() {
    	popupManager.showSuccess("Your model was successfully deleted.");
    }
    
    function deleteFile(param1){
    	var x=param1.replace("#", "%23");
    	$.ajax({url: delPath + name + "/" + x, type: "DELETE", success: function(response) {
    		$('#fileListH').empty();
    		addFileToList(name);  
    		$.ajax({url: projectsMYL + name , type: "GET", dataType: 'json', success: function(response) {
	            $.each(response.files, function(_, filename) {
	           	 addFileToList(filename);
	           });
	        }}); 	  
         }});
    	deleted();
    }
    
    function addFileToList(filename) {
        var html = "<li class=\"list-group-item-e\">";
        filename = filename[0] == '/' ? filename.substring(1) : filename;
        var extension = extractExtension(filename);
        if (extensionModeMappings[extension]) {
        	html = html + "<input title=\"Delete\" type=\"image\"  id=\"icon-file\" src=\"../images/remove.png\" onclick=\"deleteFile('"+filename+"');\"/><img id=\"icon-file\" src=\"../images/file.png\" /><a href=\"file/[filename]\" class=\"fileLink\" title=\"[filename]\"><p class=\"ellipsis\">[filename]</p></a>".replace(/\[filename\]/g, filename);
        } else {
        	 html = html + '<img id="icon-file" src="../images/folder.png" /><a href="#" class="project">[filename]</a>'.replace(/\[filename\]/g, filename);
        }
        html = html + "</li>"; 
        var jHtml = $(html);
        jHtml.find('a.fileLink').click(function(e) {
            var self = this;
            e.preventDefault();   
            var gg= filename.replace("#", "%23");
            $.ajax({url: fileSystemPath + name + "/" + gg , type: "GET", success: function(response) {
              fileSelected(filename, response)
             }});
        });
        jHtml.find('a.project').click(function(e) {
            var self = this;
            e.preventDefault();   
            fileSelectedProject(filename);
        });
        $("#fileListH").append(jHtml);
    }
	
	
	 $(document).ready(function() {
	    	//$("#filenameTitle").text("Files in the project " + name + ":");
	    	$("#menu").append("<li><a id=\"versions\" class=\"icon icon-building\" title=\"Go to factory of  "+name+ "\"  href=\"../editorMYL/"+ name.replace("_MYL", "") + "\">Factory</a></li>");
	    	  templateEditor = CodeMirror($("#editor")[0], {
	    		  value: "> Select file",
	              readOnly: true,
	              mode:  "javascript",
	              lineNumbers: true,
	              theme: 'default', // the theme can be set here
	              lineWrapping: true,
	              styleActiveLine: true,
	              matchBrackets: true,
	              indentUnit: 4
	          });
	    	  templateEditor.setSize(980, 460);
	          
	        //$("#hisButton").append("<a id=\"download\" href=\"#\" rel=\"nofollow\">Download Here</a>");
	          
	        $("#download").click(function(){
	      		location.reload();
	    	});
	        
	          addFileToList(name);  
	        $.ajax({url: projectsMYL + name , type: "GET", dataType: 'json', success: function(response) {
	            $.each(response.files, function(_, filename) {
	           	 addFileToList(filename);
	           });
	        }});
	 });


</script>

  <div class="container">
	 <h4 class="titleMYL"><strong>History</strong>

    <span id="filenameHis">No files selected.</span><button title="Download file selected." onclick="download();" id="his" class="btn btn-danger">Download <span class="glyphicon glyphicon-save"></span></button>
	</h4>
	<br>

    
    <div class="row">
     <div id="div-list" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      	<h5><span id="filenameTitle"></span></h5>
      	
        <ul id="fileListH" class="list-group">
		</ul>
      </div>
      
      <div  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
          <div id="editorContainer">
            <div id="editor">
			</div>
          </div>
        </div>
      </div>
    </div>

    	
  </div>

  <blockquote class="center">
	  <p></p>
	  <footer>
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MetApi Service</cite>
	  </footer>
  </blockquote>

</body></html>
