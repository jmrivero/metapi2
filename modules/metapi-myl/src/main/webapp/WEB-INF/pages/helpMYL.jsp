<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Make Your Language - Help</title>

<script src="lib/codemirror.js"></script><style type="text/css"></style>
<script src="lib/javascript.js"></script>
<script src="lib/dust.js"></script>
<script src="lib/xml.js"></script>
<script src="lib/css.js"></script>
<script src="lib/htmlmixed.js"></script>
<script src="lib/jquery.min.js"></script>
<script src="lib/bootstrap.min.js"></script>
<script src="lib/alert.js"></script>

<script src="libs/class.js" type="text/javascript" ></script>
<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<link rel="stylesheet" href="css/MYLStyle.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

</head>
<body style="">
  
<nav class="navbar navbar-default" role="navigation">
 <a class="navbar-brand" href="#">Make Your Language Project</a><img id="icon-menu" src="../images/factory.png"/>
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul id="menu" class="nav navbar-nav">
			 <li> <a href="projectsMYL">Home</a></li>
			 <li> <a href="helpMYL.html">Help</a></li>
		</ul>
		
	</div>
</nav>
<script type="text/javascript">     
    $(document).ready(function() {
    	 
    
        
    });   
</script>
 <div class="container">
    <h4 class="titleMYL">Help<span id="filename"> <i>Welcome to help of MYL tool.</i></span></h4> 
     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
     

      </div>
    
	
    <br>
    <h4>�Do you need help?</h4>
    <hr>
    
    <p>Make Your Language is a simple project for autogenerating source code from simple JSON's models.</p>
  
  <h5>Demo MYL</h5>
  <br>
  
	<iframe width="820" height="415" src="//www.youtube.com/embed/8CguBLcwVE0?list=UUUHAOBHxkHiwj3IzDscdKIA" frameborder="0" allowfullscreen></iframe>
  </div>
 
  <blockquote class="center">
	 
	  <footer id="footer">
	  
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MYL Project</cite>
	  </footer>
  </blockquote>

</body></html>
