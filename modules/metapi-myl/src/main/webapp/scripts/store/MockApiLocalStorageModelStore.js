MockApiLocalStorageModelStore = Class.extend({

	init: function(mockapiSerializer) {
		this.mockapiSerializer  = mockapiSerializer;
	},
	
	storeModel: function(modelId, model, callback) {
		localStorage["__mockapi_" + modelId] = this.mockapiSerializer.serialize(model);
		callback(null);
	},
	
	loadModel: function(modelId, callback) {
		if (!localStorage["__mockapi_" + modelId]) {
			callback(null);
		}
		callback(this.mockapiSerializer.deserializeModel(localStorage["__mockapi_" + modelId]));
	}

});