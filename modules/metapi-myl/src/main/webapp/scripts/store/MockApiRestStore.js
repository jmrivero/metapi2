MockApiRestStore = Class.extend({

	init: function(baseUrl, mockapiSerializer) {
		this.mockapiSerializer  = mockapiSerializer;
		this.baseUrl = baseUrl;
	},
	
	storeModel: function(modelId, model, callback) {
		var self = this;
		$.ajax({
			url: this.baseUrl + modelId,
			type: "PUT",
			data: self.mockapiSerializer.serialize(model),
			success: function() {
				if (callback) {
					callback(null);
				}
			}
		});
	},
	
	loadModel: function(modelId, callback) {
		var self = this;
		$.ajax({
			url: this.baseUrl + modelId,
			type: "GET",
			success: function(modelRepresentation) {
				debugger;
				callback(self.mockapiSerializer.deserializeModel(modelRepresentation));
			},
			error: function(modelRepresentation) {
				callback(null);
			}
		});
	}

});