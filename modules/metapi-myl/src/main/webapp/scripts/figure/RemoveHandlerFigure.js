RemoveHandlerFigure = Figure.extend({ 
	
	init: function(canvas, figure, position) {
		this.canvas = canvas;
		this.figure = figure;
		this.focused = false;
		this.position = position;
		this.initialize(canvas);
		this.redraw();
	},
	
	initialize: function(canvas) {
		var self = this;
		//this.root = $('<div class="closeButton"  style="display: none">X</div>');
		this.root = $('<img src="images/delete.png" class="closeButton" style="display: none"/>');
		this.setRootElement(this.root);
		$(canvas).append(this.root);
		this.root.click(function() {
			self.remove();
			self.figure.remove();
		});
		this.figure.addEventListener("move", function() {
			self.redraw();
		});
		this.figure.addEventListener("resize", function() {
			self.redraw();
		});
		this.figure.addEventListener("remove", function() {
			self.remove();
		});
		this.figure.addEventListener("mouseEnter", function() {
			self.redraw();
			self.root.show(0);
		});
		this.figure.addEventListener("mouseLeave", function() {
			self.redraw();
			setTimeout(function() {
				if (self.focused) {
					self.focused = false;
					return;
				}
				self.root.hide(0);
			}, 50);
		});
		this.root.mouseenter(function() {
			self.focused = true;
		});
		this.root.mouseleave(function() {
			self.focused = false;
			self.root.hide(0);
		});
		this.redraw();
	},
	
	redraw: function() {
		var boundaries = this.figure.getBoundaries();
		if (this.position == RemoveHandlerFigure.positions.TOP_RIGHT) {
			this.root.css({
				left: boundaries.left + boundaries.width - this.root.width() / 2,
				top: boundaries.top - this.root.height() / 2
			});		
		} else if (RemoveHandlerFigure.positions.CENTER) {
			this.root.css({
				left: boundaries.left + (boundaries.width / 2),
				top: boundaries.top + (boundaries.height / 2)
			});
		}
	}
	
});

RemoveHandlerFigure.positions = {TOP_RIGHT: 0, CENTER: 1};