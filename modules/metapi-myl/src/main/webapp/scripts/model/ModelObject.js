var ModelObject = Class.extend({

	init: function() {
		this.id = null;
		this.representation = null;
		this.eventListeners = {};
		this.registerEvent('change');
	},
	
	getId: function() {
		return this.id;
	},
	
	setId: function(id) {
		this.id = id;
		this.notifyEvent('change', this);
	},
	
	setRepresentation: function(representation) {
		this.representation = representation;
	},
	
	getRepresentation: function(representation) {
		return this.representation;
	},

	registerEvent: function(eventName) {
		this.eventListeners[eventName] = [];
	},
	
	addEventListener: function(eventName, f) {
		if (!this.eventListeners[eventName]) {
			throw ("Event " + eventName + " does not exist");
		}
		this.eventListeners[eventName].push(f);
	},
	
	removeEventListener: function(eventName, f) {
		if (!this.eventListeners[eventName]) {
			throw ("Event " + eventName + " does not exist");
		}
		var index = this.eventListeners.indexOf(f);
		if (index != -1) {
			this.eventListeners.splice(index, 1);
		}
	},
	
	notifyEvent: function(eventName, eventData) {
		for (i in this.eventListeners[eventName]) {
			this.eventListeners[eventName][i](eventData);
		}
	}

});