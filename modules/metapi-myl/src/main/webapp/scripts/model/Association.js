var Association = ModelObject.extend({

	init: function(feature1, feature2, mockup) {
		this._super();
		this.setFeature1(feature1);
		this.setFeature2(feature2);
		this.mockup = mockup;
	},
	
	getFeature1: function() {
		return this.feature1;
	},
	
	getFeature2: function() {
		return this.feature2;
	},

	setFeature1: function(feature1) {
		if (this.feature1 && this.feature1.hasAssociation(this)) {
			this.feature1.removeAssociation(this);
		}
		this.feature1 = feature1;
		if (feature1) {
			feature1.addAssociation(this);
		}
	},
	
	setFeature2: function(feature2) {
		if (this.feature2 && this.feature2.hasAssociation(this)) {
			this.feature2.removeAssociation(this);
		}
		this.feature2 = feature2;
		if (feature2) {
			feature2.addAssociation(this);
		}
	},
	
	getMockup: function() {
		return this.mockup;
	},
	
	dissociateFeature: function(feature) {
		if (this.feature1 == feature) {
			this.setFeature1(null);
		} else if (this.feature2 == feature) {
			this.setFeature2(null);
		}
	}

});