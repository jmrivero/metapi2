var Feature = ModelObject.extend({

	init: function(content, id) {
		this._super();
		this.associations = [];
		this.content = null;
		this.id = id;
		if (content) {
			this.setContent(content);
		}
	},

	setContent: function(content) {
		this.content = content;
	},
	
	getContent: function() {
		return this.content;
	},
	
	addAssociation: function(association) {
		this.associations.push(association);
	},
	
	removeAssociation: function(association) {
		var index = this.associations.indexOf(association);
		if (index != -1) {
			this.associations.splice(index, 1);
			association.dissociateFeature(this);
		}
	},
	
	hasAssociation: function(association) {
		return this.associations.indexOf(association) != -1;
	},
	
	disociateAll: function() {
		for (i in this.associations) {
			this.removeAssociation(this.associations[i]);
		}
	}
	
});

Feature.newFromData = function(data) {
	var feature = new Feature();
	feature.setContent(data);
	return feature;
}