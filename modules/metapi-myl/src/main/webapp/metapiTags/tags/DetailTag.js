var DetailTag = MetapiTag
		.extend({

			init : function(detalle, disparador, selector, tagHijos	) {
				this._super();
				this.detalle = detalle
				this.disparador = disparador;
				this.selector = selector;
				this.tagHijos = tagHijos;
			},


			run : function(element, objeto) {
				var self = this;
				element.find(self.selector)[self.disparador](function() {
					for (j in self.tagHijos) {
						self.tagHijos[j].run($(self.detalle), objeto);
					}
				})
			}
		});