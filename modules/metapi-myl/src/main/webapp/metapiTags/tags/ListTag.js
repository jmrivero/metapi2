var ListTag = MetapiTag.extend({

	init: function(padre, cloneElement, url, tagHijos) {
		this._super();
		this.padre = padre;
		this.url = url;
		this.tagHijos = tagHijos;
		this.cloneElement=cloneElement;
		this.domElement=null;
	},
	
	run: function(context) {
		var self = this;
		$.get(this.getUrlPrefix() + this.url, function(elements) {
			elements = JSON.parse(elements);
			self.domElement = $(self.padre).find(self.cloneElement);
			for (i in elements) {
				var newElement= self.domElement.clone();
				for (j in self.tagHijos){
					self.tagHijos[j].run(newElement,elements[i]);
					$(self.padre).append(newElement);
				}	
			}
		});
	}
	
});