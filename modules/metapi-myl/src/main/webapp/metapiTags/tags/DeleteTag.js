var DeleteTag = MetapiTag.extend({

	init: function(url,selector,disparador) {
		this._super();
		this.disparador = "click";
		this.selector = selector;
		this.url = url;
		this.domElement=null;
	},
	
	run: function(element, objeto) {
		self=this;
		element[self.disparador](
				function (){
					$.ajax({url: self.getUrlPrefix() + self.url+"/"+objeto.id, type: "DELETE", 
						success: 
						function() {
							console.log("ok");
						},
						error:
						function() {
							console.log("error");
						}	
					})
				}
		);		
	}
});