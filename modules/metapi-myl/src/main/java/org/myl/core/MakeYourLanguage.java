package org.myl.core;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.metapi.templating.mustache.MustacheTemplate;
import org.myl.resources.FilesystemMYLService;
import org.myl.utils.ErrorMYL;


/**
* The MYL Project
*
* @author  Alan Garcia Camiña
* @version 1.0
* @since   2014-06-16 
*/
public class MakeYourLanguage {

	private static final String MYL_MSG = "//MYL";
	private FilesystemMYLService mylService = new FilesystemMYLService();
	@SuppressWarnings("unused")
	private String name;
	private static final String FS_PATH = "./files/1/files/MYL/projects/";
	
	
	public MakeYourLanguage(){

	}
	
	
	//provisorio
	public void inferSampleModelInFolderRest(String route, String nameDir) throws JSONException, IOException{
		JSONObject result = new JSONObject();
		JSONObject main = new JSONObject();
		File dir = new File(FS_PATH + route);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				//obtengo el file dentro de la carpeta de entrada
				InputStream  emplate = this.mylService.getContentsOfResourceInFolder(1, nameDir, child.getName());
				//obtengo los JSON de cada file
				result = inferSampleModelFromString(IOUtils.toString(emplate));
				main.put(child.getName(), result);
			}
			InputStream is1 = new ByteArrayInputStream( main.toString().getBytes() );
			String targetFolder = nameDir + "_Models";	
			mylService.storeFileInFolder(1, targetFolder , nameDir + "_model.json", is1);		
		} else {
			System.out.println("The directory not contains files");
		}
	}
	
	
	public String renderTemplatesInFolderRest(String nameDir, String jsonFile, String render) throws Exception{
		// path folder sample: /home/alan/Escritorio/Work/workspace/metapi2/modules/metapi-engine/files/1/files/render 
		// proceso el folder de entrada
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		//get current date time with Date()
		Date date = new Date();	
		String currentDate = dateFormat.format(date);
		String result = "";
		File dir = new File(FS_PATH + nameDir);
		File[] directoryListing = dir.listFiles();  
		InputStream json =  this.mylService.getContentsOfResourceInFolder(1, nameDir + "_Models", jsonFile);
		Scanner scanner2 = new Scanner(json,"UTF-8");
		String jsonProcess = scanner2.useDelimiter("\\A").next();
		scanner2.close();  	  
		if (directoryListing != null) {
			File folder = new File(FS_PATH + nameDir + "_MYL");
			for (File child : directoryListing) {
				//obtengo el file dentro de la carpeta de entrada
				InputStream template = this.mylService.getContentsOfResourceInFolder(1, nameDir, child.getName());
				//renderizo los files
				result = (String) new MustacheTemplate(IOUtils.toString(template)).jsonRender(jsonProcess) + MYL_MSG;
				//guardo el file result en un nuevo folder
				InputStream is1 = new ByteArrayInputStream( result.getBytes() );
				mylService.storeFolderResource(1, folder,currentDate + child.getName(), is1);
			}
		} else {
			System.out.println("The directory not contains files");
		}
		return "Done. The files are created in new folder: " + currentDate + nameDir + "_MYL";
	}
	
	public MakeYourLanguage(String name){
		this.name = name;
	}

	public Object renderTemplate(String pathFile, String jsonStr) throws Exception {
		InputStream template = this.mylService.getContentsOfResource(1, pathFile);
		return new MustacheTemplate(IOUtils.toString(template)).jsonRender(jsonStr);
	}

	
	//se usa si es un Folder o un File y anda
	@SuppressWarnings("unused")
	public JSONObject inferSampleModelFromString(String aFile) throws JSONException {
		List<String> list = new ArrayList<String>();
		JSONObject mainObj = new JSONObject();
		JSONArray jaa = new JSONArray();
		String input =  aFile;
		input = input.trim();
		char[] aCaracteres = input.toCharArray();
		String name = "";
		String attr = "";
		String s;
		int count = 0;
		//Process the input
		for (int x=0; x<aCaracteres.length - 1; x++){
			s = Character.toString(aCaracteres[x]);
			s = s.trim();

			if ((s.equals("{")) && (x > 1) && (!Character.toString(aCaracteres[x -1]).equals("{") && (!Character.toString(aCaracteres[x + 1]).equals("{")))){
				x++;
				s = Character.toString(aCaracteres[x]);
				s = s.trim();
			}		  
			if(!s.equals("{") && (!s.equals(":") && (!s.equals(";") && (!s.equals(" "))))){
				attr += s;
			}
			if (s.equals("{")){
				count ++;
				if (count == 2){
					x++;
					s = Character.toString(aCaracteres[x]);
					while (!s.equals("}")){
						name += s;
						x++;
						s = Character.toString(aCaracteres[x]);
					}
					x++;
					list.add(name);
					count = 0;
					attr = " ";
					name = "";
				}			
			}
		}		
		// Build the Output
		for (int i=0; i < list.size(); i++){
			s = list.get(i);
			if(!s.contains("#")){
				if(!mainObj.has(s)){
					mainObj.put(s, "aValue");
				}
			}
			else{
				name = s;
				JSONObject jo = new JSONObject();
				while(!s.contains("/")){
					if((!list.get(i).contains("#"))){
						jo.put(list.get(i), "aValue");
					}
					i = i + 1;
					s = list.get(i);
				}
				//jo.put(name.replace("#", ""), "true");
				jaa.put(jo);
				mainObj.put(name.replace("#", ""), jaa);
				jaa = new JSONArray();	
			}
			
		}
		return mainObj;	
	}



	//se usa si es un FILE
	public void renderTemplatesRest(String nameDir, String nameOfFile, String nameOfModel, InputStream file) throws IOException, Exception {
		String result = "";	
		InputStream json =  this.mylService.getContentsOfResourceInFolder(1, nameDir + "_Models", nameOfModel);
		Scanner scanner2 = new Scanner(json,"UTF-8");
		String jsonProcess = scanner2.useDelimiter("\\A").next();
		scanner2.close(); 
		File folder = new File(FS_PATH + nameDir + "_MYL");	
		if(nameOfFile.contains("#")){
			ArrayList<String> names = new ArrayList<String>();
			ArrayList<InputStream> list = processSintaxRender(file, jsonProcess, nameOfFile, names);
			for (int i = 0; i < list.size(); i++) {
				mylService.storeFolderResource(1, folder,names.get(i), list.get(i));
			}
		}
		else{
			result = (String) new MustacheTemplate(IOUtils.toString(file)).jsonRender(jsonProcess) + MYL_MSG;
			InputStream is1 = new ByteArrayInputStream( result.getBytes() );
			mylService.storeFolderResource(1, folder,nameOfFile, is1);
		}
	}

	// procesa la sintaxis #casses.name.java
	private ArrayList<InputStream> processSintaxRender(InputStream file, String mainJsonFile, String nameOfFile, ArrayList<String> names2) throws IOException, Exception {
		String  names = nameOfFile.replace("#", "");
		String[] words = names.split("\\.");
		JSONObject jsonObject = new JSONObject(mainJsonFile);
		ArrayList<InputStream> arrayList = new ArrayList<InputStream>();
		if(validate(words[0], jsonObject)){
			JSONArray temp = jsonObject.getJSONArray(words[0]);
			String result = "";			
			int length = temp.length();
			String theFile = IOUtils.toString(file);
			if (length > 0) {
			   for (int i = 0; i < length; i++) {  
			        InputStream json = new ByteArrayInputStream(temp.getString(i).getBytes());
			        
			        JSONObject jObject = new JSONObject(temp.getString(i));
			        String name = jObject.getString(words[1]);	     
			        names2.add(name+"."+words[2]);
			        Scanner scanner2 = new Scanner(json,"UTF-8");
					String jsonProcess = scanner2.useDelimiter("\\A").next();
					scanner2.close();  
					result = (String) new MustacheTemplate(theFile).jsonRender(jsonProcess);		
					InputStream is1 = new ByteArrayInputStream(result.getBytes());
					arrayList.add(is1);
					result = "";
			   }
			}
		}
		else{
			System.out.println("no anda");
		}
		return arrayList;
	}

	@SuppressWarnings("unused")
	private String escapeSpecialChars(String jsonRender) throws UnsupportedEncodingException {
		return jsonRender;
	}


	private boolean validate(String string, JSONObject jo) {
		return (jo.has(string));
	}
	
	
	//Se usa si es un file
	public void inferSampleModelRest(String nameOfProject, InputStream file, String nameOfFile) throws JSONException, IOException {
		JSONObject result = new JSONObject();
		result = inferSampleModelFromString(IOUtils.toString(file));
		String targetFolder = nameOfProject + "_Models";
		String res;
		InputStream is1;
		if(nameOfFile.contains("#")){
			String  names = nameOfFile.replace("#", "");
			String[] words = names.split("\\.");
			JSONArray ja = new JSONArray();
			ja.put(result);
			JSONObject main = new JSONObject();
			main.put(words[0], ja);
			is1 = new ByteArrayInputStream( main.toString().getBytes() );
			res = nameOfFile;
		}
		else{
			is1 = new ByteArrayInputStream( result.toString().getBytes() );
			res = nameOfFile.substring(0, nameOfFile.lastIndexOf("."));
			
		}
		mylService.storeFileInFolder(1, targetFolder , res + "_model.json", is1);
	}
	
	// se usa si es desde el root
	@SuppressWarnings("rawtypes")
	public void inferSampleModelFromRoot(String nameOfProject) throws IOException, JSONException {
		ArrayList<String> l = mylService.getAllFilesFromRoot(nameOfProject);
		JSONObject result = new JSONObject();
		JSONObject main = new JSONObject();
		if(l != null){
			for (int i = 0; i < l.size(); i++) {
				InputStream file =  this.mylService.getStreamFromPath(1, l.get(i));
				if(file != null){
					result = inferSampleModelFromString(IOUtils.toString(file));
					//main.put(file.toString(), result);
					Iterator i1 = result.keys();
					String tmp_key;
					while(i1.hasNext()) {
					    tmp_key = (String) i1.next();
					    if(!main.has(tmp_key)){
					    	main.put(tmp_key, result.get(tmp_key));
					    }
					}
					
				}
			}
			InputStream is1 = new ByteArrayInputStream( main.toString().getBytes() );
			String targetFolder = nameOfProject + "_Models";
			mylService.storeFileInFolder(1, targetFolder , nameOfProject + "_model.json", is1);
		}
		else{
			System.out.println("la lista no tiene paths!");
		}
	}


	// se usa si es render desde el root
	public void renderFromRoot(String nameOfProject, String nameOfModel) throws Exception {
		ArrayList<String> l = mylService.getAllFilesFromRoot(nameOfProject);
		String result = "";	
		InputStream json =  this.mylService.getContentsOfResourceInFolder(1, nameOfProject + "_Models", nameOfModel);
		Scanner scanner2 = new Scanner(json,"UTF-8");
		String jsonProcess = scanner2.useDelimiter("\\A").next();
		scanner2.close();  
		File folder = new File(FS_PATH + nameOfProject + "_MYL");	
		if(l != null){
			for (int i = 0; i < l.size(); i++) {
				InputStream file =  this.mylService.getStreamFromPath(1, l.get(i));
				if(file != null){
					result = result + (String) new MustacheTemplate(IOUtils.toString(file)).jsonRender(jsonProcess);
				}
			}
			result = result + MYL_MSG;
			InputStream is1 = new ByteArrayInputStream( result.getBytes() );
			mylService.storeFolderResource(1, folder,nameOfProject  + ".js", is1);
		}
		else{
			System.out.println("la lista no tiene paths!");
		}
	}

	//This method validate languaje write in your json model
	public InputStream validateLanguaje(String jsRequest, String model) throws IOException, JSONException {
		String result = "";
		ErrorMYL errors = new ErrorMYL();
		ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");  
        engine.put("__model", model);
        engine.put("errors", errors);
        jsRequest = "var model = JSON.parse(__model);" + jsRequest;
        try {		
			ScriptContext context = engine.getContext();
			StringWriter writer = new StringWriter();
			context.setWriter(writer);
			engine.eval(jsRequest);
			String output = writer.toString();
			result = output;
			if(errors.hasErrors()){
				ArrayList<String> array = errors.printErrors();
				for (int i = 0; i < array.size(); i++) {
					result = result +  array.get(i);
				}
			}
		} catch (ScriptException e) {
			result = "Error: " + e.toString();
		}
        InputStream inStResult = IOUtils.toInputStream(result, "UTF-8");
		return inStResult;
	}


	public String processModel(String processCode, String model) {
		Object result = null;
		String lastChar = processCode.substring(processCode.length() - 1);
		if(!lastChar.equals(";")){
			processCode = processCode + ";";
		}
		ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");  
        engine.put("__model", model);
        processCode = "var model = JSON.parse(__model);" + processCode + "JSON.stringify(model);";
        try {		
        	result = engine.eval(processCode);  	
		} catch (ScriptException e) {
			result = ">>>>>>>>>>> Error en processModel: " + e;
			System.out.println("------------> " + e);
		}
        return (String) result;
	}

	
}
