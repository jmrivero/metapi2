package org.myl.core;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Tree{
	
    private Node root;
    private ArrayList<Node> elements = new ArrayList<Node>();
    
    public class Node
    {
        String data;
        ArrayList<Node> children;

        public Node(String data)
        {
            this.data = data;
            children = new ArrayList<Node>();
        }
        
        public String getData(){
        	return this.data;
        }

        public Node getChild(String data)
        {
            for(Node n : children)
                if(n.data.equals(data))
                    return n;

            return null;
        }
        
        public boolean haveChildrens(){
        	return (children.size()>0);
        }
        
        public ArrayList<Node> getChildrens(){
			return children;
        }
        public ArrayList<String> getChildrensToString(){
			ArrayList<String> a = new ArrayList<String>();
			for (int i = 0; i < children.size(); i++) {
				a.add(children.get(i).getData());
			}
        	return a;
        }
    }

    public Tree()
    {
        root = new Node("");
    }

    public boolean isEmpty()
    {
        return root==null;
    }

    public void add(String str)
    {

        Node current = root;
        StringTokenizer s = new StringTokenizer(str, "/");
        while(s.hasMoreElements())
        {
            str = (String)s.nextElement();
            Node child = current.getChild(str);
            if(child==null)
            {
                current.children.add(new Node(str));
                child = current.getChild(str);
            }
            current = child;
        	if(!elements.contains(current)){
        		elements.add(current);
        	}
        }
    }

    public void print()
    {
        System.out.println("La cantidad de nodos que se crearon son: " + elements.size() + " y son estos: ");
        for (Node e : elements) {
			System.out.println("> " + e.data  + " Hijos-> " + e.children.size() + " , tipo: " + e);
		}
    }
    
    public ArrayList<Node> getTreeInList(){
    	return elements;
    }
    

}