package org.myl.resources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.myl.model.JSONModel;
import org.myl.model.Project;

public interface MYLService {

	public abstract List<Project> listProjects(long applicationId);

	public abstract ArrayList<String> listFilesOfProject(long applicationId, String projectName) throws IOException;
	
	public abstract void uncompressContent(long applicationId, InputStream stream, String dir) throws IOException;
	
	public abstract void storeResource(long applicationId, String path, InputStream stream);
	
	public abstract File createFolder(String string);
	
	public abstract void storeFolderResource(long applicationId, File folder, String fileName, InputStream stream);
	
	public abstract void storeFileInFolder(long applicationId, String folder, String fileName, InputStream stream);
	
	public abstract InputStream getContentsOfResourceInFolder(long applicationId, String pathFolder, String pathFile);
	
	public abstract InputStream getContentsOfResource(long applicationId, String path);

	public abstract List<JSONModel> listModelsOfProject(long applicationId, String nameOfProject);

	public abstract String getFullTreeAsString(String folderPath)throws IOException, JSONException;

	public abstract String getPathOfFile(String folderPath, String path);

	public abstract InputStream getContentOfFolder(String realPathFolder) throws FileNotFoundException;

	public abstract void editName(String route, String nameOfFile, String newNameOfFile);

	public abstract void delFile(String route, String nameOfFile);

	public abstract File getFile(String route);

	public abstract boolean validateModel(String nameOfModel, String nameOfProject);

	public abstract void deleteModel(String nameOfProject, String nameOfFile);

	public abstract void addFile(String route, String newNameOfFile) throws IOException;


}
