package org.myl.resources;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.myl.core.MakeYourLanguage;
import org.myl.core.Tree;
import org.myl.core.Tree.Node;
import org.myl.model.FileMYL;
import org.myl.model.JSONModel;
import org.myl.model.Project;
import org.myl.utils.PathUtil;
import org.myl.utils.ProcessFile;

public class FilesystemMYLService implements MYLService {

	private static Log log = LogFactory.getLog("hola");
	@SuppressWarnings("unused")
	private MakeYourLanguage mylCore;
	private static final String ENCODED_PATH_SEPARATOR = "___";
	private static final String FILES_DIRECTORY = "files";
	private static final String MYL_DIRECTORY = "MYL";
	private static final int BUFFER_SIZE = 4096;
	private String rootPath = "./files";
	private File rootDir;
    
	
	public FilesystemMYLService(){
		
	}
	
	private void initializeFS(String absolutePath) throws IOException {
		String filename = new File(absolutePath).getName();
		//System.out.println("./files/1/files/MYL/projects/"+ filename + "_MYL");
		//new File("./files/1/files/MYL/projects/"+ filename + "_MYL").mkdir();
		//new File("./files/1/files/MYL/projects/"+ filename + "_Models").mkdir();
		new File("./files/1/files/MYL/projects/"+ filename + "_Rules").mkdir();
		File f = new File("./files/1/files/MYL/projects/"+ filename + "_Rules/" + filename + "_rule.js");
		f.createNewFile();
		FileWriter fw = new FileWriter(f.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("// You can acces to model with variable model and settings error with errors.addError([aString])");
		bw.close();
		
		new File("./files/1/files/MYL/projects/"+ filename + "_Process").mkdir();
		File f2 = new File("./files/1/files/MYL/projects/"+ filename + "_Process/" + filename + "_process.js");
		f2.createNewFile();
		FileWriter fw2 = new FileWriter(f2.getAbsoluteFile());
		BufferedWriter bw2 = new BufferedWriter(fw2);
		bw2.write("// You can acces to model with variable model");
		bw2.close();
		
		//this.storeFileInFolder(0, "./files/1/files/MYL/projects/"+ filename + "_Rules", "s", new FileInputStream(new File("./files/1/files/MYL/projects/"+ filename + "_Rules/sarasa.js")));
	}
	
	public FilesystemMYLService(String rootPath) throws IOException {
		this.setRootPath(rootPath);
		this.initializeDirectory();
		
	}

	private void initializeDirectory() throws IOException {

		try {
			this.setRootDir(new File(this.getRootPath()));
			FileUtils.forceMkdir(this.getRootDir());
		} catch (IOException e) {
			log.error("Error intializing root directory", e);
		}
	}
	
	@Override
	public List<Project> listProjects(long applicationId) {
		List<Project> files = new ArrayList<Project>();
		File directory = new File(this.buildFilesPathMYL(applicationId, ""));
		if (!directory.exists()) {
			directory.mkdirs();
		}	

		File[] fList = directory.listFiles();
		for (File file : fList){
			if (file.isDirectory()){
				Project p = new Project(file.getName());
				files.add(p);
			}
		}						
		return files;
	}

	
	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	private void setRootDir(File string) {
		this.rootDir = string;
	}
	
	
	private String encodePath(String path) {
		return path.replaceAll(File.separator, ENCODED_PATH_SEPARATOR);
	}
	

	private String decodePath(String path) {
		return path.replaceAll(ENCODED_PATH_SEPARATOR, File.separator);
	}
	
	private String buildFilesPathMYL(long applicationId, String path) {
		return this.getRootPath() + File.separator + applicationId + File.separator + FILES_DIRECTORY + File.separator + MYL_DIRECTORY + File.separator + "projects" + File.separator + this.encodePath(path);
	}
	
	


	private String buildFilesPath(long applicationId, String path) {
		return this.getRootPath() + File.separator + applicationId + File.separator + FILES_DIRECTORY + File.separator + this.encodePath(path);
	}


	private File getRootDir() {
		return rootDir;
	}


	private String buildFilesPathFolder(long applicationId, String folder, String pathFile) {
		return (this.getRootPath() + File.separator + applicationId + File.separator + FILES_DIRECTORY + File.separator + MYL_DIRECTORY + File.separator + "projects" + File.separator + folder + File.separator + this.encodePath(pathFile));
	}
	
	@Override
	public ArrayList<String> listFilesOfProject(long applicationId, String projectName) throws IOException {
		List<FileMYL> files = new ArrayList<FileMYL>();
		File directory = new File(this.buildFilesPathMYL(applicationId, "")+projectName);
		ArrayList<String> l = new ArrayList<String>();
		getFileNames(l, directory.toPath());
		for (int i = 0; i < l.size(); i++) {
			FileMYL f = new FileMYL(l.get(i));
			files.add(f);
		}	
		String root = this.buildFilesPathMYL(applicationId, "")+projectName;
		FileVisitor<Path> fileProcessor = new ProcessFile();
		Files.walkFileTree(Paths.get(root), fileProcessor);
		return null;
	}
	
	
	private List<String> getFileNames(List<String> fileNames, Path dir){
	    try {
	        DirectoryStream<Path> stream = Files.newDirectoryStream(dir);
	        for (Path path : stream) {
	            if(path.toFile().isDirectory())getFileNames(fileNames, path);
	            else {
	                fileNames.add(path.toAbsolutePath().toString());
	            }
	        }
	        stream.close();
	    }catch(IOException e){
	        e.printStackTrace();
	    }
	    return fileNames;
	} 

	
	
	
	
	@Override
	public void uncompressContent(long applicationId, InputStream stream, String destDirectory) throws IOException {
		 	File destDir = new File(destDirectory);
		 	
	        if (!destDir.exists()) {
	            destDir.mkdir();
	        }
	        ZipInputStream zipIn = new ZipInputStream(stream);
	        ZipEntry entry = zipIn.getNextEntry();
	        if ((entry != null)){
	        	this.initializeFS(destDirectory + File.separator + entry.getName());
	        }
	        // iterates over entries in the zip file
	        while (entry != null) {
	            String filePath = destDirectory + File.separator + entry.getName();
	            if (!entry.isDirectory()) {
	                // if the entry is a file, extracts it
	                extractFile(zipIn, filePath);
	            } else {
	                // if the entry is a directory, make the directory
	                File dir = new File(filePath);
	                dir.mkdir();
	            }
	            zipIn.closeEntry();
	            entry = zipIn.getNextEntry();
	        }
	        zipIn.close();
	        
	}
	


	private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
	
	
	@Override
	public void storeFolderResource(long applicationId, File folder, String fileName, InputStream stream) {
			try {
				FileUtils.writeByteArrayToFile(new File(this.buildFilesPathFolder(applicationId, folder.getName(), fileName)), IOUtils.toByteArray(stream));
			} catch (IOException e) {
				log.error("Error storing file", e);
			}
	}
	
	
	@Override
	public File createFolder(String string) {
		File folder = new File(string);
		folder.mkdir();
		return folder;
	}


	
	@Override
	public InputStream getContentsOfResourceInFolder(long applicationId, String pathFolder, String pathFile) {
		try {
			return (new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(this.buildFilesPathFolder(1, pathFolder, pathFile)))));
		} catch (IOException e) {
			log.error("Error getting contents of file", e);
			return null;
		}
	}

	@Override
	public InputStream getContentsOfResource(long applicationId, String path) {
		try {
			return new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(this.buildFilesPath(applicationId, path))));
		} catch (IOException e) {
			log.error("Error getting contents of file", e);
		}
		return null;
	}
	
	@Override
	public void storeResource(long applicationId, String path, InputStream stream) {
		try {
			FileUtils.writeByteArrayToFile(new File(this.buildFilesPath(applicationId, path)), IOUtils.toByteArray(stream));
		} catch (IOException e) {
			log.error("Error storing file", e);
		}
	}


	@Override
	public void storeFileInFolder(long applicationId, String folder,
			String fileName, InputStream stream) {
		try {
			FileUtils.writeByteArrayToFile(new File(this.buildFilesPathFolder(applicationId, folder, fileName)), IOUtils.toByteArray(stream));
		} catch (IOException e) {
			log.error("Error storing file", e);
		}
	}


	@Override
	public  List<JSONModel> listModelsOfProject(long applicationId, String nameOfProject) {
		List<JSONModel> files = new ArrayList<JSONModel>();
		File directory = new File(this.buildFilesPathMYL(applicationId, "")+nameOfProject);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		File[] fList = directory.listFiles();
		for (File file : fList){
			if (file.isFile()){
				JSONModel f = new JSONModel(file.getName());
				files.add(f);
			}
		}
		
		return files;
	}
	
	private String getShortPath(String path){
		return (path.replace("./files/1/files/MYL/projects/", ""));
	}
	
	//Este metodo retorna el JSON string del arbol del filesystem con root folderPath
	@Override
	public String getFullTreeAsString(String folderPath) throws IOException, JSONException {
		String root = this.buildFilesPathMYL(1, folderPath);
		ProcessFile pf = new ProcessFile();
		FileVisitor<Path> fileProcessor = pf;
		Files.walkFileTree(Paths.get(root), fileProcessor);
		Tree t = new Tree();
		ArrayList<File> alist = pf.getList();
		for (int i = 0; i < alist.size(); i++) {
			t.add(getShortPath(alist.get(i).getPath()));
		}
		ArrayList<Node> al = t.getTreeInList();
		JSONArray mainJo = new JSONArray();
		Node node = al.get(0);
		JSONObject jo = new JSONObject();
		jo.put("title", node.getData());
		if(node.haveChildrens()){
			jo.put("folder", "true");
			JSONArray ja = buildJsonChild(node);
			jo.put("children", ja);
		}
		mainJo.put(jo);
		return mainJo.toString();
	}

	private JSONArray buildJsonChild(Node node) throws JSONException{
		JSONArray jArray = new JSONArray();
		for (int i = 0; i < node.getChildrens().size(); i++) {
			JSONObject j = new JSONObject();
			j.put("title", node.getChildrens().get(i).getData());
			if(node.getChildrens().get(i).haveChildrens()){
				j.put("folder", "true");
				JSONArray ja = buildJsonChild(node.getChildrens().get(i));
				j.put("children", ja);
			}
			jArray.put(j);
		}
		return jArray;
	}

	@Override
	public String getPathOfFile(String folderPath, String filePath) {
		File dir = new File(this.buildFilesPathMYL(1, folderPath));
		PathUtil pu = new PathUtil();
		pu.findPath(filePath, dir);
		return this.getShortPath(pu.getPath());
	}

	@Override
	public InputStream getContentOfFolder(String realPathFolder) throws FileNotFoundException {
		InputStream result;
		result = new FileInputStream(this.buildFilesPathMYL(1, realPathFolder));
		return result;
	}


	public ArrayList<String> getAllFilesFromRoot(String nameOfProject) throws IOException {
		ArrayList<String> res = new ArrayList<String>();
		String root = this.buildFilesPathMYL(1, nameOfProject);
		ProcessFile pf = new ProcessFile();
		FileVisitor<Path> fileProcessor = pf;
		Files.walkFileTree(Paths.get(root), fileProcessor);		
		ArrayList<File> alist = pf.getList();
		for (int i = 0; i < alist.size(); i++) {	
			res.add(alist.get(i).getPath());
		}
		return res;
	}

	public InputStream getStreamFromPath(int i, String path) throws IOException {
		String p =  decodePath(path);
		File f = new File(p);
		//System.out.println("path : " + p);
		String extension = "";
		int i1 = p.lastIndexOf('.');
		int x = Math.max(p.lastIndexOf('/'), p.lastIndexOf('\\'));
		if (i1 > x) {
		    extension = p.substring(i1+1);
		}
		if(f.isFile() && (valid(extension))){
			InputStream is = new ByteArrayInputStream(FileUtils.readFileToByteArray(f));
			return is;
		}
		else{
			return null;
		}
		
	}

	private boolean valid(String extension) {
		Boolean res = true;
		switch (extension) {
		  case "js":
		        break;
		  case "java":
		        break;
		  case "json":
		        break;
		  case "txt":
		        break;
		  case "html":
		        break;
		  case "jsp":
		        break;
		  case "xml":
		        break;
		  case "cs":
		        break;
		  case "css":
		        break;
		  default:
		        res = false;
		        System.out.println("Invalid File for MYL");
		        break;
		}
		return res;
	}

	@Override
	public void editName(String route, String nameOfFile, String newNameOfFile) {		
		if(route.contains(nameOfFile)){
			File oldfile =new File(this.buildFilesPathMYL(1, route).replace("___", "/"));
			File newfile =new File(this.buildFilesPathMYL(1, route.replace(nameOfFile, newNameOfFile)).replace("___", "/"));
			oldfile.renameTo(newfile);
		}
		else{
			int i1 = nameOfFile.lastIndexOf('.');
			String ext =  nameOfFile.substring(i1+1).toLowerCase();
			if((!newNameOfFile.contains("."+ext)) || (!newNameOfFile.contains("."))){
				if(!newNameOfFile.contains(".")){
					newNameOfFile =  newNameOfFile + ".js";
				}
				else{
					int i2 = newNameOfFile.lastIndexOf('.');
					String extN =  newNameOfFile.substring(i2+1);
					if(valid(extN.toLowerCase())){
						newNameOfFile = newNameOfFile.replace(extN, extN.toLowerCase());
					}
					else{
						newNameOfFile = newNameOfFile.replace(extN, ext);
					}
				}
			}
			
			File oldfile = new File(this.buildFilesPathMYL(1, route + "/" + nameOfFile).replace("___", "/"));
			File newfile =new File(this.buildFilesPathMYL(1, route + "/" + newNameOfFile).replace("___", "/"));
			if(oldfile.renameTo(newfile)){
				System.out.println("Rename succesful");
			}else{
				System.out.println("Rename failed");
			}
		}
	
	}

	@Override
	public void delFile(String route, String nameOfFile) {
		File oldfile;
		if(!route.contains(nameOfFile)){
			oldfile = new File(this.buildFilesPathMYL(1, route + "/" + nameOfFile).replace("___", "/"));
		}
		else{
			oldfile = new File(this.buildFilesPathMYL(1, route).replace("___", "/"));
		}
		if(oldfile.isDirectory()){
			deleteDirectory(oldfile);
		}
		oldfile.delete();
	}



	private void deleteDirectory(File directory) {
		File[] fList = directory.listFiles();
		for (File file : fList){
			if (file.isDirectory()){
				deleteDirectory(file);
			}
			else{
				file.delete();
			}
		}							
	}

	@Override
	public File getFile(String route) {
		String p = this.buildFilesPathMYL(1,route);
		return (new File(p.replace("___", File.separator)));
	}

	@Override
	public boolean validateModel(String nameOfModel, String nameDir) {
		ProcessFile pf = new ProcessFile();
		InputStream json =  this.getContentsOfResourceInFolder(1, nameDir + "_Models", nameOfModel);
		Scanner scanner2 = new Scanner(json,"UTF-8");
		String jsonProcess = scanner2.useDelimiter("\\A").next();
		scanner2.close(); 
		return pf.isJSONValid(jsonProcess);
	}

	public void storeRule(InputStream is2, String targetFolder1, String string) {
		this.storeFileInFolder(1, targetFolder1 , string, is2);
	}


	public void deleteModel(String nameOfProject, String nameOfFile) {
		File path;		
		if(nameOfProject.contains("MYL")){
			path = new File(this.buildFilesPathMYL(1, nameOfProject) + "/" + nameOfFile);
		}
		else{
			if(nameOfFile.contains("_rule.js")){
				path = new File(this.buildFilesPathMYL(1, nameOfProject) + "_Rules/" + nameOfFile);
			}
			else{
				path = new File(this.buildFilesPathMYL(1, nameOfProject) + "_Models/" + nameOfFile);
			}	
		}
		path.delete();
	}

	@Override
	public void addFile(String route, String newNameOfFile) throws IOException {
		File f =  null;
		if(newNameOfFile.contains(".json")){
			f = new File(this.buildFilesPathMYL(1,route + "_Models" + File.separator + newNameOfFile).replace("___", "/"));
		}
		else{
			String extension = "";
			int i1 = newNameOfFile.lastIndexOf('.');
			int x = Math.max(newNameOfFile.lastIndexOf('/'), newNameOfFile.lastIndexOf('\\'));
			if (i1 > x) {
			    extension = newNameOfFile.substring(i1+1);
			}
			if(!(valid(extension))){
				newNameOfFile.replace(extension, " ");
				newNameOfFile = newNameOfFile + ".txt";
			}
			f = new File(this.buildFilesPathMYL(1,route + File.separator + newNameOfFile).replace("___", "/"));	
		}
		f.createNewFile();
	}


	 

}
