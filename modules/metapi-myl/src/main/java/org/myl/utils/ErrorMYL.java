package org.myl.utils;

import java.util.ArrayList;

public class ErrorMYL {

	private ArrayList<String> errors = new ArrayList<String>();
	
	

	public ErrorMYL() {
		super();
		//this.errors.add("");
	}

	public ArrayList<String> getErrors() {
		return errors;
	}

	public void addError(String e) {
		this.errors.add(e);
	}
	
	public boolean hasErrors(){
		return (errors.size() > 0);
	}
	
	private String getError(Integer i){
		return errors.get(i);
	}
	
	public ArrayList<String> printErrors(){
		ArrayList<String> a = new ArrayList<String>();
		for (int i = 0; i < errors.size(); i++) {
			int num = i + 1;
			a.add("Error " + num + ": " + this.getError(i));
		}
		return a;
	}
	
	
}
