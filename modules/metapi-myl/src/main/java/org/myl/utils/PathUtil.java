package org.myl.utils;

import java.io.File;

public class PathUtil {

	public String path  = "empty";
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void findPath(String name,File file)
    {
        File[] list = file.listFiles();
        if(list!=null)
        for (File fil : list)
        {
            if (fil.isDirectory())
            {
            	if(name.equals(fil.getName())){
            		this.setPath(fil.getPath());
            	}
            	else{
            		if (name.equals(fil.getParentFile().getName())){
            			this.setPath(fil.getParentFile().getName());
            		}
            	}
                findPath(name,fil);
            }
            else if (name.equalsIgnoreCase(fil.getName()))
            {
                this.setPath(fil.getParentFile().getPath());      
            }
        }
    }
	
}
