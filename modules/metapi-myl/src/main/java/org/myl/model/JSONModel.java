package org.myl.model;

public class JSONModel {

	private long id;
	private String name;
	private ValidateRule recipe;
	
	public JSONModel(String name) {
		super();
		this.name = name;
	}
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ValidateRule getRecipe() {
		return recipe;
	}
	public void setRecipe(ValidateRule aRecipe) {
		this.recipe = aRecipe;
	}
	
}
