package org.myl.model;

public class ValidateRule {
	
	private String contentOfRule;
	private long id;
	
	public ValidateRule(String rule) {
		super();
		this.contentOfRule = rule;
	}
	
	public String getContentOfRecipe() {
		return contentOfRule;
	}

	public void setContentOfRecipe(String content) {
		this.contentOfRule = content;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
		

}
