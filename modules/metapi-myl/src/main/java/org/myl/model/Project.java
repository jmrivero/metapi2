package org.myl.model;

public class Project {

	
	private String name;
	private long id;
	
	public Project(String name) {
		super();
		this.name = name;
	}
	
	public Project(){
		
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	
	
}
