<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>History MYL</title>

<script src="../lib/codemirror.js"></script><style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/bootstrap.min.js"></script>
<script src="../lib/alert.js"></script>
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<script src="../libs/class.js" type="text/javascript" ></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="../libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript" ></script>
		
<link rel="stylesheet" href="../css/MYLStyle.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">

<style>
	#alerts {
		/*height: 63px;*/
	}
  
  #fileListH {
 
    margin: 0px;
    padding: 0px;
    height:  430px;
    overflow-y: scroll;
  }
</style>

</head>
<body style="">
  
<jsp:include page="parts/menuMYL.jsp"></jsp:include>

<script type="text/javascript">
	var projectsMYL = "../../metapi/MYL/1/projectsMYL/"
	var fileSystemPath = "../../metapi/MYL/1/"
	var href = jQuery(location).attr('href').split("/");
	var name = href[5];
	var templateEditor;
	var currentFilename = null;
	var newFile = false;
	var applicationId = 2;
	var popupManager = new PopupManager();
	
    var extensionModeMappings = {
        	js: "javascript",
        	xml: "xml",
        	html: "htmlmixed",
        	htm: "htmlmixed",
        	css: "css",
        	tl: "dust"
        }
	
    function extractExtension(filename) {
    	var parts = filename.split(".");
      return parts[parts.length - 1];
    }
    
    function updateEditorMode(filename) {
    	var extension = extractExtension(filename);
    	if (extensionModeMappings[extension]) {
    		templateEditor.setOption("mode", extensionModeMappings[extension]);
    	}
    	
    }
    
    function fileSelected(filename, content) {
    	$("#download").attr('href',"../../files/1/files/MYL/projects/"+ name  +"/"+ filename + "\"");
        templateEditor.setValue(content);
        updateEditorMode(filename);
        $("#filename").text(filename);
        currentFilename = filename;
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
	
    function fileSelectedProject(filename) {
    	$("#download").attr('href',"../../files/1/files/MYL/projects/"+ name + "\"");
    	$("#filename").text(filename);
    	templateEditor.setValue("/* You have selected the folder " + filename + " with all its files.*/");
    	templateEditor.setOption("readOnly", true);
    	updateEditorMode(filename);
		currentFilename = "/" +filename;
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
	
    function addFileToList(filename) {
        var html = "<li class=\"list-group-item\">";
        filename = filename[0] == '/' ? filename.substring(1) : filename;
        var extension = extractExtension(filename);
        if (extensionModeMappings[extension]) {
        
          html = html + '<img class="icon-file" src="../images/file.png" /><a href="file/[filename]" class="fileLink">[filename]</a>'.replace(/\[filename\]/g, filename);
        } else {
        	 html = html + '<img class="icon-file" src="../images/folder.png" /><a href="#" class="project">[filename]</a>'.replace(/\[filename\]/g, filename);
        }
        html = html + "</li>"; 
        var jHtml = $(html);
        jHtml.find('a.fileLink').click(function(e) {
            var self = this;
            e.preventDefault();   
            $.ajax({url: fileSystemPath + name + "/" + filename , type: "GET", success: function(response) {
              fileSelected(filename, response)
             }});
        });
        jHtml.find('a.project').click(function(e) {
            var self = this;
            e.preventDefault();   
            fileSelectedProject(filename);
        });
        $("#fileListH").append(jHtml);
    }
	
	
	 $(document).ready(function() {
	    	$("#filenameTitle").text("Files in the project " + name + ":");
	    	
	    	  templateEditor = CodeMirror($("#editor")[0], {
	              value: "",
	              mode:  "dust",
	              lineNumbers: true
	          });
	          templateEditor.setSize(800, 450);
	          templateEditor.setOption("readOnly", true);
	          
	        $("#hisButton").append("<a id=\"download\" href=\"#\" rel=\"nofollow\">Download Here</a>");
	          
	        $("#download").click(function(){
	      		location.reload();
	    	});
	        
	          addFileToList(name);  
	        $.ajax({url: projectsMYL + name , type: "GET", dataType: 'json', success: function(response) {
	            $.each(response.files, function(_, filename) {
	           	 addFileToList(filename);
	           });
	        }});
	 });


</script>

  <div class="container">
	 <h4 class="titleMYL"><strong>History</strong>

    <span id="filename">No files selected.</span>
	</h4>
	<div id="hisButton">
    </div> 
    <div class="row">
      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      	<h5><span id="filenameTitle"></span></h5>
      	
        <ul id="fileListH" class="list-group">
		</ul>
      </div>
      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
           <h5>
        	  <span id="filename"></span>
        	</h5>
        </div>
        
     
        
        <div class="row">
          <div id="editorContainer">
            <div id="editor">
			</div>
          </div>
        </div>
      </div>
    </div>

    	
  </div>

  <blockquote class="center">
	  <p></p>
	  <footer>
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MetApi Service</cite>
	  </footer>
  </blockquote>

</body></html>
