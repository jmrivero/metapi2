<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Factory MYL</title>
<script src="../lib/codemirror.js"></script><style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/alert.js"></script>
<script src="../libs/class.js" type="text/javascript" ></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js" type="text/javascript"></script>
<!-- Include Fancytree skin and library -->
<link href="../lib/dist/skin-win8/ui.fancytree.min.css" rel="stylesheet" type="text/css">
<script src="../lib/dist/jquery.fancytree.min.js" type="text/javascript"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="../libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript" ></script>		
<link rel="stylesheet" href="../css/MYLStyle.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="shortcut icon" href="../images/favicon.ico" /> 
</head>
<body style="">

<jsp:include page="parts/menuMYL.jsp"></jsp:include>

  <script type="text/javascript">
	var templateEditor;
	var currentFilename = null;
	var newFile = false;
	var applicationId = 2;
	var popupManager = new PopupManager();

  	var editorMYLPath = "../../metapi/MYL/1/editorMYL/"
  	var fileSystemPath = "../../metapi/MYL/1/"
  	var inferPath = "../../metapi/MYL/1/infer/"
  	var delPath = "../../metapi/MYL/1/del/"
  	var renderPath = "../../metapi/MYL/1/render/"
  	var models = "../../metapi/MYL/1/models/"
  
  	var href = jQuery(location).attr('href').split("/");
	var name = href[5];  	
  	var file;
	
    var extensionModeMappings = {
        	js: "javascript",
        	xml: "xml",
        	html: "htmlmixed",
        	htm: "htmlmixed",
        	css: "css",
        	tl: "dust",
        	json: "json"
        }
	
    function extractExtension(filename) {
    	var parts = filename.split(".");
      	return parts[parts.length - 1];
    }
    
    function updateEditorMode(filename) {
 
    		templateEditor.setOption("mode", "javascript");
    	
    }
    
    function fileSelected(filename, content) {
    	templateEditor.setOption("readOnly", false);
    	$("#filename").text(filename);
    	templateEditor.setValue(content);
        updateEditorMode(filename);
        if(esJson(filename)){
        	currentFilename = "_Models/" + filename;
        }
        else{
        	currentFilename = "/" +filename;
        }
        $("#editorContainer").fadeIn(500);
        newFile = false;
    }
    
    
    function esJson(sub){
    	var res = sub.split(".");
    	if(res[res.length - 1] == "json"){
    		return true;
    	}
    	else{
    		return false;
    	}
    }

   

    function deleteFile(param1){
    	var x=param1;
    	$.ajax({url: delPath + name + "/" + x, type: "DELETE", success: function(response) {
    		$('#fileListModel').empty();
    		$.ajax({url: models + name + "_Models", type: "GET", dataType: 'json', success: function(response) {
                $.each(response.files, function(_, filename) {
               	 addFileToListModels(filename);
               });
            }});   	  
         }});
    	deleted();
    }
    
    function addFileToListModels(filename) {
        var html = "<li class=\"list-group-item-e\">";
        filename = filename[0] == '/' ? filename.substring(1) : filename;
       	html = html + "<input title=\"Delete\" type=\"image\" id=\"filename\" class=\"icon-file\" src=\"../images/remove.png\" onclick=\"deleteFile('"+filename+"');\"/><img class=\"icon-file\" src=\"../images/file.png\" /><a href=\"file/[filename]\" class=\"fileLink\" title=\"[filename]\"><p class=\"ellipsis\">[filename]</p></a>".replace(/\[filename\]/g, filename);
        html = html + '</li>'; 
        var jHtml = $(html);
        jHtml.find('a.fileLink').click(function(e) {
            var self = this;
            e.preventDefault();   
            $.ajax({url: fileSystemPath  + name + "_Models/" + filename , type: "GET", success: function(response) {
              fileSelected(filename, response)
             }});
        });
        $("#fileListModel").append(jHtml);
        $("#modelSelect").append('<option><img class="icon-file" src="../images/file.png" />'+ filename + "</option>");
    }
    
    function fileSaved(filename, content) {
    	popupManager.showSuccess("File saved!!");
    	if (newFile) {
    		addFileToList(filename, name);
    		newFile = false;
    	}
    }
    
    function inferSaved(filename, content) {
    	popupManager.showSuccess("Your model was successfully inferred.");
    }
    
    function deleted() {
    	popupManager.showSuccess("Your model was successfully deleted.");
    }
    
    function renderSaved(filename, content) {
    	$("#myModal").modal('hide');
    	$("#msj").append("<p id=\"A\" class=\"text-success\">The file are stored in versions area. Click <a href=\"../historyMYL/" + name + "_MYL\" ><i>HERE</i></a> for view, or go to the main page and then enter in Browse Versions for project "+name+".</p>");
    	$("#oModal").modal('show');
    }
        
    $(document).ready(function() {	
    	

    	
    	$("#filenameTitle").text("Files in the project " + name + ":");
    	$("#modelsTitle").text("Models for project " + name + ":");
    	
    	  templateEditor = CodeMirror($("#editor")[0], {
              value: "> Select file",
              readOnly: true,
              mode:  "javascript",
              lineNumbers: true,
              theme: 'default', // the theme can be set here
              lineWrapping: true,
              styleActiveLine: true,
              matchBrackets: true,
              indentUnit: 4,
              extraKeys: {
                  "Ctrl-S": function (instance) {
                	  var fileN = $("#filename").text();
                      if(fileN == "No files selected."){
                    	  popupManager.showError("You must select a file to save it.");
                      }
                      else{
                	  	$.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
                         	fileSaved(currentFilename, templateEditor.getValue());
                     	}});
                      }
                     return false;
                  },
                  "Cmd-S": function (instance) {
                     $.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
                          fileSaved(currentFilename, templateEditor.getValue());
                     }});
                     return false;
                  }
               }
          });
    
          templateEditor.setSize(920, 440);
      	
          
          $("#saveButton").click(function(e) {
              var self = this;
              e.preventDefault(); 
              var fileN = $("#filename").text();
              if(fileN == "No files selected."){
            	  popupManager.showError("You must select a file to save it.");
              }
              else{
              	$.ajax({url: fileSystemPath + name  + currentFilename, type: "PUT", data: templateEditor.getValue(), success: function(response) {
               	 fileSaved(currentFilename, templateEditor.getValue());
               	}});
              }
          });
          
          $("#inferButton").click(function(e) {
              var self = this;
              newFile = true;
              var fileN = $("#filename").text();
              e.preventDefault(); 
              if(fileN == "No files selected."){
            	  popupManager.showError("You must select a file to infer a model.");
              }
              else{
	              if(fileN == name){
	                  $.ajax({url: inferPath + name + "/**", type: "PUT", data: templateEditor.getValue(), success: function(response) {
	                	  addFileToListModels(name + "_model.json");
	    	              inferSaved();
	                   }});  
	              }
	              else{
	            	  var res = fileN.split(".")[0];
	                  $.ajax({url: inferPath + name + "/" + fileN, type: "PUT", data: templateEditor.getValue(), success: function(response) {
	                	  addFileToListModels(res + "_model.json");
	    	              inferSaved();
	    	              
	                   }});
	              }	 
              }
              
          });
          
          
      	$("#L").click(function(){
      		$("#A").remove();
    		$("#myModal").modal('show');
    	});
      	
      	$("#reload").click(function(){
      		location.reload();
    	});
      	

      	
      	$("#runRender").click(function(){
    		var input = $("#fileRender").val();
    		var model =  $("#modelSelect").val();
            $.ajax({url: renderPath + name + "/" + file + "/" + model, type: "PUT", success: function(response) {
	          renderSaved(); 
             }});	
    	});
 
        $.ajax({url: models + name + "_Models", type: "GET", dataType: 'json', success: function(response) {
            $.each(response.files, function(_, filename) {
           	 addFileToListModels(filename);
           });
        }});
        
        
        $("#tree").fancytree({ 	
        	checkbox: false,      	
        	classNames: {checkbox: "fancytree-radio"},
        	source: $.ajax({
		         	url: fileSystemPath + name, type: "GET",
		         	dataType: "json",
		         	success: function(response) {
		              
		            }
		    }),
            activate: function(event, data){
                var node = data.node;
                $.ajax({url: fileSystemPath + name + "/" + node.title , type: "GET", success: function(response) {
                    fileSelected(node.title, response)
                }});
            },
        });
     
        
        $("#fileSelect").fancytree({ 	
        	checkbox: false,      	
        	classNames: {checkbox: "fancytree-radio"},
        	source: $.ajax({
		         	url: fileSystemPath + name, type: "GET",
		         	dataType: "json",
		         	success: function(response) {
		              
		            }
		    }),
            activate: function(event, data){
                var node = data.node;
                $("#fileRender").empty();
                $("#fileRender").append(node.title);
                file = node.title;
            },
        });
        
   

    });
    
		</script>
		

		

  <div class="container">
  	<h4 class="titleMYL"><strong>Factory</strong>
  	<button class="btn btn-success" id="saveButton">Save</button>
    <button class="btn btn-myl-rndr" id="inferButton">Infer sample model</button> <span id="filename">No files selected.</span>
	<!-- <button class="btn btn-myl-rndr" id="renderButton" data-toggle="modal" data-target="#myModal">Generate render</button>-->
	
	<button id="L" class="btn btn-myl-rndr">Generate render</button>
	</h4>

		  <div class="bs-example">  
		    <!-- Modal HTML -->
		    <div id="myModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Generate new render</h4>
		                </div>
		                <div class="modal-body">
		                    <p>Render Factory <img class="icon-render" src="../images/render.jpg" /></p>
		                    <p class="text-warning"><small>You must be select one file and model to render</small></p>
		                    <label>Select your file:</label><div id ="fileSelect"></div>
		                     <p><i>File Selected to be renderized</i>: <label id="fileRender"></label></p>
							<br>
							<img class="icon-render" src="../images/add.png" />
							<br>
							 <label>Select your model:</label><select id="modelSelect" class="form-control" data-style="btn-primary">

							</select>
		                </div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button type="button" id="runRender" class="btn btn-success">Generate</button>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div id="oModal" class="modal fade">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title">Render Factory</h4>
		                </div>
		                <div class="modal-body">
		                    <p>Congratulations! rendering the file has been successfully. <img class="icon-render-ok" src="../images/ok.png" /></p>
		                    <div id="msj">
		                    </div>
		               
		                </div>
		                <div class="modal-footer">
		                    <button id="reload" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	
	
	
	<hr>
	

	
    <div class="row">
   
      <div id="div-list" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
      	<h5><span id="filenameTitle"></span></h5>
	
		<!-- Define where the tree should appear -->
  		<div id="fileList">
  		<div id="tree">
  		</div>
		</div>

		<hr>
		<h5><span id="modelsTitle"></span></h5>
		<ul id="fileListModel" class="list-group">
		</ul>
      </div>
      <div  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
        <div class="row">
          <div id="editorContainer">
            <div id="editor">
			</div>
          </div>
        </div>
      </div>

      
    </div>

    	
  </div>

  <blockquote class="center">
	  <p></p>
	  <footer>
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <i>@AlanGarciaC</i> <cite title="Source Title" > � MYL Project</cite>
	  </footer>
  </blockquote>

</body></html>
