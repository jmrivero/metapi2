<!DOCTYPE html>
<html>
		<head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script src="libs/jquery.js" type="text/javascript" ></script>
		<script src="libs/jquery-ui.js" type="text/javascript" ></script>
		<script src="libs/class.js" type="text/javascript" ></script>
		<script src="libs/xml2json.js" type="text/javascript" ></script>
		<script src="libs/x2js.js" type="text/javascript" ></script>
		<script src="libs/underscore-min.js" type="text/javascript" ></script>
		
		<script src="libs/noty/jquery.noty.js" type="text/javascript" ></script>
		<script src="libs/noty/layouts/top.js" type="text/javascript" ></script>
		<script src="libs/noty/layouts/topRight.js" type="text/javascript" ></script>
		<script src="libs/noty/themes/default.js" type="text/javascript" ></script>
		<script src="libs/jquery.upload-1.0.2.min.js" type="text/javascript"></script>
		
		<script src="scripts/metapiParser.js" type="text/javascript" ></script>
		
		<script src="scripts/model/ModelObject.js" type="text/javascript" ></script>
		<script src="scripts/model/MockApiModel.js" type="text/javascript" ></script>
		<script src="scripts/model/Mockup.js" type="text/javascript" ></script>
		<script src="scripts/model/Feature.js" type="text/javascript" ></script>
		<script src="scripts/model/Association.js" type="text/javascript" ></script>
		
		<script src="scripts/figure/Figure.js" type="text/javascript" ></script>
		<script src="scripts/figure/FeatureFigure.js" type="text/javascript" ></script>
		<script src="scripts/figure/MockupEditorFigure.js" type="text/javascript" ></script>
		<script src="scripts/figure/MockApiModelEditorFigure.js" type="text/javascript" ></script>
		<script src="scripts/figure/LineConnectionFigure.js" type="text/javascript" ></script>
		<script src="scripts/figure/ConnectionHandlerFigure.js" type="text/javascript" ></script>
		<script src="scripts/figure/RemoveHandlerFigure.js" type="text/javascript" ></script>
		
		<script src="scripts/model/ModelObject.js" type="text/javascript" ></script>
		<script src="scripts/serialization/ModelXMLSerializer.js" type="text/javascript" ></script>
		<script src="scripts/serialization/SimpleJSONSerializer.js" type="text/javascript" ></script>
		<script src="scripts/serialization/ModelJSONSerializer.js" type="text/javascript" ></script>
		<script src="scripts/store/MockApiLocalStorageModelStore.js" type="text/javascript" ></script>
		<script src="scripts/store/MockApiRestStore.js" type="text/javascript" ></script>
		<script src="scripts/util/TextPopup.js" type="text/javascript" ></script>
		<script src="scripts/util/PopupManager.js" type="text/javascript" ></script>
		<script type="text/javascript">
		
			function createEditor(model, store, popupManager) {
				//new MockupEditorFigure($("#mockups"), mockup);
				var editor = new MockApiModelEditorFigure($("#mockups"), model, store, popupManager, MetapiParser)
				editor.addEventListener('xmlGenerated', function(xml) {
					new TextPopup($("#mockups"), xml, "MockApi XML");
				});
				editor.addEventListener('deployAPI', function(representation) {
					popupManager.showNotification("Deploying api...");
					var request = $.ajax({
						type: "POST",
						url: "../metapi/generatedEndpoints",
						data: representation
					});
					request.done(function(msg) {
						popupManager.showSuccess("API successfully deployed");
					});
					request.fail(function(jqXHR, textStatus) {
						popupManager.showError("Error during API deploy: " + textStatus);
					});
				});
				editor.addEventListener('resetAPI', function(xml) {
					popupManager.showNotification("Resetting api...");
					var request = $.ajax({
						type: "POST",
						url: "dgsgenerator",
						data: "<reset />"
					});
					request.done(function(msg) {
						popupManager.showSuccess("API successfully resetted");
					});
					request.fail(function(jqXHR, textStatus) {
						popupManager.showError("Error during API reset: " + textStatus);
					});
				});
			};
		
			$(document).ready(function() {
				var serializer = new ModelJSONSerializer();
				//var store = new MockApiLocalStorageModelStore(serializer);
				var store = new MockApiRestStore("../metapi/models/", serializer);
				var modelId = "defaultModel"; //window.location.href.split("model=")[1];
				var model = null;
				var popupManager = new PopupManager();
				if (modelId) {
					model = store.loadModel(modelId, function(model) {
						if (!model) {
							model = new MockApiModel();
							model.addMockup(new Mockup("mockup1"));
						}
						createEditor(model, store, popupManager);
					});
				}	
			});
			
			
			
			/*
			function createFeatureAndFigure(text) {
				var feature = new Feature();
				var figure = new FeatureFigure($("#canvas"), text, feature);
				mockup.addFeature(feature);
				figure.addEventListener('delete', function(feature) {
					console.log('delete');
					mockup.removeFeature(feature);
					console.log(mockup.features);
				});
			}
			
			$(document).ready(function() {
				$("#imageUrl").change(function() {
					$("#mockupImage").attr("src", $("#imageUrl").val());
				});
				$("#addItem").click(function() {
					createFeatureAndFigure("item(Class)\nviewing");
				});
				$("#addList").click(function() {
					createFeatureAndFigure("list(Class)\nviewing");
				});
				$("#generateXML").click(function() {
					console.log(serializer.serialize({mockups: [mockup]}));
				});
			});		
			*/
		</script>
		<link type="text/css" rel="stylesheet" href="style/style.css" />
		<link type="text/css" rel="stylesheet" href="style/jquery-ui.css" />
		<link href='http://fonts.googleapis.com/css?family=Englebert' rel='stylesheet' type='text/css'>
		<link type="text/css" rel="stylesheet" href="style/custom.css" />
		<link rel="stylesheet" href="css/codemirror.css">
		<link rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	<body>
	
		<jsp:include page="parts/menu.jsp"></jsp:include>

		<div class="container">
		<div id="mockups">
		</div>
		<div id="footer">
			<div id="logos">
				DGS &amp; LIFIA				
			</div>
			<p>LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina</p>
			<p>Department of Computer Science, Chemnitz University of Technology, Germany</p>
		</div>
		</div>
	</body>
</html>
