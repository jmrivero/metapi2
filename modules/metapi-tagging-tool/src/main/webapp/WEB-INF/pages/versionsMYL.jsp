<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<title>Project MYL</title>

<script src="../lib/codemirror.js"></script><style type="text/css"></style>
<script src="../lib/javascript.js"></script>
<script src="../lib/dust.js"></script>
<script src="../lib/xml.js"></script>
<script src="../lib/css.js"></script>
<script src="../lib/htmlmixed.js"></script>
<script src="../lib/jquery.min.js"></script>
<script src="../lib/bootstrap.min.js"></script>
<script src="../lib/alert.js"></script>
<link rel="shortcut icon" href="../images/favicon.ico" /> 
<script src="../libs/class.js" type="text/javascript" ></script>
<script src="../libs/noty/jquery.noty.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/top.js" type="text/javascript" ></script>
<script src="../libs/noty/layouts/topRight.js" type="text/javascript" ></script>
<script src="../libs/noty/themes/default.js" type="text/javascript" ></script>
<script src="../scripts/util/PopupManager.js" type="text/javascript" ></script>
		
<link rel="stylesheet" href="../css/MYLStyle.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<style>
	#alerts {
		/*height: 63px;*/
	}
  
  
</style>
</head>
<body style="">
  
<jsp:include page="parts/menuMYL.jsp"></jsp:include>




  <script type="text/javascript">
    var templateEditor;
    var currentFilename = null;
    var newFile = false;
    var applicationId = 2;

	var href = jQuery(location).attr('href').split("/");
	var name = href[5];
    
    var projectsMYL = "../../metapi/MYL/1/versions/"
   
  

    

    

    
    function addFileToListMYL(filename) {
        var html = "<td>"+filename+ " Renderized Files</td><td></td><td><a title=\"List files of "+filename+"\" class=\"icon icon-list\" href=\"../historyMYL/"+ filename +"\"></a></td>";
        $("#fileListMYL").append(html);    
    }
    
    
    $(document).ready(function() {
    	  
    	  $.ajax({url: projectsMYL + name, type: "GET", dataType: 'json', success: function(response) {
    		console.log("el response viene con: " + response.files[0]);
    		  if(response.files[0] != 'empty'){
      			$.each(response.files, function(_, filename) {
              	 addFileToListMYL(filename);
              	});
              }
      		else{
      			$( ".table" ).remove();
      			$( ".title" ).remove();
      			$("#mes").append("<br><p class=\"text-center\">No exists versions rendered with <strong>Make Your Language </strong> for this project. <small>Please go to home page and click <em><a href=\"../projectsMYL\">Factory.</a></em></small></p><br>"); 
      		}
            }});

    });
    
    

    
		</script>

  <div class="container">

    <h4 class="titleMYL">Project Versions Manager. <span id="filename"><i> Manage your versions safely and organized.</i></span></h4>

    <hr>	
	
	<div id="mes">
	</div>
	
   	<table class="table">
        <thead>
          <tr>
            <th class="pink">Project Name</th>
            <th class="pink"> </th>
            <th></th>
           
          </tr>
        </thead>
        <tbody>
          <tr id="fileListMYL">
         
          </tr> 
        </tbody>
      </table>
   	
   	

  </div>

  <blockquote class="center">
	  <p></p>
	  <footer>
	  	LIFIA, Facultad de Inform&aacute;tica, Universidad Nacional de La Plata, La Plata - Argentina. <cite title="Source Title">� MYL Project</cite>
	  </footer>
  </blockquote>

</body></html>
