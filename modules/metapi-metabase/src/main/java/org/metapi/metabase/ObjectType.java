package org.metapi.metabase;

public class ObjectType {

	private String name;

	public ObjectType(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	
}
