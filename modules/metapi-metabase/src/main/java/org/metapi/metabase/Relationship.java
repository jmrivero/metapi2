package org.metapi.metabase;

public class Relationship {

	private Long id;
	private String name;
	private ObjectType sourceType;
	private ObjectType destType;
	
	public Relationship(Long id, String name, ObjectType sourceType, ObjectType destType) {
		this.id = id;
		this.name = name;
		this.sourceType = sourceType;
		this.destType = destType;
	}
	
	public Relationship(String name, ObjectType sourceType, ObjectType destType) {
		this(null, name, sourceType, destType);
	}

	public String getName() {
		return name;
	}

	public ObjectType getSourceType() {
		return sourceType;
	}

	public ObjectType getDestType() {
		return destType;
	}

	public Long getId() {
		return id;
	}
	
}
