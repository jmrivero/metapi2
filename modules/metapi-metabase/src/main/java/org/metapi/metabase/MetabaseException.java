package org.metapi.metabase;

public class MetabaseException extends Exception {

	private static final long serialVersionUID = 4041673424864348183L;
	
	public MetabaseException(String cause) {
		super(cause);
	}
	
	public MetabaseException(Throwable t) {
		super(t);
	}

}
