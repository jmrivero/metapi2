package org.metapi.metabase;

import java.util.List;

public interface Metabase {

	public List<MetabaseObject> getAll(String type) throws MetabaseException;

	public MetabaseObject getById(String type, long id) throws MetabaseException;

	public MetabaseObject save(String type, String representation) throws MetabaseException;
	
	public MetabaseObject update(String type, long id, String representation) throws MetabaseException;

	public void delete(String type, long id) throws MetabaseException;

	public void associate(String sourceType, String associationName, String destType, long sourceId, long destId) throws MetabaseException;

	public void dissociate(String sourceType, String associationName, String destType, long sourceId, long destId) throws MetabaseException;

	public List<MetabaseObject> getAssociatedObjects(String sourceType, String associationName, String destType, long sourceId) throws MetabaseException;

	public void destroyDatabase() throws MetabaseException;
	
	public void deleteAll(String type) throws MetabaseException;

	public void dissociateAll(String sourceType, String associationName, String destType, long sourceId) throws MetabaseException;

	void clearDatabase() throws MetabaseException;

	void close() throws MetabaseException;
	
}