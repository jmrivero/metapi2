package org.metapi.metabase.hsqldb;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.MetabaseObject;
import org.metapi.metabase.ObjectType;
import org.metapi.metabase.Relationship;

public class MetabaseHsqldb implements Metabase {

	private Connection connection;
	private String dbPath;
	private Map<String, ObjectType> types;
	private Map<String, Relationship> relationships;

	public MetabaseHsqldb(String dbPath, boolean ensureNewDatabase) throws Exception {
		this.initializeDbConnection(dbPath, ensureNewDatabase);
		this.types = new HashMap<String, ObjectType>();
		this.relationships = new HashMap<String, Relationship>();
		this.getTypes();
		this.getRelationships();
	}
	
	public MetabaseHsqldb(String dbPath) throws Exception {
		this(dbPath, false);
	}

	private Map<String, Relationship> getRelationships() throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement(
				"SELECT r.mb_relationship_id, r.name rname, st.name stname, dt.name dtname " +
				"FROM mb_relationship r " +
				"JOIN mb_type st ON r.src_mb_type_id = st.mb_type_id " +
				"JOIN mb_type dt ON r.dest_mb_type_id = dt.mb_type_id "
		);
		ResultSet rs = stmt.executeQuery();
		Map<String, Relationship> types = new HashMap<String, Relationship>();
		while (rs.next()) {
			this.addRelationship(new Relationship(rs.getLong(1), rs.getString(2), this.getType(rs.getString(3)), this.getType(rs.getString(4))));
		}
		stmt.close();
		return types;
	}

	private void addRelationship(Relationship rel) {
		this.relationships.put(this.getKeyForRelationship(rel), rel);
	}

	private String getKeyForRelationship(Relationship rel) {
		return rel.getSourceType().getName() + "-" + rel.getName() + "-" + rel.getDestType().getName();
	}

	private ObjectType getType(String typeName) {
		return this.types.get(typeName);
	}

	private void initializeDbConnection(String dbPath, boolean ensureNewDatabase) throws ClassNotFoundException, SQLException, IOException, MetabaseException {
		Class.forName("org.hsqldb.jdbcDriver");
		this.dbPath = dbPath;
		if (ensureNewDatabase) {
			this.destroyDatabase();
		}
		boolean fileExists = new File(dbPath + ".script").exists();
		this.connection = DriverManager.getConnection("jdbc:hsqldb:file:" + dbPath + ";sql.syntax_mys=true;shutdown=true", "SA", "");
		this.connection.setAutoCommit(true);
		if (!fileExists) {
			this.initializeDB();
		}
	}

	private void initializeDB() throws SQLException, IOException {
		Statement stmt = this.connection.createStatement();
		stmt.executeUpdate(IOUtils.toString(this.getClass().getResourceAsStream("/org/metapi/metabase/schema/schema.sql")));
		stmt.close();
	}

	private void getTypes() throws SQLException {
		
		PreparedStatement stmt = this.connection.prepareStatement(
			"SELECT name FROM mb_type"
		);
		ResultSet rs = stmt.executeQuery();
		Map<String, ObjectType> types = new HashMap<String, ObjectType>();
		while (rs.next()) {
			this.addType(rs.getString(1));
		}
		stmt.close();
	}

	/* (non-Javadoc)
	 * @see org.metapi.metabase.Metabase#getAll(java.lang.String)
	 */
	@Override
	public List<MetabaseObject> getAll(String type) throws MetabaseException {
		try {
			PreparedStatement stmt = this.connection.prepareStatement(
				"SELECT * " +
				"FROM mb_object o " +
				"JOIN mb_type t ON o.mb_type_id = t.mb_type_id " +
				"WHERE t.name = ?"
			);
			stmt.setString(1, type);
			ResultSet rs = stmt.executeQuery();
			stmt.close();
			return this.objectsFromResultSet(rs, type);
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}

	private List<MetabaseObject> objectsFromResultSet(ResultSet rs, String type) throws SQLException {
		List<MetabaseObject> objects = new ArrayList<MetabaseObject>();
		while (rs.next()) {
			objects.add(new MetabaseObject(rs.getLong("mb_object_id"), rs.getString("representation"), this.getType(type)));
		}
		return objects;
	}
	
	/* (non-Javadoc)
	 * @see org.metapi.metabase.Metabase#getById(java.lang.String, long)
	 */
	@Override
	public MetabaseObject getById(String type, long id) throws MetabaseException {
		try {
			PreparedStatement stmt = this.connection.prepareStatement(
				"SELECT * " +
				"FROM mb_object o " +
				"JOIN mb_type t ON o.mb_type_id = t.mb_type_id " +
				"WHERE t.name = ? AND o.mb_object_id = ?"
			);
			stmt.setString(1, type);
			stmt.setLong(2, id);
			ResultSet rs = stmt.executeQuery();
			stmt.close();
			List<MetabaseObject> objects = objectsFromResultSet(rs, type);
			if (objects.size() == 1) {
				return objects.get(0);
			}
			return null;
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public MetabaseObject save(String type, String representation) throws MetabaseException {
		return this.save(type, new MetabaseObject(representation, new ObjectType(type)));
	}

	private MetabaseObject save(String type, MetabaseObject object) throws MetabaseException {
		try {
			this.ensureType(type);
			if (object.getId() == null) {
				return this.createObject(type, object);
			} else {
				return this.update(object.getType().getName(), object.getId(), object.getRepresentation());
			}
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}

	private ObjectType ensureType(String type) throws SQLException {
		if (!this.types.containsKey(type)) {
			this.createType(type);
		}
		return this.types.get(type);
	}
	
	@Override
	public MetabaseObject update(String typeName, long objectId, String representation) throws MetabaseException {
		try {
			if (this.getType(typeName) == null) {
				throw new MetabaseException("Type not defined: " + typeName);
			}
			PreparedStatement stmt = this.connection.prepareStatement(
				"UPDATE mb_object SET representation = ? WHERE mb_object_id = ?"
			);
			stmt.setString(1,  representation);
			stmt.setLong(2,  objectId);
			stmt.executeUpdate();
			stmt.close();
			return new MetabaseObject(objectId, representation, this.getType(typeName));
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}

	private MetabaseObject createObject(String type, MetabaseObject object) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement(
			"INSERT INTO mb_object (mb_type_id, representation) " +
			"SELECT (SELECT mb_type_id FROM mb_type where name = ?), ?", Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1,  type);
		stmt.setString(2,  object.getRepresentation());
		stmt.executeUpdate();
		Long freshId = this.getLastInsertedId(stmt);
		stmt.close();
		return new MetabaseObject(freshId, object.getRepresentation(), object.getType());
	}

	private Long getLastInsertedId(PreparedStatement stmt) throws SQLException {
		ResultSet keysRS = stmt.getGeneratedKeys();
		keysRS.next();
		Long freshId = keysRS.getLong(1);
		keysRS.close();
		return freshId;
	}

	private void createType(String type) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement(
			"INSERT INTO mb_type (name) VALUES ?"
		);
		stmt.setString(1,  type);
		stmt.executeUpdate();
		stmt.close();
		this.addType(type);
	}

	private void addType(String type) {
		this.types.put(type, new ObjectType(type));		
	}

	@Override
	public void delete(String type, long id) throws MetabaseException {
		try {
			PreparedStatement stmt = this.connection.prepareStatement(
					"DELETE FROM mb_related_object WHERE src_mb_object_id = ? OR dest_mb_object_id = ?"
				);
			stmt.setLong(1, id);
			stmt.setLong(2, id);
			stmt.executeUpdate();
			stmt.close();
			stmt = this.connection.prepareStatement(
				"DELETE FROM mb_object WHERE mb_object_id = ?"
			);
			stmt.setLong(1, id);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.metapi.metabase.Metabase#associate(java.lang.String, java.lang.String, java.lang.String, long, long)
	 */
	@Override
	public void associate(String sourceType, String associationName, String destType, long sourceId, long destId) throws MetabaseException {
		try {
			this.ensureObject(sourceType, sourceId);
			this.ensureObject(destType, destId);
			Relationship rel = this.ensureRelationship(sourceType, associationName, destType);
			PreparedStatement stmt = this.connection.prepareStatement(
				"INSERT INTO mb_related_object (mb_relationship_id, src_mb_object_id, dest_mb_object_id) " +
				"VALUES                        (?                 , ?               , ?                )"                         
			);
			stmt.setLong(1,  rel.getId());
			stmt.setLong(2,  sourceId);
			stmt.setLong(3,  destId);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}

	private void ensureObject(String type, long id) throws SQLException, MetabaseException {
		if (this.getById(type, id) == null) {
			throw new MetabaseException("Object not found: " + type + " id =" + id);
		}
	}
	
	private Relationship ensureRelationship(String sourceType, String name, String destType) throws SQLException {
		this.ensureType(sourceType);
		this.ensureType(destType);
		String key = this.getKeyForRelationship(new Relationship(name, this.getType(sourceType), this.getType(destType)));
		if (!this.relationships.containsKey(key)) {
			this.createRelationship(sourceType, name, destType);
		}
		return this.relationships.get(key);
	}

	private Relationship createRelationship(String sourceType, String name, String destType) throws SQLException {
		PreparedStatement stmt = this.connection.prepareStatement(
			"INSERT INTO mb_relationship (src_mb_type_id, dest_mb_type_id, name) " +
			"SELECT (SELECT mb_type_id FROM mb_type where name = ?), " +
			"       (SELECT mb_type_id FROM mb_type where name = ?), " +
			"       ?",
			Statement.RETURN_GENERATED_KEYS
		);
		stmt.setString(1,  sourceType);
		stmt.setString(2,  destType);
		stmt.setString(3,  name);
		stmt.executeUpdate();
		Long freshId = this.getLastInsertedId(stmt);
		stmt.close();
		Relationship rel = new Relationship(freshId, name, this.getType(sourceType), this.getType(destType));
		this.addRelationship(rel);
		return rel;
	}

	@Override
	public void dissociate(String sourceType, String associationName, String destType, long sourceId, long destId) throws MetabaseException {
		try {
			this.ensureObject(sourceType, sourceId);
			this.ensureObject(destType, destId);
			Relationship rel = this.ensureRelationship(sourceType, associationName, destType);
			PreparedStatement stmt = this.connection.prepareStatement(
				"DELETE FROM mb_related_object " +
				"WHERE mb_relationship_id = ? AND src_mb_object_id = ? AND dest_mb_object_id = ?"                         
			);
			stmt.setLong(1,  rel.getId());
			stmt.setLong(2,  sourceId);
			stmt.setLong(3,  destId);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public List<MetabaseObject> getAssociatedObjects(String sourceType, String associationName, String destType, long sourceId) throws MetabaseException {
		try {
			this.ensureObject(sourceType, sourceId);
			Relationship rel = this.ensureRelationship(sourceType, associationName, destType);
			PreparedStatement stmt = this.connection.prepareStatement(
					"SELECT * " +
					"FROM mb_object o " +
					"JOIN mb_related_object ro ON o.mb_object_id = ro.dest_mb_object_id " +
					"WHERE ro.mb_relationship_id = ? AND ro.src_mb_object_id = ? "
				);
				stmt.setLong(1, rel.getId());
				stmt.setLong(2, sourceId);
				ResultSet rs = stmt.executeQuery();
				stmt.close();
				return this.objectsFromResultSet(rs, destType);
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public void clearDatabase() throws MetabaseException {
		try {
			Statement stmt = this.connection.createStatement();
			stmt.execute(
				"DELETE FROM mb_related_object"
			);
			stmt.execute(
				"DELETE FROM mb_object"
			);
			stmt.execute(
				"DELETE FROM mb_relationship"
			);
			stmt.execute(
				"DELETE FROM mb_type"
			);
			stmt.close();
			this.relationships.clear();
			this.types.clear();
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public void destroyDatabase() throws MetabaseException {
		try {
			if (this.connection != null) {
				this.connection.close();
			}
			new File(this.dbPath + ".log").delete();
			new File(this.dbPath + ".properties").delete();
			new File(this.dbPath + ".script").delete();
			FileUtils.deleteDirectory(new File(this.dbPath + ".tmp"));
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}

	@Override
	public void deleteAll(String type) throws MetabaseException {
		try {
			PreparedStatement stmt = this.connection.prepareStatement(
				"DELETE FROM mb_related_object r " + 
				"WHERE EXISTS (" +
					"SELECT * " + 
					"FROM mb_object o " +
					"JOIN mb_type t on t.mb_type_id = o.mb_type_id " +
					"WHERE t.name = ? AND (r.src_mb_object_id = o.mb_object_id OR r.dest_mb_object_id = o.mb_object_id)" + 
				")"
			);
			stmt.setString(1, type);
			stmt.executeUpdate();
			stmt.close();
			stmt = this.connection.prepareStatement(
				"DELETE FROM mb_object o " + 
				"WHERE EXISTS (" +
					"SELECT * " + 
					"FROM mb_type t " +
					"WHERE t.name = ? AND (o.mb_type_id = t.mb_type_id)" + 
				")"		
			);
			stmt.setString(1, type);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public void dissociateAll(String sourceType, String associationName, String destType, long sourceId) throws MetabaseException {
		try {
			this.ensureObject(sourceType, sourceId);
			Relationship rel = this.ensureRelationship(sourceType, associationName, destType);
			PreparedStatement stmt = this.connection.prepareStatement(
					"DELETE FROM mb_related_object ro " + 
					"WHERE ro.mb_relationship_id = ? AND ro.src_mb_object_id = ? "
			);
			stmt.setLong(1, rel.getId());
			stmt.setLong(2, sourceId);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new MetabaseException(e);
		}
	}
	
	@Override
	public void close() throws MetabaseException {
		try {
			this.connection.close();
		} catch (SQLException e) {
			throw new MetabaseException(e);

		}
	}

}
