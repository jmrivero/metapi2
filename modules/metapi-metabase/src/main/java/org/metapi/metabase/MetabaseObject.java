package org.metapi.metabase;

public class MetabaseObject {

	private Long id;
	private String representation;
	private ObjectType type;

	public MetabaseObject(Long id, String representation, ObjectType type) {
		this.id = id;
		this.representation = representation;
		this.type = type;
	}

	public MetabaseObject(String representation, ObjectType type) {
		this(null, representation, type);
	}

	public String getRepresentation() {
		return representation;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public Long getId() {
		return id;
	}

	public ObjectType getType() {
		return type;
	}

}
