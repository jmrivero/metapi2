CREATE TABLE mb_type (
  mb_type_id BIGINT IDENTITY,
  name varchar(255) DEFAULT NULL,
  CONSTRAINT mb_type_name_index UNIQUE (name)
);

CREATE TABLE mb_object (
  mb_object_id BIGINT IDENTITY,
  representation TEXT,
  mb_type_id INTEGER DEFAULT NULL,
  CONSTRAINT mb_object_mb_type FOREIGN KEY (mb_type_id) REFERENCES mb_type (mb_type_id)
);

CREATE TABLE mb_relationship (
  mb_relationship_id BIGINT IDENTITY,
  src_mb_type_id INTEGER DEFAULT NULL,
  dest_mb_type_id INTEGER DEFAULT NULL,
  name varchar(255) DEFAULT NULL,
  CONSTRAINT mb_relationship_unique_index_1 UNIQUE (src_mb_type_id,dest_mb_type_id,name),
  CONSTRAINT mb_relationship_src_mb_type_id FOREIGN KEY (src_mb_type_id) REFERENCES mb_type (mb_type_id),
  CONSTRAINT mb_relationship_dest_mb_type_id FOREIGN KEY (dest_mb_type_id) REFERENCES mb_type (mb_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE mb_related_object (
  mb_relationship_id INTEGER NOT NULL,
  src_mb_object_id INTEGER NOT NULL,
  dest_mb_object_id INTEGER NOT NULL,
  PRIMARY KEY (mb_relationship_id,src_mb_object_id,dest_mb_object_id),
  CONSTRAINT mb_related_mb_object_src_mb_object_id FOREIGN KEY (src_mb_object_id) REFERENCES mb_object (mb_object_id),
  CONSTRAINT mb_related_mb_object_dest_mb_object_id FOREIGN KEY (dest_mb_object_id) REFERENCES mb_object (mb_object_id)
)