package org.metapi.metabase.test;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.metapi.metabase.Metabase;
import org.metapi.metabase.MetabaseException;
import org.metapi.metabase.MetabaseObject;
import org.metapi.metabase.hsqldb.MetabaseHsqldb;

public class MetabaseTestCase extends TestCase {

	Metabase metabase;
	
	public void testBasicCrud() throws Exception {
		metabase = new MetabaseHsqldb("src/test/java/org/metapi/metabase/test/__testing_metabase_basicCrud", true);
		List<MetabaseObject> objects = metabase.getAll("Car");
		Assert.assertEquals(0, objects.size());

		metabase.save("Car", "c1");
		objects = metabase.getAll("Car");
		Assert.assertEquals(1, objects.size());
		Assert.assertEquals("c1", objects.get(0).getRepresentation());

		MetabaseObject object = metabase.getById("Car", objects.get(0).getId());
		Assert.assertEquals(object.getId(), objects.get(0).getId());
		Assert.assertEquals(object.getRepresentation(), objects.get(0).getRepresentation());
		
		metabase.update("Car", object.getId(), "cOne");
		object = metabase.getById("Car", objects.get(0).getId());
		Assert.assertEquals(object.getId(), objects.get(0).getId());
		Assert.assertEquals(object.getRepresentation(), "cOne");

		metabase.delete("Car", object.getId());
		objects = metabase.getAll("Car");
		Assert.assertEquals(0, objects.size());
		
		metabase.save("Car", "c2");
		metabase.save("Car", "c3");
		metabase.save("Car", "c4");
		objects = metabase.getAll("Car");
		Assert.assertEquals(3, objects.size());
		metabase.deleteAll("Car");
		objects = metabase.getAll("Car");
		Assert.assertEquals(0, objects.size());
		
		metabase.save("Car", "c");
		metabase.save("Wheel", "w");
		metabase.save("Engine", "e");
		objects = metabase.getAll("Car");
		Assert.assertEquals(1, metabase.getAll("Car").size());
		Assert.assertEquals(1, metabase.getAll("Wheel").size());
		Assert.assertEquals(1, metabase.getAll("Engine").size());
		metabase.clearDatabase();
		Assert.assertEquals(0, metabase.getAll("Car").size());
		Assert.assertEquals(0, metabase.getAll("Wheel").size());
		Assert.assertEquals(0, metabase.getAll("Engine").size());
		
		metabase.save("Car", "c");
		Assert.assertEquals(1, metabase.getAll("Car").size());
		
		this.metabase.destroyDatabase();
	}
	
	public void testAssociations() throws Exception {
		metabase = new MetabaseHsqldb("src/test/java/org/metapi/metabase/test/__testing_metabase_associations", true);
		
		MetabaseObject c = metabase.save("Car", "c1");
		MetabaseObject w1 = metabase.save("Wheel", "w1");
		MetabaseObject w2 = metabase.save("Wheel", "w2");
		MetabaseObject w3 = metabase.save("Wheel", "w3");
		
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w1.getId());
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w2.getId());
		
		List<MetabaseObject> wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(2, wheels.size());
		Assert.assertEquals(w1.getId(), wheels.get(0).getId());
		Assert.assertEquals(w1.getRepresentation(), wheels.get(0).getRepresentation());
		Assert.assertEquals(w2.getId(), wheels.get(1).getId());
		Assert.assertEquals(w2.getRepresentation(), wheels.get(1).getRepresentation());
		
		metabase.dissociate("Car", "wheels", "Wheel", c.getId(), w1.getId());
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(1, wheels.size());
		
		metabase.delete("Wheel", w2.getId());
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(0, wheels.size());
		
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w1.getId());
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w3.getId());
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(2, wheels.size());
		
		metabase.deleteAll("Wheel");
		
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(0, wheels.size());
		
		MetabaseObject w4 = metabase.save("Wheel", "w4");
		MetabaseObject w5 = metabase.save("Wheel", "w5");
		
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w4.getId());
		metabase.associate("Car", "wheels", "Wheel", c.getId(), w5.getId());
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(2, wheels.size());
		metabase.dissociateAll("Car", "wheels", "Wheel", c.getId());
		wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId());
		Assert.assertEquals(0, wheels.size());
		
		try {
			wheels = metabase.getAssociatedObjects("Car", "wheels", "Wheel", c.getId() + 1);
			Assert.fail("No exception was thrown by getAssociatedObjects when source object does not exist");
		} catch (MetabaseException e) { 	}
		try {
			wheels = metabase.getAssociatedObjects("Car", "wheeels", "Wheel", c.getId() + 1);
			Assert.fail("No exception was thrown by getAssociatedObjects association does not exist");
		} catch (MetabaseException e) { 	}
		
		this.metabase.destroyDatabase();

	}
	
	public void testReopen() throws Exception {
		metabase = new MetabaseHsqldb("src/test/java/org/metapi/metabase/test/__testing_metabase_reopen", true);
		
		metabase.save("Car", "c1");
		
		//metabase.close();
		
		metabase = new MetabaseHsqldb("src/test/java/org/metapi/metabase/test/__testing_metabase_reopen", false);
		
		assertEquals(1, metabase.getAll("Car").size());
		
		//metabase.close();

	}

}
